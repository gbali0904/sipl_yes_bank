package com.example.gunjan.spil_cpv_appyes_bank.models;

import java.io.Serializable;

/**
 * Created by Gunjan on 11-11-2016.
 */

public class ViewDataEntryForRvr {

    private static ViewDataEntryForRvr mInstance;
    /**
     * status : true
     * message : RVR Data Entry Details
     * user_data : {"CPVId":"4614","Executive":"JITENDER","LoanNo":"1610201750260","AppName":"SHAILESH THAPA","AddConfirm":"null","MetWith":"hkj","FamilyMember":"vgvj","YrOfCurrResidence":"ssss","ResStatus":"None","Loacate":"s","ResiConstruction":"None","Tpc1":"s","Tpc2":"ss","GateColor":"ss","ExeComment":"s","PermAddress":"ss","OffAddress":"s","Dates":"2016/11/11 ","Times":"22:53 PM","DOB":"ss","Landmark":"ss","Relation":"ss","EarningMember":"null","YearsInCity":"ss","AreaSqft":"s","Laoclity":"None","Exterior":"null","NameConfirm":"null","JobConfirm":"null","WallColor":"ss","IDNo":"ss","MobileNo":"s","TypeOfHouse":"null","InteriorSeen":"null","ResidingLast1":"ss","ResidingLast2":"ss","Designation":"ss","Education":"None","addconfirm_index":0,"married_status_index":0,"located_index":0,"education_index":0,"reident_status_index":0,"locality_index":0,"type_of_hous_index":0,"exterior_index":0,"interior_seen_index":0,"name_conf_index":0,"job_conf_index":0}
     */

    private String status;
    private String message;
    /**
     * CPVId : 4614
     * Executive : JITENDER
     * LoanNo : 1610201750260
     * AppName : SHAILESH THAPA
     * AddConfirm : null
     * MetWith : hkj
     * FamilyMember : vgvj
     * YrOfCurrResidence : ssss
     * ResStatus : None
     * Loacate : s
     * ResiConstruction : None
     * Tpc1 : s
     * Tpc2 : ss
     * GateColor : ss
     * ExeComment : s
     * PermAddress : ss
     * OffAddress : s
     * Dates : 2016/11/11
     * Times : 22:53 PM
     * DOB : ss
     * Landmark : ss
     * Relation : ss
     * EarningMember : null
     * YearsInCity : ss
     * AreaSqft : s
     * Laoclity : None
     * Exterior : null
     * NameConfirm : null
     * JobConfirm : null
     * WallColor : ss
     * IDNo : ss
     * MobileNo : s
     * TypeOfHouse : null
     * InteriorSeen : null
     * ResidingLast1 : ss
     * ResidingLast2 : ss
     * Designation : ss
     * Education : None
     * addconfirm_index : 0
     * married_status_index : 0
     * located_index : 0
     * education_index : 0
     * reident_status_index : 0
     * locality_index : 0
     * type_of_hous_index : 0
     * exterior_index : 0
     * interior_seen_index : 0
     * name_conf_index : 0
     * job_conf_index : 0
     */


    private ViewDataEntryForRvr() {
    }
    public static synchronized ViewDataEntryForRvr getInstance() {
        if(mInstance==null){
            mInstance=new ViewDataEntryForRvr();
        }
        return mInstance;
    }

    private UserDataBean user_data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDataBean getUser_data() {
        return user_data;
    }

    public void setUser_data(UserDataBean user_data) {
        this.user_data = user_data;
    }

    public static class UserDataBean implements Serializable {
        private String CPVId;
        private String Executive;
        private String LoanNo;
        private String AppName;
        private String AddConfirm;
        private String MetWith;
        private String FamilyMember;
        private String YrOfCurrResidence;
        private String ResStatus;
        private String Loacate;
        private String ResiConstruction;
        private String Tpc1;
        private String Tpc2;
        private String GateColor;
        private String ExeComment;
        private String PermAddress;
        private String OffAddress;
        private String Dates;
        private String Times;
        private String DOB;
        private String Landmark;
        private String Relation;
        private String EarningMember;
        private String YearsInCity;
        private String AreaSqft;
        private String Laoclity;
        private String Exterior;
        private String NameConfirm;
        private String JobConfirm;
        private String WallColor;
        private String IDNo;
        private String MobileNo;
        private String TypeOfHouse;
        private String InteriorSeen;
        private String ResidingLast1;
        private String ResidingLast2;
        private String Designation;
        private String Education;
        private int addconfirm_index;
        private int married_status_index;
        private int located_index;
        private int education_index;
        private int reident_status_index;
        private int locality_index;
        private int type_of_hous_index;
        private int exterior_index;
        private int interior_seen_index;
        private int name_conf_index;
        private int job_conf_index;

        public String getCPVId() {
            return CPVId;
        }

        public void setCPVId(String CPVId) {
            this.CPVId = CPVId;
        }

        public String getExecutive() {
            return Executive;
        }

        public void setExecutive(String Executive) {
            this.Executive = Executive;
        }

        public String getLoanNo() {
            return LoanNo;
        }

        public void setLoanNo(String LoanNo) {
            this.LoanNo = LoanNo;
        }

        public String getAppName() {
            return AppName;
        }

        public void setAppName(String AppName) {
            this.AppName = AppName;
        }

        public String getAddConfirm() {
            return AddConfirm;
        }

        public void setAddConfirm(String AddConfirm) {
            this.AddConfirm = AddConfirm;
        }

        public String getMetWith() {
            return MetWith;
        }

        public void setMetWith(String MetWith) {
            this.MetWith = MetWith;
        }

        public String getFamilyMember() {
            return FamilyMember;
        }

        public void setFamilyMember(String FamilyMember) {
            this.FamilyMember = FamilyMember;
        }

        public String getYrOfCurrResidence() {
            return YrOfCurrResidence;
        }

        public void setYrOfCurrResidence(String YrOfCurrResidence) {
            this.YrOfCurrResidence = YrOfCurrResidence;
        }

        public String getResStatus() {
            return ResStatus;
        }

        public void setResStatus(String ResStatus) {
            this.ResStatus = ResStatus;
        }

        public String getLoacate() {
            return Loacate;
        }

        public void setLoacate(String Loacate) {
            this.Loacate = Loacate;
        }

        public String getResiConstruction() {
            return ResiConstruction;
        }

        public void setResiConstruction(String ResiConstruction) {
            this.ResiConstruction = ResiConstruction;
        }

        public String getTpc1() {
            return Tpc1;
        }

        public void setTpc1(String Tpc1) {
            this.Tpc1 = Tpc1;
        }

        public String getTpc2() {
            return Tpc2;
        }

        public void setTpc2(String Tpc2) {
            this.Tpc2 = Tpc2;
        }

        public String getGateColor() {
            return GateColor;
        }

        public void setGateColor(String GateColor) {
            this.GateColor = GateColor;
        }

        public String getExeComment() {
            return ExeComment;
        }

        public void setExeComment(String ExeComment) {
            this.ExeComment = ExeComment;
        }

        public String getPermAddress() {
            return PermAddress;
        }

        public void setPermAddress(String PermAddress) {
            this.PermAddress = PermAddress;
        }

        public String getOffAddress() {
            return OffAddress;
        }

        public void setOffAddress(String OffAddress) {
            this.OffAddress = OffAddress;
        }

        public String getDates() {
            return Dates;
        }

        public void setDates(String Dates) {
            this.Dates = Dates;
        }

        public String getTimes() {
            return Times;
        }

        public void setTimes(String Times) {
            this.Times = Times;
        }

        public String getDOB() {
            return DOB;
        }

        public void setDOB(String DOB) {
            this.DOB = DOB;
        }

        public String getLandmark() {
            return Landmark;
        }

        public void setLandmark(String Landmark) {
            this.Landmark = Landmark;
        }

        public String getRelation() {
            return Relation;
        }

        public void setRelation(String Relation) {
            this.Relation = Relation;
        }

        public String getEarningMember() {
            return EarningMember;
        }

        public void setEarningMember(String EarningMember) {
            this.EarningMember = EarningMember;
        }

        public String getYearsInCity() {
            return YearsInCity;
        }

        public void setYearsInCity(String YearsInCity) {
            this.YearsInCity = YearsInCity;
        }

        public String getAreaSqft() {
            return AreaSqft;
        }

        public void setAreaSqft(String AreaSqft) {
            this.AreaSqft = AreaSqft;
        }

        public String getLaoclity() {
            return Laoclity;
        }

        public void setLaoclity(String Laoclity) {
            this.Laoclity = Laoclity;
        }

        public String getExterior() {
            return Exterior;
        }

        public void setExterior(String Exterior) {
            this.Exterior = Exterior;
        }

        public String getNameConfirm() {
            return NameConfirm;
        }

        public void setNameConfirm(String NameConfirm) {
            this.NameConfirm = NameConfirm;
        }

        public String getJobConfirm() {
            return JobConfirm;
        }

        public void setJobConfirm(String JobConfirm) {
            this.JobConfirm = JobConfirm;
        }

        public String getWallColor() {
            return WallColor;
        }

        public void setWallColor(String WallColor) {
            this.WallColor = WallColor;
        }

        public String getIDNo() {
            return IDNo;
        }

        public void setIDNo(String IDNo) {
            this.IDNo = IDNo;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getTypeOfHouse() {
            return TypeOfHouse;
        }

        public void setTypeOfHouse(String TypeOfHouse) {
            this.TypeOfHouse = TypeOfHouse;
        }

        public String getInteriorSeen() {
            return InteriorSeen;
        }

        public void setInteriorSeen(String InteriorSeen) {
            this.InteriorSeen = InteriorSeen;
        }

        public String getResidingLast1() {
            return ResidingLast1;
        }

        public void setResidingLast1(String ResidingLast1) {
            this.ResidingLast1 = ResidingLast1;
        }

        public String getResidingLast2() {
            return ResidingLast2;
        }

        public void setResidingLast2(String ResidingLast2) {
            this.ResidingLast2 = ResidingLast2;
        }

        public String getDesignation() {
            return Designation;
        }

        public void setDesignation(String Designation) {
            this.Designation = Designation;
        }

        public String getEducation() {
            return Education;
        }

        public void setEducation(String Education) {
            this.Education = Education;
        }

        public int getAddconfirm_index() {
            return addconfirm_index;
        }

        public void setAddconfirm_index(int addconfirm_index) {
            this.addconfirm_index = addconfirm_index;
        }

        public int getMarried_status_index() {
            return married_status_index;
        }

        public void setMarried_status_index(int married_status_index) {
            this.married_status_index = married_status_index;
        }

        public int getLocated_index() {
            return located_index;
        }

        public void setLocated_index(int located_index) {
            this.located_index = located_index;
        }

        public int getEducation_index() {
            return education_index;
        }

        public void setEducation_index(int education_index) {
            this.education_index = education_index;
        }

        public int getReident_status_index() {
            return reident_status_index;
        }

        public void setReident_status_index(int reident_status_index) {
            this.reident_status_index = reident_status_index;
        }

        public int getLocality_index() {
            return locality_index;
        }

        public void setLocality_index(int locality_index) {
            this.locality_index = locality_index;
        }

        public int getType_of_hous_index() {
            return type_of_hous_index;
        }

        public void setType_of_hous_index(int type_of_hous_index) {
            this.type_of_hous_index = type_of_hous_index;
        }

        public int getExterior_index() {
            return exterior_index;
        }

        public void setExterior_index(int exterior_index) {
            this.exterior_index = exterior_index;
        }

        public int getInterior_seen_index() {
            return interior_seen_index;
        }

        public void setInterior_seen_index(int interior_seen_index) {
            this.interior_seen_index = interior_seen_index;
        }

        public int getName_conf_index() {
            return name_conf_index;
        }

        public void setName_conf_index(int name_conf_index) {
            this.name_conf_index = name_conf_index;
        }

        public int getJob_conf_index() {
            return job_conf_index;
        }

        public void setJob_conf_index(int job_conf_index) {
            this.job_conf_index = job_conf_index;
        }
    }
}
