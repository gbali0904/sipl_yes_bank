package com.example.gunjan.spil_cpv_appyes_bank;

import android.util.Log;

import com.example.gunjan.spil_cpv_appyes_bank.models.FormDataForBVR;
import com.example.gunjan.spil_cpv_appyes_bank.models.FormDataForRVR;
import com.example.gunjan.spil_cpv_appyes_bank.models.UserDataBean;
import com.example.gunjan.spil_cpv_appyes_bank.models.UserDataBeenRVR;

import java.net.URI;
import java.net.URL;

/**
 * Created by Gunjan on 29-10-2016.
 */

public class API {
    public static final String BASE_URL="http://survinindia.in/capital/json/DataInsert.php?";
    public static final String BASE_URL_fOR_RVR="http://survinindia.in/capital/json/DataInsertRVR.php?";
    public static final String BASE_URL_fOR_BVR_UPDATE="http://survinindia.in/capital/json/DataUpdate.php?";
    public static final String BASE_URL_fOR_RVR_UPDATE="http://survinindia.in/CAPITAL/json/DataUpdateRVR.php?";
    private static final String TAG = API.class.getSimpleName();
    private static String s1;




    private static URL convertToUrl(String urlStr) {
        try {
            URL url = new URL(urlStr);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(),
                    url.getHost(), url.getPort(), url.getPath(),
                    url.getQuery(), url.getRef());
            url = uri.toURL();
            return url;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //for bVR Insert
    public static String getFormDataURL(FormDataForBVR formData){
        String UrlgetFormDataURL = "" + BASE_URL+
                "CPVId="+formData.getCpv_id()+
                "&Executive="+formData.getExecutive_name()+
                "&LoanNo="+formData.getLoan_no()+
                "&AppName="+formData.getApp_name()+
                "&AddConfirm="+formData.getAddress_confirm_value()+
                "&MetWith="+formData.getMet_with()+
                "&ApplicantDesignation="+formData.getApp_designate()+
                "&WorkingSince="+ formData.getWorking_since() +
                "&NatureOfBuss="+formData.getNature_of_business()+
                "&BoardSeen="+formData.getCompany_board_seen_value()  +
                "&ActivitySeen="+ formData.getBusiness_activity_seen_value()+
                "&BussinessCenter="+ formData.getLocality_value() +
                "&TPC1="+formData.getTcp1()+
                "&TPC2="+formData.getTcp2()+
                "&GateColor="+formData.getGate_color()+
                "&ExeComment="+formData.getExecutive_comment()+
                "&AnyUpdation="+formData.getAny_update()+
                "&ResAddress="+formData.getResident_address()+
                "&LoanDate="+formData.getDate()+
                "&LoanTime="+formData.getTime()+
                "&DOB="+formData.getDob()+
                "&LandMark="+formData.getLandmark()+
                "&Designation="+formData.getDesignation()+
                "&Located="+formData.getLocated_value()+
                "&CoStatus="+ formData.getCo_status_value()+
                "&Mobile="+ formData.getMobile()+
                "&AreaSqft="+formData.getArea_sqft()+
                "&StockSeen="+formData.getStock_seen_value()+
                "&Exterior="+formData.getExterior_value()+
                "&NameConfirm="+formData.getName_and_job_conf1_value() +
                "&JobConfirm="+formData.getName_and_job_conf2_value()+
                "&WallColor="+formData.getWall_color()+
                "&Department="+formData.getDepartment()+
                "&Education="+formData.getEducation_index_value() +
                "&EmployeeID="+formData.getEmp_id()+
                "&NumberOfEmpSeen="+formData.getNo_of_emp_seen()+
                "&TypeOfStock="+formData.getType_of_stock() +
                "&EntryPermitted="+formData.getInterior_seen_index()+
                "&EntryPermittedSeen="+formData.getInterior_seen_value()+
                "&WorkingLast1="+formData.getWorking_last1()+
                "&WorkingLast2="+ formData.getWorking_last2()+
                "&GridType="+formData.getType()+

                "&addconfirm_index="+formData.getAddress_confirm()+
                "&located_index="+formData.getLocated()+
                "&educatin_index="+formData.getEducation_index()+
                "&costatus_index="+formData.getCo_status()+
                "&boardseen_index="+formData.getCompany_board_seen()+
                "&activityseen_index="+formData.getBusiness_activity_seen()+
                "&stockseen_index="+formData.getStock_seen()+
                "&exterior_index="+formData.getExterior()+
                "&name_conf_index="+formData.getName_and_job_conf1()+
                "&job_conf_index="+formData.getName_and_job_conf2();



        UrlgetFormDataURL.trim();
        URL url;
        url = convertToUrl(UrlgetFormDataURL);
        Log.d(TAG, "UrlgetFormDataURL url: " + url.toString());
        return url.toString();
    }


    public static String getFormDataURLForRVR(FormDataForRVR formdata){

        String UrlgetFormDataURL = "" + BASE_URL_fOR_RVR+
                "CPVId="+formdata.getCpv_id()+
                "&Executive="+formdata.getExecutive_name()+
                "&LoanNo="+formdata.getLoan_no()+
                "&Education="+formdata.getEducation_index_value()+
                "&AppName="+formdata.getApp_name()+
                "&AddConfirm="+formdata.getAddress_confirm_value()+
                "&MetWith="+formdata.getMet_with()+
                "&FamilyMember="+formdata.getFamily_member()+
                "&YrOfCurrResidence="+formdata.getYear_of_resident()+
                "&ResStatus="+formdata.getResident_status_value()+
                "&Loacate="+formdata.getLocated()+
                "&ResiConstruction="+formdata.getResident_construction_value()+
                "&Tpc1="+formdata.getTcp1()+
                "&Tpc2="+formdata.getTcp2()+
                "&GateColor="+formdata.getGate_color()+
                "&ExeComment="+formdata.getExecutive_comment()+
                "&PermAddress="+formdata.getPermanent_address()+
                "&OffAddress="+formdata.getOffice_address()+
                "&Dates="+formdata.getDate()+
                "&Times="+formdata.getTime()+
                "&DOB="+formdata.getDob()+
                "&Landmark="+formdata.getLandmark()+
                "&Relation="+formdata.getRelation()+
                "&EarningMember="+formdata.getEarning_member()+
                "&YearsInCity="+formdata.getYears_in_city()+
                "&AreaSqft="+formdata.getArea_sqft()+
                "&Laoclity="+ formdata.getLocality_value() +
                "&Exterior="+formdata.getExterior_value()+
                "&NameConfirm="+formdata.getName_and_job_conf1_value() +
                "&JobConfirm="+formdata.getName_and_job_conf2_value()+
                "&WallColor="+formdata.getWall_color()+
                "&IDNo="+formdata.getId_no()+
                "&Mobileno="+ formdata.getMobile()+
                "&TypeOfHouse="+formdata.getType_of_house_value() +
                "&InteriorSeen="+formdata.getExterior_seen_value()+
                "&ResidingLast1="+formdata.getResiding_last()+
                "&ResidingLast2="+ formdata.getResiding_last2()+
                "&Designation="+formdata.getDesignation()+
                "&addconfirm_index="+formdata.getAddress_confirm()+
                "&married_status_index="+formdata.getMar_satus()+
                "&located_index="+formdata.getLocated()+
                "&education_index="+formdata.getEducation_index()+
                "&reident_status_index="+formdata.getResident_status_index()+
                "&locality_index="+formdata.getLocality_index()+
                "&type_of_hous_index="+formdata.getType_of_house_index()+
                "&exterior_index="+formdata.getExterior()+
                "&interior_seen_index="+formdata.getInterior_seen_index()+
                "&name_conf_index="+formdata.getName_and_job_conf1()+
                "&job_conf_index="+formdata.getName_and_job_conf2();
        UrlgetFormDataURL.trim();
        URL url;
        url = convertToUrl(UrlgetFormDataURL);
        Log.d(TAG, "UrlgetFormDataURL url: " + url.toString());
        return url.toString();
    }


    public static String getUpdateURL(UserDataBean instance) {
        String UrlgetFormDataURL = "" + BASE_URL_fOR_BVR_UPDATE+
                "CPVId="+instance.getCPVId()+
                "&Executive="+instance.getExecutive()+
                "&LoanNo="+instance.getLoanNo()+
                "&AppName="+instance.getAppName()+
                "&AddConfirm="+instance.getAddConfirm()+
                "&MetWith="+instance.getMetWith()+
                "&ApplicantDesignation="+instance.getApplicantDesignation()+
                "&WorkingSince="+ instance.getWorkingSince() +
                "&NatureOfBuss="+instance.getNatureOfBuss()+
                "&BoardSeen="+instance.getBoardSeen()  +
                "&ActivitySeen="+ instance.getActivitySeen()+
                "&BussinessCenter="+ instance.getBussinessCenter() +
                "&TPC1="+instance.getTPC1()+
                "&TPC2="+instance.getTPC2()+
                "&GateColor="+instance.getGateColor()+
                "&ExeComment="+instance.getExeComment()+
                "&AnyUpdation="+instance.getAnyUpdation()+
                "&ResAddress="+instance.getResAddress()+
                "&LoanDate="+instance.getLoanDate()+
                "&LoanTime="+instance.getLoanTime()+
                "&DOB="+instance.getDOB()+
                "&LandMark="+instance.getLandMark()+
                "&Designation="+instance.getDesignation()+
                "&Located="+instance.getLocated()+
                "&CoStatus="+ instance.getCoStatus()+
                "&Mobile="+ instance.getMobile()+
                "&AreaSqft="+instance.getAreaSqft()+
                "&StockSeen="+instance.getStockSeen()+
                "&Exterior="+instance.getExterior()+
                "&NameConfirm="+instance.getNameConfirm() +
                "&JobConfirm="+instance.getJobConfirm()+
                "&WallColor="+instance.getWallColor()+
                "&Department="+instance.getDepartment()+
                "&Education="+instance.getEducation() +
                "&EmployeeID="+instance.getEmployeeID()+
                "&NumberOfEmpSeen="+instance.getNumberOfEmpSeen()+
                "&TypeOfStock="+instance.getTypeOfStock() +
                "&EntryPermitted="+"aba"+
                "&EntryPermittedSeen="+instance.getEntryPermittedSeen()+
                "&WorkingLast1="+instance.getWorkingLast1()+
                "&WorkingLast2="+ instance.getWorkingLast2()+
                "&GridType="+instance.getType()+
                "&addconfirm_index="+instance.getAddconfirm_index()+
                "&located_index="+instance.getLocated_index()+
                "&educatin_index="+instance.getEducatin_index()+
                "&costatus_index="+instance.getCostatus_index()+
                "&boardseen_index="+instance.getBoardseen_index()+
                "&activityseen_index="+instance.getActivityseen_index()+
                "&stockseen_index="+instance.getStockseen_index()+
                "&exterior_index="+instance.getExterior_index()+
                "&name_conf_index="+instance.getName_conf_index()+
                "&job_conf_index="+instance.getJob_conf_indext();



        UrlgetFormDataURL.trim();
        URL url;
        url = convertToUrl(UrlgetFormDataURL);
        Log.d(TAG, "UrlgetFormDataURL url: " + url.toString());
        return url.toString();
    }

    public static String getUpdateURLRVR(UserDataBeenRVR instance) {
        String UrlgetFormDataURL = "" + BASE_URL_fOR_RVR_UPDATE+
                "CPVId="+instance.getCPVId()+
                "&Executive="+instance.getExecutive()+
                "&LoanNo="+instance.getLoanNo()+
                "&Education="+instance.getEducation()+
                "&AppName="+instance.getAppName()+
                "&AddConfirm="+instance.getAddConfirm()+
                "&MetWith="+instance.getMetWith()+
                "&FamilyMember="+instance.getFamilyMember()+
                "&YrOfCurrResidence="+instance.getYrOfCurrResidence()+
                "&ResStatus="+instance.getResStatus()+
                "&Loacate="+instance.getLoacate()+
                "&ResiConstruction="+instance.getResiConstruction()+
                "&Tpc1="+instance.getTpc1()+
                "&Tpc2="+instance.getTpc2()+
                "&GateColor="+instance.getGateColor()+
                "&ExeComment="+instance.getExeComment()+
                "&PermAddress="+instance.getPermAddress()+
                "&OffAddress="+instance.getOffAddress()+
                "&Dates="+instance.getDates()+
                "&Times="+instance.getTimes()+
                "&DOB="+instance.getDOB()+
                "&Landmark="+instance.getLandmark()+
                "&Relation="+instance.getRelation()+
                "&EarningMember="+instance.getEarningMember()+
                "&YearsInCity="+instance.getYearsInCity()+
                "&AreaSqft="+instance.getAreaSqft()+
                "&Laoclity="+ instance.getLaoclity() +
                "&Exterior="+instance.getExterior()+
                "&NameConfirm="+instance.getNameConfirm() +
                "&JobConfirm="+instance.getJobConfirm()+
                "&WallColor="+instance.getWallColor()+
                "&IDNo="+instance.getIDNo()+
                "&MobileNo="+ instance.getMobileNo()+
                "&TypeOfHouse="+instance.getTypeOfHouse() +
                "&InteriorSeen="+instance.getInteriorSeen()+
                "&ResidingLast1="+instance.getResidingLast1()+
                "&ResidingLast2="+ instance.getResidingLast2()+
                "&Designation="+instance.getDesignation()+
                "&GridType="+instance.getType()+
                "&addconfirm_index="+instance.getAddconfirm_index()+
                "&married_status_index="+instance.getMarried_status_index()+
                "&located_index="+instance.getLocated_index()+
                "&education_index="+instance.getEducation_index()+
                "&reident_status_index="+instance.getReident_status_index()+
                "&locality_index="+instance.getLocality_index()+
                "&type_of_hous_index="+instance.getType_of_hous_index()+
                "&exterior_index="+instance.getExterior_index()+
                "&interior_seen_index="+instance.getInterior_seen_index()+
                "&name_conf_index="+instance.getName_conf_index()+
                "&job_conf_index="+instance.getJob_conf_index();

        UrlgetFormDataURL.trim();
        URL url;
        url = convertToUrl(UrlgetFormDataURL);
        Log.d(TAG, "UrlgetFormDataURL url: " + url.toString());
        return url.toString();
    }


}

