package com.example.gunjan.spil_cpv_appyes_bank.models;

/**
 * Created by Gunjan on 31-10-2016.
 */

public class FormDataForRVR {



    private static FormDataForRVR mInstance;
       /**
     * executive_name : kljlkjl
     * loan_no : kjlkjl
     * cpv_id : kjlkjl
     * type : kjlkjl
     * date : kjlkjl
     * time : kjlkjl
     * address_confirm : 1
     * address_confirm_value : sj
     * landmark : lkjkjkj
     * met_with : lkjkjl
     * mar_satus : 1
     * mar_status_value : sjhd
     * family_member : lkjkjl
     * earning_member : lkjkjl
     * located : 1
     * located_value : sjhd
     * education_index : 1
     * education_index_value : fdgdf
     * year_of_resident : lkjlkj
     * years_in_city : fdgfd
     * id_no : lkjl
     * mobile : lkjl
     * resident_status_index : 1
     * resident_status_value : cnfkxdjvh
     * locality_index : 1
     * locality_value : dffff
     * area_sqft : lkjl
     * type_of_house_index : 1
     * type_of_house_value : lkjl
     * resident_construction_index : 1
     * resident_construction_value : dbfjks
     * exterior : 1
     * exterior_value : dfdv
     * interior_seen_index : 1
     * exterior_seen_value : dsf
     * tcp1 : dshfkj
     * tcp2 : hdsfhkj
     * name_and_job_conf1 : 1
     * name_and_job_conf1_value : dfd
     * name_and_job_conf2 : 1
     * name_and_job_conf2_value : sds
     * Residing_last : fdsfd
     * Residing_last2 : kvnxflkfvm
     * gate_color : 1
     * wall_color : 1
     * executive_comment : 1
     * permanent_address : 1
     * office_address : lkjlkj
     */

    private String executive_name;
    private String loan_no;
    private String cpv_id;
    private String type;
    private String date;
    private String time;
    private int address_confirm;
    private String address_confirm_value;
    private String landmark;
    private String met_with;
    private int mar_satus;
    private String dob;
    private String relation;
    private String mar_status_value;
    private String family_member;
    private String earning_member;
    private int located;
    private String located_value;
    private int education_index;
    private String education_index_value;
    private String year_of_resident;
    private String years_in_city;
    private String id_no;
    private String mobile;
    private int resident_status_index;
    private String resident_status_value;
    private int locality_index;
    private String locality_value;
    private String area_sqft;
    private int type_of_house_index;
    private String type_of_house_value;
    private int resident_construction_index;
    private String resident_construction_value;
    private int exterior;
    private String exterior_value;
    private int interior_seen_index;
    private String exterior_seen_value;
    private String tcp1;
    private String tcp2;
    private int name_and_job_conf1;
    private String name_and_job_conf1_value;
    private int name_and_job_conf2;
    private String name_and_job_conf2_value;
    private String Residing_last;
    private String Residing_last2;
    private String gate_color;
    private String wall_color;
    private String executive_comment;
    private String permanent_address;
    private String office_address;
    private String Designation;
    private String app_name;

    private FormDataForRVR() {
    }
    public static synchronized FormDataForRVR getInstance() {
        if(mInstance==null){
            mInstance=new FormDataForRVR();
        }
        return mInstance;
    }

    public static void setInstance(FormDataForRVR formData) {
        mInstance=formData;
    }


    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String Designation) {
        this.Designation = Designation;
    }


    public String getExecutive_name() {
        return executive_name;
    }

    public void setExecutive_name(String executive_name) {
        this.executive_name = executive_name;
    }


    public String getLoan_no() {
        return loan_no;
    }

    public void setLoan_no(String loan_no) {
        this.loan_no = loan_no;
    }

    public String getCpv_id() {
        return cpv_id;
    }

    public void setCpv_id(String cpv_id) {
        this.cpv_id = cpv_id;
    }


    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getAddress_confirm() {
        return address_confirm;
    }

    public void setAddress_confirm(int address_confirm) {
        this.address_confirm = address_confirm;
    }

    public String getAddress_confirm_value() {
        return address_confirm_value;
    }

    public void setAddress_confirm_value(String address_confirm_value) {
        this.address_confirm_value = address_confirm_value;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getMet_with() {
        return met_with;
    }

    public void setMet_with(String met_with) {
        this.met_with = met_with;
    }

    public int getMar_satus() {
        return mar_satus;
    }

    public void setMar_satus(int mar_satus) {
        this.mar_satus = mar_satus;
    }

    public String getMar_status_value() {
        return mar_status_value;
    }

    public void setMar_status_value(String mar_status_value) {
        this.mar_status_value = mar_status_value;
    }

    public String getFamily_member() {
        return family_member;
    }

    public void setFamily_member(String family_member) {
        this.family_member = family_member;
    }

    public String getEarning_member() {
        return earning_member;
    }

    public void setEarning_member(String earning_member) {
        this.earning_member = earning_member;
    }

    public int getLocated() {
        return located;
    }

    public void setLocated(int located) {
        this.located = located;
    }

    public String getLocated_value() {
        return located_value;
    }

    public void setLocated_value(String located_value) {
        this.located_value = located_value;
    }

    public int getEducation_index() {
        return education_index;
    }

    public void setEducation_index(int education_index) {
        this.education_index = education_index;
    }

    public String getEducation_index_value() {
        return education_index_value;
    }

    public void setEducation_index_value(String education_index_value) {
        this.education_index_value = education_index_value;
    }

    public String getYear_of_resident() {
        return year_of_resident;
    }

    public void setYear_of_resident(String year_of_resident) {
        this.year_of_resident = year_of_resident;
    }

    public String getYears_in_city() {
        return years_in_city;
    }

    public void setYears_in_city(String years_in_city) {
        this.years_in_city = years_in_city;
    }

    public String getId_no() {
        return id_no;
    }

    public void setId_no(String id_no) {
        this.id_no = id_no;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getResident_status_index() {
        return resident_status_index;
    }

    public void setResident_status_index(int resident_status_index) {
        this.resident_status_index = resident_status_index;
    }

    public String getResident_status_value() {
        return resident_status_value;
    }

    public void setResident_status_value(String resident_status_value) {
        this.resident_status_value = resident_status_value;
    }

    public int getLocality_index() {
        return locality_index;
    }

    public void setLocality_index(int locality_index) {
        this.locality_index = locality_index;
    }

    public String getLocality_value() {
        return locality_value;
    }

    public void setLocality_value(String locality_value) {
        this.locality_value = locality_value;
    }

    public String getArea_sqft() {
        return area_sqft;
    }

    public void setArea_sqft(String area_sqft) {
        this.area_sqft = area_sqft;
    }

    public int getType_of_house_index() {
        return type_of_house_index;
    }

    public void setType_of_house_index(int type_of_house_index) {
        this.type_of_house_index = type_of_house_index;
    }

    public String getType_of_house_value() {
        return type_of_house_value;
    }

    public void setType_of_house_value(String type_of_house_value) {
        this.type_of_house_value = type_of_house_value;
    }

    public int getResident_construction_index() {
        return resident_construction_index;
    }

    public void setResident_construction_index(int resident_construction_index) {
        this.resident_construction_index = resident_construction_index;
    }

    public String getResident_construction_value() {
        return resident_construction_value;
    }

    public void setResident_construction_value(String resident_construction_value) {
        this.resident_construction_value = resident_construction_value;
    }

    public int getExterior() {
        return exterior;
    }

    public void setExterior(int exterior) {
        this.exterior = exterior;
    }

    public String getExterior_value() {
        return exterior_value;
    }

    public void setExterior_value(String exterior_value) {
        this.exterior_value = exterior_value;
    }

    public int getInterior_seen_index() {
        return interior_seen_index;
    }

    public void setInterior_seen_index(int interior_seen_index) {
        this.interior_seen_index = interior_seen_index;
    }

    public String getExterior_seen_value() {
        return exterior_seen_value;
    }

    public void setExterior_seen_value(String exterior_seen_value) {
        this.exterior_seen_value = exterior_seen_value;
    }

    public String getTcp1() {
        return tcp1;
    }

    public void setTcp1(String tcp1) {
        this.tcp1 = tcp1;
    }

    public String getTcp2() {
        return tcp2;
    }

    public void setTcp2(String tcp2) {
        this.tcp2 = tcp2;
    }

    public int getName_and_job_conf1() {
        return name_and_job_conf1;
    }

    public void setName_and_job_conf1(int name_and_job_conf1) {
        this.name_and_job_conf1 = name_and_job_conf1;
    }

    public String getName_and_job_conf1_value() {
        return name_and_job_conf1_value;
    }

    public void setName_and_job_conf1_value(String name_and_job_conf1_value) {
        this.name_and_job_conf1_value = name_and_job_conf1_value;
    }

    public int getName_and_job_conf2() {
        return name_and_job_conf2;
    }

    public void setName_and_job_conf2(int name_and_job_conf2) {
        this.name_and_job_conf2 = name_and_job_conf2;
    }

    public String getName_and_job_conf2_value() {
        return name_and_job_conf2_value;
    }

    public void setName_and_job_conf2_value(String name_and_job_conf2_value) {
        this.name_and_job_conf2_value = name_and_job_conf2_value;
    }

    public String getResiding_last() {
        return Residing_last;
    }

    public void setResiding_last(String Residing_last) {
        this.Residing_last = Residing_last;
    }

    public String getResiding_last2() {
        return Residing_last2;
    }

    public void setResiding_last2(String Residing_last2) {
        this.Residing_last2 = Residing_last2;
    }

    public String getGate_color() {
        return gate_color;
    }

    public void setGate_color(String gate_color) {
        this.gate_color = gate_color;
    }

    public String getWall_color() {
        return wall_color;
    }

    public void setWall_color(String wall_color) {
        this.wall_color = wall_color;
    }

    public String getExecutive_comment() {
        return executive_comment;
    }

    public void setExecutive_comment(String executive_comment) {
        this.executive_comment = executive_comment;
    }

    public String getPermanent_address() {
        return permanent_address;
    }

    public void setPermanent_address(String permanent_address) {
        this.permanent_address = permanent_address;
    }

    public String getOffice_address() {
        return office_address;
    }

    public void setOffice_address(String office_address) {
        this.office_address = office_address;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation
            (String relation) {
        this.relation = relation;
    }

    public String getStatusMarried() {
        return mar_status_value;
    }

    public void setStatusMarried
            (String mar_status_value) {
        this.mar_status_value = mar_status_value;
    }
    public int getStatusMarriedindex() {
        return mar_satus;
    }

    public void setStatusMarriedindex(int mar_satus) {
        this.mar_satus = mar_satus;
    }



    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }
}
