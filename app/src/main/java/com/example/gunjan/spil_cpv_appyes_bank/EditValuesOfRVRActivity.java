package com.example.gunjan.spil_cpv_appyes_bank;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.gunjan.spil_cpv_appyes_bank.models.UpdateResponseforRVR;
import com.example.gunjan.spil_cpv_appyes_bank.models.UserDataBeenRVR;
import com.example.gunjan.spil_cpv_appyes_bank.models.ViewDataEntryForRvr;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Gunjan on 11-11-2016.
 */
public class EditValuesOfRVRActivity extends AppCompatActivity {

    private static final String TAG = EditValuesOfRVRActivity.class.getSimpleName();
    private static EditValuesOfRVRActivity mInstance;
    private static OnUpdateListener mListener;
    private final UserDataBeenRVR instance;
    @Bind(R.id.edtexecutivecode)
    TextView edtexecutivecode;
    @Bind(R.id.textView3)
    TextView textView3;
    @Bind(R.id.edtloanno)
    TextView edtloanno;
    @Bind(R.id.txtfordate)
    TextView txtfordate;
    @Bind(R.id.textView5)
    TextView textView5;
    @Bind(R.id.txtfortime)
    TextView txtfortime;
    @Bind(R.id.edtAppname)
    TextView edtAppname;
    @Bind(R.id.edtFordob)
    EditText edtFordob;
    @Bind(R.id.textView6)
    TextView textView6;
    @Bind(R.id.rbFirstFragConfirm)
    RadioButton rbFirstFragConfirm;
    @Bind(R.id.radionoaddconfirm)
    RadioButton radionoaddconfirm;
    @Bind(R.id.radiononeaddconfirm)
    RadioButton radiononeaddconfirm;
    @Bind(R.id.rgAddconfirm)
    RadioGroup rgAddconfirm;
    @Bind(R.id.textView7)
    TextView textView7;
    @Bind(R.id.txtforlandmrak)
    EditText txtforlandmrak;
    @Bind(R.id.textView8)
    TextView textView8;
    @Bind(R.id.txtmetwith)
    EditText txtmetwith;
    @Bind(R.id.edtForrelation)
    EditText edtForrelation;
    @Bind(R.id.textView4)
    TextView textView4;
    @Bind(R.id.rbformarrid)
    RadioButton rbformarrid;
    @Bind(R.id.rbforunmarrid)
    RadioButton rbforunmarrid;
    @Bind(R.id.rgforrelation)
    RadioGroup rgforrelation;
    @Bind(R.id.txtforfamilymember)
    EditText txtforfamilymember;
    @Bind(R.id.textView9)
    TextView textView9;
    @Bind(R.id.edtforEarning_Member)
    EditText edtforEarningMember;
    @Bind(R.id.Located)
    TextView Located;
    @Bind(R.id.radiobuttonforactivitylocate)
    RadioButton radiobuttonforactivitylocate;
    @Bind(R.id.radionotseenactivitylocate)
    RadioButton radionotseenactivitylocate;
    @Bind(R.id.activitylocate)
    RadioGroup activitylocate;
    @Bind(R.id.textView10)
    TextView textView10;
    @Bind(R.id.spinnerforeduction)
    Spinner spinnerforeduction;
    @Bind(R.id.edtForYear_of_current_Resi)
    EditText edtForYearOfCurrentResi;
    @Bind(R.id.edtforYears_in_city)
    EditText edtforYearsInCity;
    @Bind(R.id.textView)
    TextView textView;
    @Bind(R.id.edtforidno)
    EditText edtforidno;
    @Bind(R.id.spinnerforResident_Status)
    Spinner spinnerforResidentStatus;
    @Bind(R.id.spinnerforLocality)
    Spinner spinnerforLocality;
    @Bind(R.id.txtformobileno)
    EditText txtformobileno;
    @Bind(R.id.txtforareaaquarefeet)
    EditText txtforareaaquarefeet;
    @Bind(R.id.rbforhouseflat)
    RadioButton rbforhouseflat;
    @Bind(R.id.rbforhouseIndependent)
    RadioButton rbforhouseIndependent;
    @Bind(R.id.rgfortypeofhouse)
    RadioGroup rgfortypeofhouse;
    @Bind(R.id.locality)
    TextView locality;
    @Bind(R.id.spinnerforresidentcons)
    Spinner spinnerforresidentcons;
    @Bind(R.id.exterior)
    TextView exterior;
    @Bind(R.id.radioexteriorgood)
    RadioButton radioexteriorgood;
    @Bind(R.id.radioexteriorpoor)
    RadioButton radioexteriorpoor;
    @Bind(R.id.radioexterioravergare)
    RadioButton radioexterioravergare;
    @Bind(R.id.radioexterior)
    RadioGroup radioexterior;
    @Bind(R.id.spinnerforinteriorseen)
    Spinner spinnerforinteriorseen;
    @Bind(R.id.txtfortcp1)
    EditText txtfortcp1;
    @Bind(R.id.radionamejobyes)
    RadioButton radionamejobyes;
    @Bind(R.id.radionamejobNo)
    RadioButton radionamejobNo;
    @Bind(R.id.radionamejobnone)
    RadioButton radionamejobnone;
    @Bind(R.id.radionamejob)
    RadioGroup radionamejob;
    @Bind(R.id.edtforresidinglast1)
    EditText edtforresidinglast1;
    @Bind(R.id.txtfortcp2)
    EditText txtfortcp2;
    @Bind(R.id.radionamejobyes1)
    RadioButton radionamejobyes1;
    @Bind(R.id.radionamejobNo1)
    RadioButton radionamejobNo1;
    @Bind(R.id.radionamejobnone1)
    RadioButton radionamejobnone1;
    @Bind(R.id.radionamejob1)
    RadioGroup radionamejob1;
    @Bind(R.id.edtforresidinglast2)
    EditText edtforresidinglast2;
    @Bind(R.id.txtforgatecolor)
    EditText txtforgatecolor;
    @Bind(R.id.txtforwallcolor)
    EditText txtforwallcolor;
    @Bind(R.id.edtforexecomment)
    EditText edtforexecomment;
    @Bind(R.id.edtforpermanentaddress)
    EditText edtforpermanentaddress;
    @Bind(R.id.edtforofficeandbussiaddressaddress)
    EditText edtforofficeandbussiaddressaddress;
    @Bind(R.id.edtforDesignation)
    EditText edtforDesignation;
    @Bind(R.id.right_nav)
    Button rightNav;
    @Bind(R.id.scrollView)
    ScrollView scrollView;
    private int addconfirm_index;
    private int married_status_index;
    private int located_index;
    private int education_index;
    private int reident_status_index;
    private int locality_index;
    private int type_of_hous_index;
    private int exterior_index;
    private int interior_seen_index;
    private int name_conf_index;
    private int job_conf_index;
    private String cpvid;
    private String typeforbvrrvr;

    public EditValuesOfRVRActivity() {
        // Required empty public constructor
        instance = UserDataBeenRVR.getInstance();
    }

    public static EditValuesOfRVRActivity getInstance() {
        if (mInstance == null) {
            mInstance = new EditValuesOfRVRActivity();
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.data_edit_rvr_activity);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        ViewDataEntryForRvr.UserDataBean model = (ViewDataEntryForRvr.UserDataBean) intent.getSerializableExtra(TAGS.KEY_USERDATA);
        cpvid = intent.getStringExtra(TAGS.KEY_CPVID);
        typeforbvrrvr=intent.getStringExtra(TAGS.KEY_TYPE);

        addconfirm_index = model.getAddconfirm_index();
        married_status_index = model.getMarried_status_index();
        located_index = model.getLocated_index();
        education_index = model.getEducation_index();
        reident_status_index = model.getReident_status_index();
        locality_index = model.getLocality_index();
        type_of_hous_index = model.getType_of_hous_index();
        exterior_index = model.getExterior_index();
        interior_seen_index = model.getInterior_seen_index();
        name_conf_index = model.getName_conf_index();
        job_conf_index = model.getJob_conf_index();


        //address confirm
        rbFirstFragConfirm = (RadioButton) rgAddconfirm.getChildAt(addconfirm_index);
        rbFirstFragConfirm.setChecked(true);
        instance.setAddConfirm(rbFirstFragConfirm.getText().toString());
        instance.setAddconfirm_index(addconfirm_index);

        //for located
        radiobuttonforactivitylocate = (RadioButton) activitylocate.getChildAt(located_index);
        radiobuttonforactivitylocate.setChecked(true);
        instance.setLoacate(radiobuttonforactivitylocate.getText().toString());
        instance.setLocated_index(located_index);

        //for Married
        rbformarrid = (RadioButton) rgforrelation.getChildAt(married_status_index);
        rbformarrid.setChecked(true);
        instance.setMarried_status(rbformarrid.getText().toString());
        instance.setMarried_status_index(married_status_index);

        //for typeofhouse
        rbforhouseflat = (RadioButton) rgfortypeofhouse.getChildAt(type_of_hous_index);
        rbforhouseflat.setChecked(true);
        instance.setTypeOfHouse(rbforhouseflat.getText().toString());
        instance.setType_of_hous_index(type_of_hous_index);


        //for job
        radionamejobyes1 = (RadioButton) radionamejob1.getChildAt(job_conf_index);
        radionamejobyes1.setChecked(true);
        instance.setJobConfirm(radionamejobyes1.getText().toString());
        instance.setJob_conf_index(job_conf_index);

        //for name
        radionamejobyes = (RadioButton) radionamejob.getChildAt(name_conf_index);
        radionamejobyes.setChecked(true);
        instance.setNameConfirm(radionamejobyes.getText().toString());
        instance.setName_conf_index(name_conf_index);

        //for exterior
        radioexterioravergare = (RadioButton) radioexterior.getChildAt(exterior_index);
        radioexterioravergare.setChecked(true);
        instance.setExterior(radioexterioravergare.getText().toString());
        instance.setExterior_index(exterior_index);


        setEditTextValues(model);

        //for all radio groups ie address confirm,located
        radioMethod();
        //for all spinners ie eductaion
        spinerMethod();
    }

    private void setEditTextValues(ViewDataEntryForRvr.UserDataBean model) {
        txtfortcp1.setText(model.getTpc1());
        txtformobileno.setText(model.getMobileNo());
        txtforareaaquarefeet.setText(model.getAreaSqft());
        exterior.setText(model.getExterior());
        edtforresidinglast1 .setText(model.getResidingLast1());
        txtfortcp2 .setText(model.getTpc2());
        edtforresidinglast2 .setText(model.getResidingLast2());
        txtforgatecolor.setText(model.getGateColor());
        txtforwallcolor .setText(model.getWallColor());
        edtforexecomment .setText(model.getExeComment());
        edtforpermanentaddress.setText(model.getPermAddress());
        edtforofficeandbussiaddressaddress.setText(model.getOffAddress());
        edtforidno.setText(model.getIDNo());
        edtforYearsInCity.setText(model.getYearsInCity());
        edtforDesignation.setText(model.getDesignation());
        edtexecutivecode.setText(model.getExecutive());
        edtloanno.setText(model.getLoanNo());
        txtfordate.setText(model.getDates());
        txtfortime.setText(model.getTimes());
        edtAppname .setText(model.getAppName());
        txtforlandmrak.setText(model.getLandmark());
        edtForYearOfCurrentResi.setText(model.getYrOfCurrResidence());
        txtmetwith .setText(model.getMetWith());
        edtForrelation.setText(model.getRelation());
        txtforfamilymember .setText(model.getFamilyMember());
        edtforEarningMember.setText(model.getEarningMember());
        Located.setText(model.getLoacate());
        edtFordob.setText(model.getDOB());
    }

    private void spinerMethod() {
        //eduction
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.Eduction_array,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforeduction.setAdapter(adapter);
        spinnerforeduction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                instance.setEducation_index(position);
                instance.setEducation(spinnerforeduction.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //locality
        ArrayAdapter<CharSequence> adapterforlocality = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.Locality_array_forrvr,
                android.R.layout.simple_spinner_item);
        adapterforlocality.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforLocality.setAdapter(adapterforlocality);
        spinnerforLocality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                instance.setLocality_index(position);
                instance.setLaoclity(spinnerforLocality.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //resident status
        ArrayAdapter<CharSequence> adapterforresidentstatus = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.resident_status_forrvr,
                android.R.layout.simple_spinner_item);
        adapterforresidentstatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforResidentStatus.setAdapter(adapterforresidentstatus);
        spinnerforResidentStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                instance.setReident_status_index(position);
                instance.setResStatus(spinnerforResidentStatus.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //resident construction
        ArrayAdapter<CharSequence> adapterforresidentconstruction = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.resident_construction_forrvr,
                android.R.layout.simple_spinner_item);
        adapterforresidentconstruction.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforresidentcons.setAdapter(adapterforresidentconstruction);
        spinnerforresidentcons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                instance.setReident_status_index(position);
                instance.setResiConstruction(spinnerforresidentcons.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //interior seen
        ArrayAdapter<CharSequence> adapterforinteriorseen = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.InteriorSeen_array_forrvr,
                android.R.layout.simple_spinner_item);
        adapterforinteriorseen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforinteriorseen.setAdapter(adapterforinteriorseen);
        spinnerforinteriorseen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                instance.setReident_status_index(position);
                instance.setResStatus(spinnerforinteriorseen.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void radioMethod() {
        activitylocate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radiobuttonforactivitylocate = (RadioButton) findViewById(checkedId);
                instance.setLoacate(radiobuttonforactivitylocate.getText().toString());
                instance.setLocated_index(activitylocate.indexOfChild(radiobuttonforactivitylocate));
            }
        });
        rgAddconfirm.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                rbFirstFragConfirm = (RadioButton) findViewById(checkedId);
                instance.setAddConfirm(rbFirstFragConfirm.getText().toString());
                instance.setAddconfirm_index(rgAddconfirm.indexOfChild(rbFirstFragConfirm));

            }
        });
        rgforrelation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                rbformarrid = (RadioButton) findViewById(checkedId);
                instance.setMarried_status(rbformarrid.getText().toString());
                instance.setMarried_status_index(rgforrelation.indexOfChild(rbformarrid));

            }
        });
        rgfortypeofhouse.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                rbforhouseflat = (RadioButton) findViewById(checkedId);
                instance.setType_of_hous_index(rgfortypeofhouse.indexOfChild(rbforhouseflat));
                instance.setTypeOfHouse(rbforhouseflat.getText().toString());
            }
        });
        //for job
        radionamejob1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radionamejobyes1 = (RadioButton) findViewById(checkedId);
                instance.setName_conf_index(radionamejob1.indexOfChild(radionamejobyes1));
                instance.setNameConfirm(radionamejobyes1.getText().toString());
            }
        });
        //for name
        radionamejob.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radionamejobyes = (RadioButton) findViewById(checkedId);
                instance.setJob_conf_index(radionamejob.indexOfChild(radionamejobyes));
                instance.setJobConfirm(radionamejobyes.getText().toString());
            }
        });
        //for exterior
        radioexterior.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radioexteriorgood = (RadioButton) findViewById(checkedId);
                instance.setExterior_index(radioexterior.indexOfChild(radioexteriorgood));
                instance.setExterior(radioexteriorgood.getText().toString());
            }
        });
    }


    @OnClick(R.id.right_nav)
    public void onClick() {
        setFormData();
        updateToServer();

    }

    private void updateToServer() {

        final String formDataURL = API.getUpdateURLRVR(instance);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, formDataURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //  Toast.makeText(getContext(),  response.toString() , Toast.LENGTH_LONG).show();
                Gson gson = new Gson();
                UpdateResponseforRVR updateResponse = gson.fromJson(response.toString(), UpdateResponseforRVR.class);
                final List<String> status = updateResponse.getStatus();
                String message = null;
                // retrieving data from string list array in for loop
                for (int i = 0; i < status.size(); i++) {
                    Log.i("Value of element " + i, status.get(i));
                    message = status.get(i);
                }
                if (message.equalsIgnoreCase("Success")) {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                    if(mListener!=null){
                        mListener.onUpdate();
                    }
                    finish();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(getApplicationContext(), "--" + "Error" + error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });
        App.getInstance().addToRequestQueue(request);
    }

    private void setFormData() {
        instance.setCPVId(cpvid);
        instance.setType(typeforbvrrvr);
        instance.setDates(txtfordate.getText().toString());
        instance.setTimes(txtfortime.getText().toString());
        instance.setLoanNo(edtloanno.getText().toString());
        instance.setExecutive(edtexecutivecode.getText().toString());
        instance.setLandmark(txtforlandmrak.getText().toString());
        instance.setAppName(edtAppname.getText().toString());
        instance.setMetWith(txtmetwith.getText().toString());
        instance.setFamilyMember(txtforfamilymember.getText().toString());
        instance.setEarningMember(edtforEarningMember.getText().toString());
        instance.setDOB(edtFordob.getText().toString());
        instance.setRelation(edtForrelation.getText().toString());
        instance.setMobileNo(txtformobileno.getText().toString());
        instance.setAreaSqft(txtforareaaquarefeet.getText().toString());
        instance.setYrOfCurrResidence(edtForYearOfCurrentResi.getText().toString());
        instance.setYearsInCity(edtforYearsInCity.getText().toString());
        instance.setIDNo(edtforidno.getText().toString());
        instance.setTpc1(txtfortcp1.getText().toString());
        instance.setResidingLast1(edtforresidinglast1.getText().toString());
        instance.setTpc2(txtfortcp2.getText().toString());
        instance.setResidingLast2(edtforresidinglast2.getText().toString());
        instance.setGateColor(txtforgatecolor.getText().toString());
        instance.setWallColor(txtforwallcolor.getText().toString());
        instance.setExeComment(edtforexecomment.getText().toString());
        instance.setOffAddress(edtforofficeandbussiaddressaddress.getText().toString());
        instance.setPermAddress(edtforpermanentaddress.getText().toString());
        instance.setDesignation(edtforDesignation.getText().toString());

        final String jsonFormData = App.getInstance().getGSON().toJson(instance);
        App.getInstance().getPreferencesEditor().putString(TAGS.JSON_FORM_DATA_RVR, jsonFormData).commit();
    }

    //Refreshing the page
    public static void setOnUpdateListener(EditValuesOfRVRActivity.OnUpdateListener onUpdateListener) {

        mListener=onUpdateListener;
    }
    interface OnUpdateListener {
        void onUpdate();
    }
}
