package com.example.gunjan.spil_cpv_appyes_bank;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.gunjan.spil_cpv_appyes_bank.models.UpdateResponse;
import com.example.gunjan.spil_cpv_appyes_bank.models.UserDataBean;
import com.example.gunjan.spil_cpv_appyes_bank.models.ViewDataEntryForBVR;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Gunjan on 10-11-2016.
 */

public class EditValuesOfBVRActivity extends AppCompatActivity {
    private static final String TAG = EditValuesOfBVRActivity.class.getSimpleName();
    private static EditValuesOfBVRActivity mInstance;
    private static OnUpdateListener mListener;
    private final UserDataBean instance;
    @Bind(R.id.edtexecutivecode)
    TextView edtexecutivecode;
    @Bind(R.id.edtloanno)
    TextView edtloanno;
    @Bind(R.id.txtfordate)
    TextView txtfordate;
    @Bind(R.id.txtfortime)
    TextView txtfortime;
    @Bind(R.id.edtAppname)
    TextView edtAppname;
    @Bind(R.id.edtFordob)
    EditText edtFordob;
    @Bind(R.id.rbFirstFragConfirm)
    RadioButton rbFirstFragConfirm;
    @Bind(R.id.radionoaddconfirm)
    RadioButton radionoaddconfirm;
    @Bind(R.id.radiononeaddconfirm)
    RadioButton radiononeaddconfirm;
    @Bind(R.id.rgAddconfirm)
    RadioGroup rgAddconfirm;

    @Bind(R.id.txtforlandmrak)
    EditText txtforlandmrak;
    @Bind(R.id.txtdesignation)
    EditText txtdesignation;
    @Bind(R.id.txtdepartment)
    EditText txtdepartment;
    @Bind(R.id.txtmetwith)
    EditText txtmetwith;
    @Bind(R.id.txtforDesignate)
    EditText txtforDesignate;
    @Bind(R.id.Located)

    TextView Located;
    @Bind(R.id.radiobuttonforactivitylocate)
    RadioButton radiobuttonforactivitylocate;
    @Bind(R.id.radionotseenactivitylocate)
    RadioButton radionotseenactivitylocate;
    @Bind(R.id.activitylocate)
    RadioGroup activitylocate;

    @Bind(R.id.spinnerforeduction)
    Spinner spinnerforeduction;
    @Bind(R.id.txtforworkingsince)
    EditText txtforworkingsince;

    @Bind(R.id.companystatusyes)
    RadioButton companystatusyes;
    @Bind(R.id.companystatusno)
    RadioButton companystatusno;
    @Bind(R.id.companystatus)
    RadioGroup companystatus;

    @Bind(R.id.txtforemployid)
    EditText txtforemployid;
    @Bind(R.id.txtfornature)
    EditText txtfornature;
    @Bind(R.id.txtformobileno)
    EditText txtformobileno;
    @Bind(R.id.txtfornoofemployeeseen)
    EditText txtfornoofemployeeseen;

    @Bind(R.id.companynameboardseenyes)
    RadioButton companynameboardseenyes;
    @Bind(R.id.companynameboardseenno)
    RadioButton companynameboardseenno;
    @Bind(R.id.companynameboardseennone)
    RadioButton companynameboardseennone;
    @Bind(R.id.companynameboardseen)
    RadioGroup companynameboardseen;

    @Bind(R.id.txtforareaaquarefeet)
    EditText txtforareaaquarefeet;

    @Bind(R.id.busnessactivityseenyes)
    RadioButton busnessactivityseenyes;
    @Bind(R.id.busnessactivityseenno)
    RadioButton busnessactivityseenno;
    @Bind(R.id.busnessactivityseennone)
    RadioButton busnessactivityseennone;
    @Bind(R.id.busnessactivityseen)
    RadioGroup busnessactivityseen;

    @Bind(R.id.stockseenyes)
    RadioButton stockseenyes;
    @Bind(R.id.stockseenseenno)
    RadioButton stockseenseenno;
    @Bind(R.id.stockseennone)
    RadioButton stockseennone;
    @Bind(R.id.stockseen)
    RadioGroup stockseen;

    @Bind(R.id.txtfortypeofstock)
    EditText txtfortypeofstock;
    @Bind(R.id.locality)
    TextView locality;
    @Bind(R.id.spinnerforLocality)
    Spinner spinnerforLocality;
    @Bind(R.id.exterior)
    TextView exterior;
    @Bind(R.id.radioexteriorgood)
    RadioButton radioexteriorgood;
    @Bind(R.id.radioexteriorpoor)
    RadioButton radioexteriorpoor;
    @Bind(R.id.radioexterioravergare)
    RadioButton radioexterioravergare;
    @Bind(R.id.radioexterior)
    RadioGroup radioexterior;
    @Bind(R.id.ch_privacy)
    CheckBox chPrivacy;
    @Bind(R.id.exteriorseen)
    TextView exteriorseen;
    @Bind(R.id.edtexteriorseen)
    EditText edtexteriorseen;
    @Bind(R.id.layout)
    LinearLayout layout;
    @Bind(R.id.txtfortcp1)
    EditText txtfortcp1;
    @Bind(R.id.radionamejobyes)
    RadioButton radionamejobyes;
    @Bind(R.id.radionamejobNo)
    RadioButton radionamejobNo;
    @Bind(R.id.radionamejobnone)
    RadioButton radionamejobnone;
    @Bind(R.id.radionamejob)
    RadioGroup radionamejob;
    @Bind(R.id.txtforworkinglast)
    EditText txtforworkinglast;
    @Bind(R.id.txtfortcp2)
    EditText txtfortcp2;
    @Bind(R.id.radionamejobyes1)
    RadioButton radionamejobyes1;
    @Bind(R.id.radionamejobNo1)
    RadioButton radionamejobNo1;
    @Bind(R.id.radionamejobnone1)
    RadioButton radionamejobnone1;
    @Bind(R.id.radionamejob1)
    RadioGroup radionamejob1;
    @Bind(R.id.txtforworkinglast1)
    EditText txtforworkinglast1;
    @Bind(R.id.txtforgatecolor)
    EditText txtforgatecolor;
    @Bind(R.id.txtforwallcolor)
    EditText txtforwallcolor;
    @Bind(R.id.txtforExecutiveComment)
    EditText txtforExecutiveComment;
    @Bind(R.id.txtforanyupdate)
    EditText txtforanyupdate;
    @Bind(R.id.txtforresidentadd)
    EditText txtforresidentadd;
    @Bind(R.id.right_nav)
    Button rightNav;

    private int education_index;
    private int located;
    private int instanceLocality;
    private int address_confirm;
    private int co_status;
    private int company_board_seen;
    private int business_activity_seen;
    private int stock_seen_index;
    private int name_and_job_conf1;
    private int name_and_job_conf2;
    private int instanceExterior;
    private String cpvid;
    private String type;

    public EditValuesOfBVRActivity() {
        // Required empty public constructor
         instance = UserDataBean.getInstance();
    }
    public static EditValuesOfBVRActivity getInstance() {
        if (mInstance == null) {
            mInstance = new EditValuesOfBVRActivity();
        }
        return mInstance;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.data_edit_bvr_activity);
        ButterKnife.bind(this);
        Intent intent=getIntent();
        ViewDataEntryForBVR.UserDataBean model = (ViewDataEntryForBVR.UserDataBean) intent.getSerializableExtra(TAGS.KEY_USERDATA);
        cpvid = intent.getStringExtra(TAGS.KEY_CPVID);
        type=intent.getStringExtra(TAGS.KEY_TYPE);

        address_confirm=model.getAddconfirm_index();
        education_index = model.getEducatin_index();
        located = model.getLocated_index();
        instanceLocality = model.getLocated_index();
        address_confirm = model.getAddconfirm_index();
        co_status = model.getCostatus_index();
        instanceExterior = model.getExterior_index();
        company_board_seen = model.getBoardseen_index();
        business_activity_seen = model.getActivityseen_index();
        stock_seen_index = model.getStockseen_index();
        name_and_job_conf1 = model.getName_conf_index();
        name_and_job_conf2 = model.getJob_conf_index();


        //address confirm
        rbFirstFragConfirm = (RadioButton) rgAddconfirm.getChildAt(address_confirm);
        rbFirstFragConfirm.setChecked(true);
        instance.setAddConfirm(rbFirstFragConfirm.getText().toString());
        instance.setAddconfirm_index(address_confirm);
        //for located
        radiobuttonforactivitylocate = (RadioButton)activitylocate.getChildAt(located);
        radiobuttonforactivitylocate.setChecked(true);
        instance.setLocated(radiobuttonforactivitylocate.getText().toString());
        instance.setLocated_index(located);
        //for exterior
        radioexteriorgood = (RadioButton)radioexterior.getChildAt(instanceExterior);
        radioexteriorgood.setChecked(true);
        instance.setExterior_index(instanceExterior);
        instance.setExterior(radioexteriorgood.getText().toString());
        //for company status
        companystatusyes = (RadioButton)companystatus.getChildAt(co_status) ;
        companystatusyes.setChecked(true);
        instance.setCostatus_index(co_status);
        instance.setCoStatus(companystatusyes.getText().toString());
        //forstock seen
        stockseenyes = (RadioButton)stockseen.getChildAt(stock_seen_index);
        stockseenyes.setChecked(true);
        instance.setStockseen_index(stock_seen_index);
        instance.setStockSeen(stockseenyes.getText().toString());
        //for name conf.
        radionamejobyes = (RadioButton)radionamejob.getChildAt(name_and_job_conf1);
        radionamejobyes.setChecked(true);
        instance.setName_conf_index(name_and_job_conf1);
        instance.setNameConfirm(radionamejobyes.getText().toString());
        //for job conf.
        radionamejobyes1 = (RadioButton)radionamejob1.getChildAt(name_and_job_conf2);
        radionamejobyes1.setChecked(true);
        instance.setJob_conf_indext(name_and_job_conf2);
        instance.setJobConfirm(radionamejobyes1.getText().toString());
        //for company board seen
        companynameboardseenyes = (RadioButton) companynameboardseen.getChildAt(company_board_seen);
        companynameboardseenyes.setChecked(true);
        instance.setBoardseen_index(company_board_seen);
        instance.setBoardSeen(companynameboardseenyes.getText().toString());
    /*

        //for bussiness activity seen
        busnessactivityseenyes = (RadioButton)busnessactivityseen.getChildAt(business_activity_seen);
        busnessactivityseenyes.setChecked(true);
        instance.setActivityseen_index(business_activity_seen);
        instance.setActivitySeen(busnessactivityseenyes.getText().toString());*/


       setEditTextValues(model);
        spinnerMethod();
        radioMethod();

    }

    private void radioMethod() {
        //for addressconfirm

        rgAddconfirm.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                rbFirstFragConfirm = (RadioButton) findViewById(checkedId);
                int index = rgAddconfirm.indexOfChild(rbFirstFragConfirm);
                instance.setAddConfirm(rbFirstFragConfirm.getText().toString());
                instance.setAddconfirm_index(index);
            }
        });
        //Exterior
        radioexterior.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radioexteriorgood = (RadioButton) findViewById(checkedId);
                instance.setExterior_index(radioexterior.indexOfChild(radioexteriorgood));
                instance.setExterior(radioexteriorgood.getText().toString());
            }
        });

        //for located
        activitylocate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radiobuttonforactivitylocate = (RadioButton) findViewById(checkedId);
                instance.setLocated(radiobuttonforactivitylocate.getText().toString());
                instance.setLocated_index(activitylocate.indexOfChild(radiobuttonforactivitylocate));
            }
        });
        //for company status
        companystatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                companystatusyes = (RadioButton) findViewById(checkedId);
                instance.setCostatus_index(companystatus.indexOfChild(companystatusyes));
                instance.setCoStatus(companystatusyes.getText().toString());
            }
        });
        //for stock seen
        stockseen.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                stockseenyes = (RadioButton) findViewById(checkedId);
                instance.setStockseen_index(stockseen.indexOfChild(stockseenyes));
                instance.setStockSeen(stockseenyes.getText().toString());

            }
        });

        //for name conf.
        radionamejob.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radionamejobyes = (RadioButton) findViewById(checkedId);
                instance.setName_conf_index(radionamejob.indexOfChild(radionamejobyes));
                instance.setNameConfirm(radionamejobyes.getText().toString());
            }
        });

        //for job conf.
        radionamejob1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radionamejobyes1 = (RadioButton)findViewById(checkedId);
                instance.setJob_conf_indext(radionamejob1.indexOfChild(radionamejobyes1));
                instance.setJobConfirm(radionamejobyes1.getText().toString());
            }
        });

        //for company board seen
        companynameboardseen.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                companynameboardseenyes = (RadioButton)findViewById(checkedId);
                instance.setBoardseen_index(companynameboardseen.indexOfChild(companynameboardseenyes));
                instance.setBoardSeen(companynameboardseenyes.getText().toString());
            }
        });


      /*  //for activity seen
        busnessactivityseen.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                busnessactivityseenyes = (RadioButton)findViewById(checkedId);
                instance.setActivityseen_index(checkedId);
                instance.setActivitySeen(busnessactivityseenyes.getText().toString());

            }
        });*/

    }

    private void spinnerMethod() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.Eduction_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforeduction.setAdapter(adapter);
        spinnerforeduction.setSelection(education_index);
        spinnerforeduction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                instance.setEducatin_index(position);
                instance.setEducation( spinnerforeduction.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //for locality
        ArrayAdapter<CharSequence> adapterLocality_array = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.Locality_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapterLocality_array.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinnerforlocated
        spinnerforLocality.setAdapter(adapterLocality_array);
        spinnerforLocality.setSelection(instanceLocality);
        spinnerforLocality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              //  instance.setLocality(position);
                instance.setBussinessCenter(spinnerforLocality.getSelectedItem().toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setEditTextValues(ViewDataEntryForBVR.UserDataBean model) {
        txtforresidentadd.setText(model.getResAddress());
        edtexteriorseen.setText(model.getEntryPermittedSeen());
        txtfortypeofstock.setText(model.getTypeOfStock());
        txtfortcp1.setText(model.getTPC1());
        edtexecutivecode.setText(model.getExecutive());
        txtforworkinglast.setText(model.getWorkingLast1());
        txtforworkingsince.setText(model.getWorkingSince());
        txtforemployid.setText(model.getEmployeeID());
        txtfortcp2.setText(model.getTPC2());
        txtforworkinglast1.setText(model.getWorkingLast2());
        txtforgatecolor.setText(model.getGateColor());
        txtforwallcolor.setText(model.getWallColor());
        txtforExecutiveComment.setText(model.getExeComment());
        txtforanyupdate.setText(model.getAnyUpdation());
        txtforareaaquarefeet.setText(model.getAreaSqft());
        txtfornoofemployeeseen.setText(model.getNumberOfEmpSeen());
        edtloanno.setText(model.getLoanNo());
        txtfordate.setText(model.getLoanDate());
        txtfortime.setText(model.getLoanTime());
        edtAppname.setText(model.getAppName());
        edtFordob.setText(model.getDOB());
        txtforlandmrak.setText(model.getLandMark());
        txtdesignation.setText(model.getDesignation());
        txtdepartment.setText(model.getDepartment());
        txtmetwith.setText(model.getMetWith());
        txtforDesignate.setText(model.getApplicantDesignation());
        txtfornature.setText(model.getNatureOfBuss());
        txtformobileno.setText(model.getMobile());
  }

    @OnClick(R.id.right_nav)
    public void onClick() {
    setFormData();
    updateToServer();

    }

    private void updateToServer() {
        final String formDataURL = API.getUpdateURL(instance);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, formDataURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //  Toast.makeText(getContext(),  response.toString() , Toast.LENGTH_LONG).show();
                Gson gson = new Gson();
                UpdateResponse updateResponse = gson.fromJson(response.toString(), UpdateResponse.class);
                final List<String> status = updateResponse.getStatus();
                String message = null;
                // retrieving data from string list array in for loop
                for (int i = 0; i < status.size(); i++) {
                    Log.i("Value of element " + i, status.get(i));
                    message = status.get(i);

                }
                if (message.equalsIgnoreCase("Success")) {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                    if(mListener!=null){
                        mListener.onUpdate();
                    }
                    finish();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(getApplicationContext(), "--" + "Error" + error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });
        App.getInstance().addToRequestQueue(request);

    }

    private void setFormData() {
        instance.setLoanDate(txtfordate.getText().toString());
        instance.setCPVId(cpvid);
        instance.setType(type);
        instance.setLoanTime(txtfortime.getText().toString());
        instance.setLoanNo(edtloanno.getText().toString());
        instance.setDOB(edtFordob.getText().toString());
        instance.setExecutive(edtexecutivecode.getText().toString());
        instance.setAppName(edtAppname.getText().toString());
        instance.setLandMark(txtforlandmrak.getText().toString());
        instance.setDesignation(txtdesignation.getText().toString());
        instance.setDepartment(txtdepartment.getText().toString());
        instance.setMetWith(txtmetwith.getText().toString());
        instance.setApplicantDesignation(txtforDesignate.getText().toString());

        instance.setWorkingSince(txtforworkingsince.getText().toString());
        instance.setEmployeeID(txtforemployid.getText().toString());
        instance.setNatureOfBuss(txtfornature.getText().toString());
        instance.setMobile(txtformobileno.getText().toString());
        instance.setNumberOfEmpSeen(txtfornoofemployeeseen.getText().toString());
        instance.setTypeOfStock(txtfortypeofstock.getText().toString());
        instance.setAreaSqft(txtforareaaquarefeet.getText().toString());

        instance.setTPC1(txtfortcp1.getText().toString());
        instance.setWorkingLast1(txtforworkinglast.getText().toString());
        instance.setTPC2(txtfortcp2.getText().toString());
        instance.setWorkingLast2(txtforworkinglast1.getText().toString());
        instance.setGateColor(txtforgatecolor.getText().toString());
        instance.setWallColor(txtforwallcolor.getText().toString());
        instance.setExeComment(txtforExecutiveComment.getText().toString());
        instance.setEntryPermittedSeen(edtexteriorseen.getText().toString());

        instance.setAnyUpdation(txtforanyupdate.getText().toString());
        instance.setResAddress(txtforresidentadd.getText().toString());

        final String jsonFormData = App.getInstance().getGSON().toJson(instance);
        App.getInstance().getPreferencesEditor().putString(TAGS.JSON_FORM_DATA, jsonFormData).commit();


    }

    //Refreshing the page
    public static void setOnUpdateListener(EditValuesOfBVRActivity.OnUpdateListener onUpdateListener) {

        mListener=onUpdateListener;
    }
    interface OnUpdateListener {
        void onUpdate();
    }
}
