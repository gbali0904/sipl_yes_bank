package com.example.gunjan.spil_cpv_appyes_bank;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.gunjan.spil_cpv_appyes_bank.models.ViewDataEntryForBVR;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Gunjan on 10-11-2016.
 */

public class DataViewBVRActivity extends AppCompatActivity {

    public static final String BASE_URL_FOR_VIEW_BVR = "http://survinindia.in/capital/json/ViewDataEntry.php?CPVId=";
    private static final String TAG = DataEntryRVRActivity.class.getSimpleName();
    private static DataViewBVRActivity mInstance;
    private final ViewDataEntryForBVR viewData;

    @Bind(R.id.edtexecutivecode)
    TextView edtexecutivecode;
    @Bind(R.id.edtloanno)
    TextView edtloanno;
    @Bind(R.id.txtfordate)
    TextView txtfordate;
    @Bind(R.id.txtfortime)
    TextView txtfortime;
    @Bind(R.id.edtAppname)
    TextView edtAppname;
    @Bind(R.id.edtFordob)
    TextView edtFordob;
    @Bind(R.id.txtforaddressconfirm)
    TextView txtforaddressconfirm;
    @Bind(R.id.txtforlandmrak)
    TextView txtforlandmrak;
    @Bind(R.id.txtdesignation)
    TextView txtdesignation;
    @Bind(R.id.txtdepartment)
    TextView txtdepartment;
    @Bind(R.id.txtmetwith)
    TextView txtmetwith;
    @Bind(R.id.txtforDesignate)
    TextView txtforDesignate;
    @Bind(R.id.spinnerforeduction)
    TextView spinnerforeduction;
    @Bind(R.id.txtforworkingsince)
    TextView txtforworkingsince;
    @Bind(R.id.txforcoststus)
    TextView txforcoststus;
    @Bind(R.id.txtforemployid)
    TextView txtforemployid;
    @Bind(R.id.txtfornature)
    TextView txtfornature;
    @Bind(R.id.txtformobileno)
    TextView txtformobileno;
    @Bind(R.id.txtfornoofemployeeseen)
    TextView txtfornoofemployeeseen;
    @Bind(R.id.txtforcomboardseen)
    TextView txtforcomboardseen;
    @Bind(R.id.txtforareaaquarefeet)
    TextView txtforareaaquarefeet;
    @Bind(R.id.txtforbussactivityseen)
    TextView txtforbussactivityseen;
    @Bind(R.id.txtforstockseen)
    TextView txtforstockseen;
    @Bind(R.id.txtfortypeofstock)
    TextView txtfortypeofstock;
    @Bind(R.id.spinnerforLocality)
    TextView spinnerforLocality;
    @Bind(R.id.txtforexterior)
    TextView txtforexterior;
    @Bind(R.id.edtexteriorseen)
    TextView edtexteriorseen;
    @Bind(R.id.txtfortcp1)
    TextView txtfortcp1;
    @Bind(R.id.txtfornamecnf)
    TextView txtfornamecnf;
    @Bind(R.id.txtforworkinglast)
    TextView txtforworkinglast;
    @Bind(R.id.txtfortcp2)
    TextView txtfortcp2;
    @Bind(R.id.txtforjobcnf)
    TextView txtforjobcnf;
    @Bind(R.id.txtforworkinglast1)
    TextView txtforworkinglast1;
    @Bind(R.id.txtforgatecolor)
    TextView txtforgatecolor;
    @Bind(R.id.txtforwallcolor)
    TextView txtforwallcolor;
    @Bind(R.id.txtforExecutiveComment)
    TextView txtforExecutiveComment;
    @Bind(R.id.txtforanyupdate)
    TextView txtforanyupdate;
    @Bind(R.id.txtforresidentadd)
    TextView txtforresidentadd;
    @Bind(R.id.txtforlocated)
    TextView txtforlocated;
    @Bind(R.id.layout)
    LinearLayout layout;
    @Bind(R.id.scrollView)
    ScrollView scrollView;
    private String cpvid;
    private ViewDataEntryForBVR.UserDataBean user_data;
    private String type;

    public DataViewBVRActivity() {
        // Required empty public constructor
        viewData = ViewDataEntryForBVR.getInstance();
    }

    public static DataViewBVRActivity getInstance() {
        if (mInstance == null) {
            mInstance = new DataViewBVRActivity();
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.data_view_bvr_activity);
        ButterKnife.bind(this);
        final Intent intent = getIntent();
        cpvid = intent.getStringExtra(TAGS.KEY_CPVID);
        type= intent.getStringExtra(TAGS.KEY_TYPE);
        viewDataMethodForbvr(cpvid);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewDataEntryForBVR.UserDataBean user_data_pass = user_data;

                Intent intent1 = new Intent(DataViewBVRActivity.this, EditValuesOfBVRActivity.class);
                intent1.putExtra(TAGS.KEY_CPVID, cpvid);
                intent1.putExtra(TAGS.KEY_TYPE,type);
                intent1.putExtra(TAGS.KEY_USERDATA, (Serializable) user_data_pass);
                startActivity(intent1);

            }
        });

        EditValuesOfBVRActivity.setOnUpdateListener(new EditValuesOfBVRActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {
                refreshList();

            }
        });

    }

    private void refreshList() {
        viewDataMethodForbvr(cpvid);
    }


    private void viewDataMethodForbvr(String cpvid) {
        String POST_VALUE = cpvid;
        String FULL_URL_FORBVR = BASE_URL_FOR_VIEW_BVR + POST_VALUE;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, FULL_URL_FORBVR, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = new Gson();
                ViewDataEntryForBVR viewDataEntryForBVR = gson.fromJson(response.toString(), ViewDataEntryForBVR.class);
                final String status = viewDataEntryForBVR.getStatus();
                if (status.equals("true")) {
                    user_data = viewDataEntryForBVR.getUser_data();
                    putValuesInTextBVR(user_data);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(getApplicationContext(), "--" + "Error" + error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });

        App.getInstance().addToRequestQueue(request);

    }

    private void putValuesInTextBVR(ViewDataEntryForBVR.UserDataBean user_data) {
        txtforresidentadd.setText(user_data.getResAddress());
        txtforlocated.setText(user_data.getLocated());
        edtexteriorseen.setText(user_data.getEntryPermittedSeen());
        txtforbussactivityseen.setText(user_data.getActivitySeen());
        txtforstockseen.setText(user_data.getStockSeen());
        txtfortypeofstock.setText(user_data.getTypeOfStock());
        spinnerforLocality.setText(user_data.getBussinessCenter());
        txtforexterior.setText(user_data.getExterior());
        txtfortcp1.setText(user_data.getTPC1());
        edtexecutivecode.setText(user_data.getExecutive());
        txtfornamecnf.setText(user_data.getNameConfirm());
        txtforworkinglast.setText(user_data.getWorkingLast1());
        txtforworkingsince.setText(user_data.getWorkingSince());
        txtforemployid.setText(user_data.getEmployeeID());
        txtfortcp2.setText(user_data.getTPC2());
        txtforjobcnf.setText(user_data.getJobConfirm());
        txtforworkinglast1.setText(user_data.getWorkingLast2());
        txtforgatecolor.setText(user_data.getGateColor());
        txtforwallcolor.setText(user_data.getWallColor());
        txtforExecutiveComment.setText(user_data.getExeComment());
        txtforanyupdate.setText(user_data.getAnyUpdation());
        txtforareaaquarefeet.setText(user_data.getAreaSqft());
        txtforcomboardseen.setText(user_data.getBoardSeen());
        txtfornoofemployeeseen.setText(user_data.getNumberOfEmpSeen());
        edtloanno.setText(user_data.getLoanNo());
        txtfordate.setText(user_data.getLoanDate());
        txtfortime.setText(user_data.getLoanTime());
        edtAppname.setText(user_data.getAppName());
        edtFordob.setText(user_data.getDOB());
        txtforaddressconfirm.setText(user_data.getAddConfirm());
        txtforlandmrak.setText(user_data.getLandMark());
        txtdesignation.setText(user_data.getDesignation());
        txtdepartment.setText(user_data.getDepartment());
        txtmetwith.setText(user_data.getMetWith());
        txtforDesignate.setText(user_data.getApplicantDesignation());
        spinnerforeduction.setText(user_data.getEducation());
        txforcoststus.setText(user_data.getCoStatus());
        txtfornature.setText(user_data.getNatureOfBuss());
        txtformobileno.setText(user_data.getMobile());
    }


}
