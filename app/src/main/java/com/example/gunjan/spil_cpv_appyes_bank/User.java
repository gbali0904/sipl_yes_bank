package com.example.gunjan.spil_cpv_appyes_bank;

public class User {

    String date,aging,finalaging, priority, appno, type, name,pinno ,
            cpvid,executivecode,address,landmark,thumbnailUrl,
            department,designation,extension,CurrentStatus,Locationaddress,
            password1,mobileno,VerifyByMobile,upadatelocaction,remarks,namevisibility;


    // date,aging,finalaging, priority,appno,type, name, pinno ,cpvid,executivecode,address,landmark


    //for date
    public String getdate() {
        return date;
    }
    public void setdate(String date) {
        this.date = date;
    }

    //for finalaging
    public String getfinalaging() {
        return finalaging;
    }
    public void setfinalaging(String finalaging) {
        this.finalaging = finalaging;
    }

    //for aging
    public String getaging() {
        return aging;
    }
    public void setaging(String aging) {
        this.aging = aging;
    }

    //for appno
    public String getappno() {
        return appno;
    }

    public void setappno(String appno) {
        this.appno = appno;
    }

    //for priority
    public String getpriority() {
        return priority;
    }
    public void setpriority(String priority) {
        this.priority = priority;
    }

    //for type
    public String gettype() {
        return type;
    }

    public void settype(String type) {
        this.type = type;
    }

    //  for name
    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    //for pinno
    public String getpinno() {
        return pinno;
    }

    public void setpinno(String pinno) {
        this.pinno = pinno;
    }

    //  for cpvid
    public String getcpvid() {
        return cpvid;
    }

    public void setcpvid(String cpvid) {
        this.cpvid = cpvid;
    }

    //  for executivecode
    public String getexecutivecode() {
        return executivecode;
    }

    public void setexecutivecode(String executivecode) {
        this.executivecode = executivecode;
    }

    //  for address
    public String getaddress() {
        return address;
    }

    public void setaddress(String address) {
        this.address = address;
    }

    //  for landmark
    public String getlandmark() {
        return landmark;
    }

    public void setlandmark(String landmark) {
        this.landmark = landmark;
    }


    //  for department
    public String getdepartment() {
        return department;
    }

    public void setdepartment(String department) {
        this.department = department;
    }

    //  for designation
    public String getdesignation() {
        return designation;
    }

    public void setdesignation(String designation) {
        this.designation = designation;
    }


    //  for extension
    public String getextension() {
        return extension;
    }

    public void setextension(String extension) {
        this.extension = extension;
    }

    //for CurrentStatus
    public String getCurrentStatus() {
        return CurrentStatus;
    }

    public void setCurrentStatus(String CurrentStatus) {
        this.CurrentStatus = CurrentStatus;
    }


    //for VerifyByMobile
    public String getVerifyByMobile() {
        return VerifyByMobile;
    }

    public void setVerifyByMobile(String VerifyByMobile) {
        this.VerifyByMobile = VerifyByMobile;
    }



    //for image

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }


    //for loctionaddress

    public String getLocationAddress() {
        return Locationaddress;
    }
    public void setLocationAddress(String Locationaddress) {
        this.Locationaddress = Locationaddress;
    }

    //for password

    public String getforgotpassword() {
        return password1;
    }
    public void setforgotpassword(String password1) {
        this.password1 = password1;
    }
    //for mobileno

    public String getmobileno() {
        return mobileno;
    }
    public void setmobileno(String mobileno) {
        this.mobileno = mobileno;
    }


    //for upadatelocaction
    public String getupadateLocationAddress() {
        return upadatelocaction;
    }
    public void setupadateLocationAddress(String upadatelocaction) {
        this.upadatelocaction = upadatelocaction;
    }


    public void setremark(String remarks) {
        this.remarks = remarks;
    }
    public String getremark() {
        return remarks;
    }

    public void setbuttonvisibility(String namevisibility) {
        this.namevisibility = namevisibility;
    }
    public String getbuttonvisibility() {
        return namevisibility;
    }
}
