package com.example.gunjan.spil_cpv_appyes_bank;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by App on 4/23/2016.
 */
public class UpdateActivity extends AppCompatActivity implements View.OnClickListener , LocationListener {

    private static OnUpdateListener mListener;
    EditText editText;
    AppCompatButton appCompatButton;
    public static final String LOGIN_URL = "http://survinindia.in/CAPITAL/APP_Json/AndroidDataBase/comment.php/?Comment=";
    public static final String KEY_COMMENT = "comment";
    String Comment,CPVId,username,appid;
    TextView textViewforusername ,textViewforappid;
    CheckBox checkBox;
   int VerifyByMobile;

    //for location
    private static long MIN_TIME = 1 * 5 * 1000; // 5 seconds
    private static final long MIN_DISTANCE = 1; // 1 meter
    LocationManager locationManager;
    String mprovider;
    double LONGITUDE;
    double LATITUDE;
    String date, currentDateandTime;
    String address,map;
    // flag for GPS status
    boolean isGPSEnabled = false;
    User user=new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_activity);
        editText = (EditText) findViewById(R.id.editTextforverifiercomments);
        textViewforappid = (TextView) findViewById(R.id.txtAppId);
        textViewforusername = (TextView) findViewById(R.id.txtName);

        Intent intent = getIntent();
        CPVId=intent.getStringExtra(RecyclerViewAdapter.KEY_CPVID);
        username=intent.getStringExtra(RecyclerViewAdapter.KEY_NAME);
        textViewforusername.setText(username);

        appid=intent.getStringExtra(RecyclerViewAdapter.KEY_APPID);
        textViewforappid.setText(appid);

        checkBox=(CheckBox) findViewById(R.id.checkBox1);
         //  Toast.makeText(UpdateActivity.this, CPVId, Toast.LENGTH_LONG).show();

        appCompatButton = (AppCompatButton) findViewById(R.id.btn_update);
        appCompatButton.setOnClickListener(this);


        getLocationAddress();
        copy();
    }

    private void copy() {

        textViewforusername.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    String s1= textViewforusername.getText().toString();
                    clipboard.setText(s1);

                    Toast.makeText(getApplicationContext(), "Copied", Toast.LENGTH_SHORT).show();
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("label", "" + textViewforusername.getText().toString());
                    clipboard.setPrimaryClip(clip);

                    Toast.makeText(getApplicationContext(), "Copied", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        textViewforappid.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    String s1= textViewforappid.getText().toString();
                    clipboard.setText(s1);
                    Toast.makeText(getApplicationContext(), "Copied", Toast.LENGTH_SHORT).show();

                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("label", "" + textViewforappid.getText().toString());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getApplicationContext(), "Copied", Toast.LENGTH_SHORT).show();

                }
                return true;
            }
        });

    }

    public void onCheckboxClicked(View view) {

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        if(checked)
        {
            VerifyByMobile=1;
         //   Toast.makeText(UpdateActivity.this, "checked : "+VerifyByMobile, Toast.LENGTH_SHORT).show();
            appCompatButton.setVisibility(View.VISIBLE);
        }
        else {
            VerifyByMobile =0;
         //
         //   Toast.makeText(UpdateActivity.this, "unchecked : "+VerifyByMobile, Toast.LENGTH_SHORT).show();
        }
    }
    private void comment()
    {
        Calendar c = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String VerifierClosedOn = dateFormat.format(c.getTime());
        VerifierClosedOn =VerifierClosedOn.replace(" ", "_");
        Comment = editText.getText().toString();

        String CloseLocation=user.getupadateLocationAddress();
        CloseLocation=CloseLocation.replace("\n"," ").replace(" ","_");
        Log.e("this", "closed location" + CloseLocation);


        //  Comment =Comment.replace(" ", "_");
        try {
            Comment= URLEncoder.encode(Comment, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    //    Toast.makeText(UpdateActivity.this,"comment"+ Comment+"\ndate"+VerifierClosedOn, Toast.LENGTH_LONG).show();

        String postvalue =Comment+"&CPVId="+CPVId+"&VerifyByMobile="+VerifyByMobile+
                "&VerifierClosedOn="+VerifierClosedOn+
                "&CloseLocation="+CloseLocation
                ;
        String myurl = LOGIN_URL+postvalue;
            JsonArrayRequest request=new JsonArrayRequest(Request.Method.GET, myurl, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });


            App.getInstance().addToRequestQueue(request);
        editText.setText("");

        if(mListener!=null){
            mListener.onUpdate();
        }
        finish();

        }

    @Override
    public void onClick(View v) {

        comment();
    }
    //Refreshing the page

    public static void setOnUpdateListener(OnUpdateListener listener) {

        mListener=listener;
    }

    interface OnUpdateListener {
        void onUpdate();
    }
    private boolean getLocationAddress() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME,MIN_DISTANCE, this);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);//true if required
        criteria.setBearingRequired(false);//true if required
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        mprovider = locationManager.getBestProvider(criteria, false);
        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if ( !isGPSEnabled) {

            buildAlertMessageNoGps();

        }
        else {
            if (mprovider != null && !mprovider.equals("")) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return true;
                }
                Location location = locationManager.getLastKnownLocation(mprovider);
                if (location == null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                locationManager.requestLocationUpdates(mprovider, 15000, 1, this);

                if (location != null) {
                    onLocationChanged(location);
                    map	= user.getupadateLocationAddress();
                //    Toast.makeText(getBaseContext(), "address" + map, Toast.LENGTH_LONG).show();
                }
                else
                    Toast.makeText(getBaseContext(), "No Location Provider Found Check Your Code", Toast.LENGTH_SHORT).show();
            }
        }
        return false;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Toast.makeText(getApplication(), "Sorry GPS is not ON", Toast.LENGTH_LONG).show();
                        dialog.cancel();
                        finish();

                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        LONGITUDE = location.getLongitude();
        LATITUDE = location.getLatitude();
        MyAddress timeSpan=new MyAddress();
        address= timeSpan.addressfromLatLon(this,LONGITUDE,LATITUDE);
        user.setupadateLocationAddress(address);
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
/**/
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


}
