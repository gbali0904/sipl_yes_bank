package com.example.gunjan.spil_cpv_appyes_bank.models;

/**
 * Created by Gunjan on 11-11-2016.
 */


    /**
     * CPVId : 4612
     * Executive : ATUL
     * LoanNo : 1222
     * AppName : dd
     * AddConfirm : dd
     * MetWith : ss
     * ApplicantDesignation : ss
     * WorkingSince : ss
     * NatureOfBuss : ss
     * BoardSeen : ss
     * ActivitySeen : ss
     * BussinessCenter : ss
     * TPC1 : ss
     * TPC2 : ss
     * GateColor : ss
     * ExeComment : ss
     * AnyUpdation : ss
     * ResAddress : ss
     * LoanDate : ss
     * LoanTime : ss
     * DOB : ss
     * LandMark : ss
     * Designation : ss
     * Located : ss
     * CoStatus : ss
     * Mobile : ss
     * AreaSqft : ss
     * StockSeen : ss
     * Exterior : ss
     * NameConfirm : ss
     * JobConfirm : ss
     * WallColor : ss
     * Department : ss
     * Education : ss
     * EmployeeID : ss
     * NumberOfEmpSeen : ss
     * TypeOfStock : ss
     * EntryPermitted : ss
     * EntryPermittedSeen : ss
     * WorkingLast1 : ss
     * WorkingLast2 : s
     * addconfirm_index : 0
     * located_index  : 0
     * educatin_index : 0
     * costatus_index : 0
     * boardseen_index : 0
     * activityseen_index : 0
     * stockseen_index : 0
     * exterior_index : 0
     * name_conf_index : 0
     * job_conf_indext : 0
     */

    public class UserDataBean {
        private static UserDataBean mInstance;
        public static synchronized UserDataBean getInstance() {
            if(mInstance==null){
                mInstance=new UserDataBean();
            }
            return mInstance;
        }
        private String CPVId;
        private String Executive;
        private String LoanNo;
        private String AppName;
        private String AddConfirm;
        private String MetWith;
        private String ApplicantDesignation;
        private String WorkingSince;
        private String NatureOfBuss;
        private String BoardSeen;
        private String ActivitySeen;
        private String BussinessCenter;
        private String TPC1;
        private String TPC2;
        private String GateColor;
        private String ExeComment;
        private String AnyUpdation;
        private String ResAddress;
        private String LoanDate;
        private String LoanTime;
        private String DOB;
        private String LandMark;
        private String Designation;
        private String Located;
        private String CoStatus;
        private String Mobile;
        private String AreaSqft;
        private String StockSeen;
        private String Exterior;
        private String NameConfirm;
        private String JobConfirm;
        private String WallColor;
        private String Department;
        private String Education;
        private String EmployeeID;
        private String NumberOfEmpSeen;
        private String TypeOfStock;
        private String EntryPermitted;
        private String EntryPermittedSeen;
        private String WorkingLast1;
        private String WorkingLast2;
        private String type;

        private int addconfirm_index;
        private int located_index;
        private int educatin_index;
        private int costatus_index;
        private int boardseen_index;
        private int activityseen_index;
        private int stockseen_index;
        private int exterior_index;
        private int name_conf_index;
        private int job_conf_indext;

        public String getCPVId() {
            return CPVId;
        }

        public void setCPVId(String CPVId) {
            this.CPVId = CPVId;
        }

        public String getExecutive() {
            return Executive;
        }

        public void setExecutive(String Executive) {
            this.Executive = Executive;
        }

        public String getLoanNo() {
            return LoanNo;
        }

        public void setLoanNo(String LoanNo) {
            this.LoanNo = LoanNo;
        }

        public String getAppName() {
            return AppName;
        }

        public void setAppName(String AppName) {
            this.AppName = AppName;
        }

        public String getAddConfirm() {
            return AddConfirm;
        }

        public void setAddConfirm(String AddConfirm) {
            this.AddConfirm = AddConfirm;
        }

        public String getMetWith() {
            return MetWith;
        }

        public void setMetWith(String MetWith) {
            this.MetWith = MetWith;
        }

        public String getApplicantDesignation() {
            return ApplicantDesignation;
        }

        public void setApplicantDesignation(String ApplicantDesignation) {
            this.ApplicantDesignation = ApplicantDesignation;
        }

        public String getWorkingSince() {
            return WorkingSince;
        }

        public void setWorkingSince(String WorkingSince) {
            this.WorkingSince = WorkingSince;
        }

        public String getNatureOfBuss() {
            return NatureOfBuss;
        }

        public void setNatureOfBuss(String NatureOfBuss) {
            this.NatureOfBuss = NatureOfBuss;
        }

        public String getBoardSeen() {
            return BoardSeen;
        }

        public void setBoardSeen(String BoardSeen) {
            this.BoardSeen = BoardSeen;
        }

        public String getActivitySeen() {
            return ActivitySeen;
        }

        public void setActivitySeen(String ActivitySeen) {
            this.ActivitySeen = ActivitySeen;
        }

        public String getBussinessCenter() {
            return BussinessCenter;
        }

        public void setBussinessCenter(String BussinessCenter) {
            this.BussinessCenter = BussinessCenter;
        }

        public String getTPC1() {
            return TPC1;
        }

        public void setTPC1(String TPC1) {
            this.TPC1 = TPC1;
        }

        public String getTPC2() {
            return TPC2;
        }

        public void setTPC2(String TPC2) {
            this.TPC2 = TPC2;
        }

        public String getGateColor() {
            return GateColor;
        }

        public void setGateColor(String GateColor) {
            this.GateColor = GateColor;
        }

        public String getExeComment() {
            return ExeComment;
        }

        public void setExeComment(String ExeComment) {
            this.ExeComment = ExeComment;
        }

        public String getAnyUpdation() {
            return AnyUpdation;
        }

        public void setAnyUpdation(String AnyUpdation) {
            this.AnyUpdation = AnyUpdation;
        }

        public String getResAddress() {
            return ResAddress;
        }

        public void setResAddress(String ResAddress) {
            this.ResAddress = ResAddress;
        }

        public String getLoanDate() {
            return LoanDate;
        }

        public void setLoanDate(String LoanDate) {
            this.LoanDate = LoanDate;
        }

        public String getLoanTime() {
            return LoanTime;
        }

        public void setLoanTime(String LoanTime) {
            this.LoanTime = LoanTime;
        }

        public String getDOB() {
            return DOB;
        }

        public void setDOB(String DOB) {
            this.DOB = DOB;
        }

        public String getLandMark() {
            return LandMark;
        }

        public void setLandMark(String LandMark) {
            this.LandMark = LandMark;
        }

        public String getDesignation() {
            return Designation;
        }

        public void setDesignation(String Designation) {
            this.Designation = Designation;
        }

        public String getLocated() {
            return Located;
        }

        public void setLocated(String Located) {
            this.Located = Located;
        }

        public String getCoStatus() {
            return CoStatus;
        }

        public void setCoStatus(String CoStatus) {
            this.CoStatus = CoStatus;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String Mobile) {
            this.Mobile = Mobile;
        }

        public String getAreaSqft() {
            return AreaSqft;
        }

        public void setAreaSqft(String AreaSqft) {
            this.AreaSqft = AreaSqft;
        }

        public String getStockSeen() {
            return StockSeen;
        }

        public void setStockSeen(String StockSeen) {
            this.StockSeen = StockSeen;
        }

        public String getExterior() {
            return Exterior;
        }

        public void setExterior(String Exterior) {
            this.Exterior = Exterior;
        }

        public String getNameConfirm() {
            return NameConfirm;
        }

        public void setNameConfirm(String NameConfirm) {
            this.NameConfirm = NameConfirm;
        }

        public String getJobConfirm() {
            return JobConfirm;
        }

        public void setJobConfirm(String JobConfirm) {
            this.JobConfirm = JobConfirm;
        }

        public String getWallColor() {
            return WallColor;
        }

        public void setWallColor(String WallColor) {
            this.WallColor = WallColor;
        }

        public String getDepartment() {
            return Department;
        }

        public void setDepartment(String Department) {
            this.Department = Department;
        }

        public String getEducation() {
            return Education;
        }

        public void setEducation(String Education) {
            this.Education = Education;
        }

        public String getEmployeeID() {
            return EmployeeID;
        }

        public void setEmployeeID(String EmployeeID) {
            this.EmployeeID = EmployeeID;
        }

        public String getNumberOfEmpSeen() {
            return NumberOfEmpSeen;
        }

        public void setNumberOfEmpSeen(String NumberOfEmpSeen) {
            this.NumberOfEmpSeen = NumberOfEmpSeen;
        }

        public String getTypeOfStock() {
            return TypeOfStock;
        }

        public void setTypeOfStock(String TypeOfStock) {
            this.TypeOfStock = TypeOfStock;
        }

        public String getEntryPermitted() {
            return EntryPermitted;
        }

        public void setEntryPermitted(String EntryPermitted) {
            this.EntryPermitted = EntryPermitted;
        }

        public String getEntryPermittedSeen() {
            return EntryPermittedSeen;
        }

        public void setEntryPermittedSeen(String EntryPermittedSeen) {
            this.EntryPermittedSeen = EntryPermittedSeen;
        }

        public String getWorkingLast1() {
            return WorkingLast1;
        }

        public void setWorkingLast1(String WorkingLast1) {
            this.WorkingLast1 = WorkingLast1;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getWorkingLast2() {
            return WorkingLast2;
        }

        public void setWorkingLast2(String WorkingLast2) {
            this.WorkingLast2 = WorkingLast2;
        }

        public int getAddconfirm_index() {
            return addconfirm_index;
        }

        public void setAddconfirm_index(int addconfirm_index) {
            this.addconfirm_index = addconfirm_index;
        }

        public int getLocated_index() {
            return located_index;
        }

        public void setLocated_index(int located_index) {
            this.located_index = located_index;
        }

        public int getEducatin_index() {
            return educatin_index;
        }

        public void setEducatin_index(int educatin_index) {
            this.educatin_index = educatin_index;
        }

        public int getCostatus_index() {
            return costatus_index;
        }

        public void setCostatus_index(int costatus_index) {
            this.costatus_index = costatus_index;
        }

        public int getBoardseen_index() {
            return boardseen_index;
        }

        public void setBoardseen_index(int boardseen_index) {
            this.boardseen_index = boardseen_index;
        }

        public int getActivityseen_index() {
            return activityseen_index;
        }

        public void setActivityseen_index(int activityseen_index) {
            this.activityseen_index = activityseen_index;
        }

        public int getStockseen_index() {
            return stockseen_index;
        }

        public void setStockseen_index(int stockseen_index) {
            this.stockseen_index = stockseen_index;
        }

        public int getExterior_index() {
            return exterior_index;
        }

        public void setExterior_index(int exterior_index) {
            this.exterior_index = exterior_index;
        }

        public int getName_conf_index() {
            return name_conf_index;
        }

        public void setName_conf_index(int name_conf_index) {
            this.name_conf_index = name_conf_index;
        }

        public int getJob_conf_indext() {
            return job_conf_indext;
        }

        public void setJob_conf_indext(int job_conf_indext) {
            this.job_conf_indext = job_conf_indext;
        }
    }
