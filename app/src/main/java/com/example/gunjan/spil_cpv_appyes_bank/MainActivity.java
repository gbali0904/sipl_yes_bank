package com.example.gunjan.spil_cpv_appyes_bank;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText editTextforuser;
    EditText editTextforpassword;
    AppCompatButton buttonforlogin;
    CoordinatorLayout coordinatorLayout;
    LinearLayout linearLayout;
    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String LOGIN_URL = "http://survinindia.in/CAPITAL/APP_Json/AndroidDataBase/loginclone.php/?username=";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_VerifierId = "verifierId";
    public static final String KEY_MOBILENO = "mobileno";
    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";
    private String username;
    private String password;
    TextView forgotpassword;
    private String name = "";
    private String verifierId = "";
    private String mobileno = "",mobileno1="";
    private String pass = "";
    String status = "";
    private String checkStatus = "1";
    private static int Splash_TIME_DELEY =0;
    // Session Manager Class

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_activity);
        checkConnectivity();
    }

    private void checkConnectivity() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)&& (wifi.isConnected() | datac.isConnected())) {
            editTextforuser = (EditText) findViewById(R.id.editTextforuser);
            editTextforpassword = (EditText) findViewById(R.id.editTextforpassword);
            buttonforlogin = (AppCompatButton) findViewById(R.id.btn_signup);
            buttonforlogin.setOnClickListener(this);
            forgotpassword=(TextView) findViewById(R.id.textforgotpassword);
            forgotpassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userid();
                }
            });

        }
        else {
            //no connection
            Toast toast = Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_LONG);
            toast.show();
            showNetDisabledAlertToUser(MainActivity.this);
        }
    }

    private void userid() {
        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        // Setting Dialog Message
        alertDialog.setMessage("Enter Mobile no");
        final EditText input = new EditText(this);
        alertDialog.setView(input);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        mobileno1=input.getText().toString().trim();
                        ForgotPassword.getpassword(mobileno1, new ForgotPassword.OnFetchUserDetailListener() {
                            @Override
                            public void onUserFetched(String mobile_number,String pass) {
                                String m_no=mobile_number;
                                Toast.makeText(MainActivity.this,"mobile_number is "+m_no+"\n"+mobileno1,Toast.LENGTH_SHORT).show();

                                String s1="Your Password is: "+pass;

                                if(mobileno1.equals(m_no)) {
                                    showCustomAlert(s1);
                                }
                                else {
                                    Toast.makeText(MainActivity.this,"Wrong mobileno", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Message
        alertDialog.show();
    }
    public void showCustomAlert(String s1)
    {
        Context context = getApplicationContext();
        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = getLayoutInflater();
        // Call toast.xml file for toast layout
        View toastRoot = inflater.inflate(R.layout.toast_activity, null);
        TextView txtId = (TextView) toastRoot.findViewById(R.id.toast);
        txtId.setText("Your password is:"+s1);
        Toast toast = new Toast(context);
        // Set layout to toast
        toast.setView(toastRoot);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,
                0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
    public static void showNetDisabledAlertToUser(final Context context){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setMessage("Would you like to enable it?")
                .setTitle("No Internet Connection")
                .setPositiveButton(" Enable Internet ", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(dialogIntent);
                    }
                });
        alertDialogBuilder.setNegativeButton(" Cancel ", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private void userLogin() {
        String postvalue = username + "&password=" + password;
        String finalurl = LOGIN_URL + postvalue;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, finalurl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                username = editTextforuser.getText().toString().trim();
                password = editTextforpassword.getText().toString().trim();
                JSONArray jsonarray = response;
                try {
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        mobileno = jsonobject.getString("Mobile");
                        name =jsonobject.getString("Name");
                        verifierId = jsonobject.getString("VerifierId");
                        pass = jsonobject.getString("password");
                        status = jsonobject.getString("status");

                    }
                    if (username.equals(mobileno) && password.equals(pass) && checkStatus.equals(status)) {
                        //    Toast.makeText(MainActivity.this, mobileno + "\n" + name + "\n" + verifierId, Toast.LENGTH_LONG).show();
                        openProfile();
                    }

                    else if (!username.equals(mobileno) && !password.equals(pass) && !checkStatus.equals(status)) {
                        Toast.makeText(MainActivity.this, "Wrong Credentials", Toast.LENGTH_LONG).show();
                    }
                    else if(!checkStatus.equals(status))
                    {
                        Toast.makeText(MainActivity.this,"Please check ur Status", Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(MainActivity.this, "--" + "Error"+  error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });
        App.getInstance().addToRequestQueue(request);

    }
    private void openProfile() {
        //    session.createLoginSession(mobileno,pass,status);

        SplashActivity.savePreferences(TAGS.LOGIN_USER,"1");
        SplashActivity.savePreferences(TAGS.KEY_MOBILENO,mobileno);
        SplashActivity.savePreferences(TAGS.KEY_PASS,pass);
        SplashActivity.savePreferences(TAGS.KEY_STATUS,status);
        SplashActivity.savePreferences(TAGS.KEY_USERNAME,name);
        SplashActivity.savePreferences(TAGS.KEY_USERID,verifierId);
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(TAGS.KEY_USERNAME, name);
        intent.putExtra(TAGS.KEY_USERID, verifierId);
        intent.putExtra(TAGS.KEY_MOBILENO, mobileno);
        startActivity(intent);
        finish();
    }
    @Override
    public void onClick(View v) {
        username = editTextforuser.getText().toString().trim();
        password = editTextforpassword.getText().toString().trim();
        if ((username.equals("")) && (password.equals(""))) {
            Toast.makeText(MainActivity.this, "--" + "Please Enter UserName & Password" + "!", Toast.LENGTH_LONG).show();

        }
        else if((password.equals("")))
        {
            Toast.makeText(MainActivity.this, "--" + "Please Password" + "!", Toast.LENGTH_LONG).show();
        }
        else
        {
            userLogin();
        }
    }

}

