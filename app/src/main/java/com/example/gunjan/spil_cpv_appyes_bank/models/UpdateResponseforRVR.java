package com.example.gunjan.spil_cpv_appyes_bank.models;

import java.util.List;

/**
 * Created by Gunjan on 13-11-2016.
 */

public class UpdateResponseforRVR {

    private List<String> Status;

    public List<String> getStatus() {
        return Status;
    }

    public void setStatus(List<String> Status) {
        this.Status = Status;
    }
}
