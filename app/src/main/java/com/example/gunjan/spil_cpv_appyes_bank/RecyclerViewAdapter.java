package com.example.gunjan.spil_cpv_appyes_bank;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public static String cpvidforsecoundactivicty;
    private final Context context;
    private final Activity activity;
    String checktype,CurrentStatus;
    List<User> userList;
    String cpvid;
    public static final String KEY_CPVID = "id";
    public static final String KEY_NAME= "username";
    public static final String KEY_APPID = "appid";
    public static final String KEY_CUserList = "userno";
    String passcpvid,passname,passappid;
    public static final String KEY_TYPE="type";
    public static final String KEY_ExecutiveCode="executivecode";
    public static AppCompatButton bucttonforformfordata;
    public static AppCompatButton buttonforupdate;
    public static  int viewId;
    public RecyclerViewAdapter(Context ctx, List<User> response) {
        this.context = ctx;
        this.activity = (Activity) ctx;

        userList = response;

    }

    @Override
    public int getItemCount() {

        return userList.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_row, parent, false);


        holder = new ItemViewHolder(v);
        Context context = parent.getContext();

     /*   int n = userList.length()-1;
        Toast.makeText(context, "Total number of Items are:" + n, Toast.LENGTH_LONG).show();
*/
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        final User user =  userList.get(position);

        String finaldate;
        checktype=user.gettype().toString();

        String datetime=user.getdate().toString();
        String getbuttonvisibility = user.getbuttonvisibility().toString();

        bucttonforformfordata = itemHolder.buttonforform;
        buttonforupdate = itemHolder.buttonforupdate;
        if (getbuttonvisibility.equals("0"))
        {
            bucttonforformfordata.setVisibility(View.VISIBLE);
            buttonforupdate.setVisibility(View.GONE);
        }
        else if(getbuttonvisibility.equals("1"))
        {
            bucttonforformfordata.setVisibility(View.GONE);
            buttonforupdate.setVisibility(View.VISIBLE);
        }
        SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        try {
            date = format1.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        finaldate = format2.format(date);
        //checking for BVR or RVR
        forCheckingBVROrRVRData(itemHolder, user);
        itemHolder.date.setText(finaldate);
        itemHolder.aging.setText(user.getfinalaging());
        String red=user.getpriority().toString();
        itemHolder.address.setText(user.getaddress());
        itemHolder.pinno.setText(user.getpinno());
        itemHolder.landmark.setText(user.getlandmark());
        itemHolder.remark.setText(user.getremark());
        forButtonsInList(itemHolder, user,position);
        int n = userList.size();
        //Toast.makeText(context, "Total number of Items are:" + n, Toast.LENGTH_LONG).show();
        ((SecondActivity)context).textView3.setText("Total No: " + n);
        //checking priority
        checkPriorityType(itemHolder, user, red);
        itemHolder.appno.setText(user.getappno());
        itemHolder.type.setText(user.gettype());
        itemHolder.name.setText(user.getname());
        itemHolder.executivecode.setText(user.getexecutivecode());
    }

    private void forButtonsInList(final ItemViewHolder itemHolder, final User user, final int position) {
        //for UploadImage
        itemHolder.buttonforupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UploadImagesActivity.class);
                passcpvid = user.getcpvid().toString();
                passname = user.getname().toString();
                passappid = user.getappno().toString();
                intent.putExtra(KEY_CPVID, passcpvid);
                intent.putExtra(KEY_NAME, passname);
                intent.putExtra(KEY_APPID, passappid);
                context.startActivity(intent);
            }
        });
        //for view image
        itemHolder.viewimages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImagesViewActivity.class);
                passcpvid = user.getcpvid().toString();
                intent.putExtra(KEY_CPVID, passcpvid);
                context.startActivity(intent);
            }

        });
        // for update status
        itemHolder.buttonforcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UpdateActivity.class);
                passcpvid = user.getcpvid().toString();
                passname = user.getname().toString();
                passappid = user.getappno().toString();
                intent.putExtra(KEY_CPVID, passcpvid);
                intent.putExtra(KEY_NAME, passname);
                intent.putExtra(KEY_APPID, passappid);
                context.startActivity(intent);

            }
        });
        //buttonforform

        // for update status

        itemHolder.buttonforform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bucttonforformfordata = itemHolder.buttonforform;
                buttonforupdate = itemHolder.buttonforupdate;
                cpvidforsecoundactivicty = user.getcpvid();
                String type_check = user.gettype().toString();
                if (type_check.equals("BVR")) {
                    Intent intent = new Intent(context, DataEntryBVRActivity.class);
                    passcpvid = user.getcpvid().toString();
                    String type = user.gettype().toString();
                    String executivecode = user.getexecutivecode();
                    String getname = user.getname();
                    String getlandmark = user.getlandmark();
                    String getdepartment = user.getdepartment();
                    String getdesignation = user.getdesignation();
                    String getappno = user.getappno();
                    intent.putExtra(TAGS.KEY_TYPE, type);
                    intent.putExtra(TAGS.KEY_CPVID, passcpvid);
                    intent.putExtra(TAGS.KEY_EXECUTIVECODE, executivecode);
                    intent.putExtra(TAGS.KEY_NAME, getname);
                    intent.putExtra(TAGS.KEY_LANDMARK, getlandmark);
                    intent.putExtra(TAGS.KEY_APPID, getappno);
                    intent.putExtra(TAGS.KEY_DEPARTMENT, getdepartment);
                    intent.putExtra(TAGS.KEY_DESIGNATION, getdesignation);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, DataEntryRVRActivity.class);
                    passcpvid = user.getcpvid().toString();
                    String type = user.gettype().toString();
                    String executivecode = user.getexecutivecode();
                    String getname = user.getname();
                    String getlandmark = user.getlandmark();
                    String getdepartment = user.getdepartment();
                    String getdesignation = user.getdesignation();
                    String getappno = user.getappno();
                    intent.putExtra(TAGS.KEY_TYPE, type);
                    intent.putExtra(TAGS.KEY_CPVID, passcpvid);
                    intent.putExtra(TAGS.KEY_EXECUTIVECODE, executivecode);
                    intent.putExtra(TAGS.KEY_APPID, getappno);
                    intent.putExtra(TAGS.KEY_NAME, getname);
                    intent.putExtra(TAGS.KEY_LANDMARK, getlandmark);
                    intent.putExtra(TAGS.KEY_DEPARTMENT, getdepartment);
                    intent.putExtra(TAGS.KEY_DESIGNATION, getdesignation);
                    context.startActivity(intent);
                }
            }
        });
        itemHolder.buttonforupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 String type_check = user.gettype().toString();
                if (type_check.equals("BVR")) {
                    Intent intent = new Intent(context, DataViewBVRActivity.class);
                    String cpvid = user.getcpvid().toString();
                    String type = user.gettype().toString();
                    intent.putExtra(TAGS.KEY_CPVID, cpvid);
                    intent.putExtra(TAGS.KEY_TYPE, type);
                    context.startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(context, DataViewRVRActivity.class);
                    String cpvid = user.getcpvid().toString();
                    String type = user.gettype().toString();
                    intent.putExtra(TAGS.KEY_CPVID, cpvid);
                    intent.putExtra(TAGS.KEY_TYPE, type);
                    context.startActivity(intent);

                }

            }

        });
    }

    private void checkPriorityType(ItemViewHolder itemHolder, User user, String red) {
        if(red.equals("N"))
        {
            itemHolder.priority.setText("High");
            itemHolder.priority.setTextColor(Color.parseColor("#FF0000"));
        }
        else
        {
            itemHolder.priority.setText("REDU");
            itemHolder.priority.setTextColor(Color.parseColor("#0000FF"));
        }
    }

    private void forCheckingBVROrRVRData(ItemViewHolder itemHolder, User user) {
        if(checktype.equals("BVR"))
        {
            itemHolder.department1.setVisibility(View.VISIBLE);
            itemHolder.designation1.setVisibility(View.VISIBLE);
            itemHolder.extension1.setVisibility(View.VISIBLE);

            itemHolder.department.setVisibility(View.VISIBLE);
            itemHolder.designation.setVisibility(View.VISIBLE);
            itemHolder.extension.setVisibility(View.VISIBLE);

            itemHolder.department.setText(user.getdepartment());
            itemHolder.designation.setText(user.getdesignation());
            itemHolder.extension.setText(user.getextension());
        }

        //for RVR Type
        else {
            itemHolder.department1.setVisibility(View.GONE);
            itemHolder.designation1.setVisibility(View.GONE);
            itemHolder.extension1.setVisibility(View.GONE);

            itemHolder.department.setVisibility(View.GONE);
            itemHolder.designation.setVisibility(View.GONE);
            itemHolder.extension.setVisibility(View.GONE);

        }
    }


    public void setFilter(List<User> filteredModelList) {
        userList = new ArrayList<>();
        userList.addAll(filteredModelList);
        notifyDataSetChanged();
    }
    public class ItemViewHolder extends RecyclerView.ViewHolder {

        AppCompatButton buttonforform;
        AppCompatButton buttonforupdate;
        TextView date,aging, priority,appno,type, name, pinno
                ,executivecode,address,landmark,viewimages
                ,department,designation,extension
                ,department1,designation1,extension1,remark;
        AppCompatButton buttonforupload ,buttonforcomment;

        public ItemViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.txtDate);
            aging= (TextView) itemView.findViewById(R.id.txtAging);
            priority = (TextView) itemView.findViewById(R.id.txtPriority);
            appno=(TextView)itemView.findViewById(R.id.txtAppId);
            type=(TextView)itemView.findViewById(R.id.txtType);
            name = (TextView) itemView.findViewById(R.id.txtName);
            executivecode = (TextView) itemView.findViewById(R.id.edtexecutivecode);
            address=(TextView)itemView.findViewById(R.id.txtAddress);
            pinno=(TextView)itemView.findViewById(R.id.txtpinno);
            landmark=(TextView)itemView.findViewById(R.id.txtlandmark);
            department1=(TextView)itemView.findViewById(R.id.department);
            designation1=(TextView)itemView.findViewById(R.id.designation);
            extension1=(TextView)itemView.findViewById(R.id.extension);
            department=(TextView)itemView.findViewById(R.id.txtdepartment);
            designation=(TextView)itemView.findViewById(R.id.txtdesignation);
            extension=(TextView)itemView.findViewById(R.id.txtextension);
            remark=(TextView)itemView.findViewById(R.id.txtremark);
            buttonforupload = (AppCompatButton) itemView.findViewById(R.id.buttonforupload);
            buttonforcomment=(AppCompatButton) itemView.findViewById(R.id.buttonforcomment);
            viewimages=(TextView) itemView.findViewById(R.id.viewimages);
            buttonforform=(AppCompatButton)itemView.findViewById(R.id.buttonforform);
            buttonforupdate=(AppCompatButton)itemView.findViewById(R.id.buttonforupdate);


        }

    }

}
