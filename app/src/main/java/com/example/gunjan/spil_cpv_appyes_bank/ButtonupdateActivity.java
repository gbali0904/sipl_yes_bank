package com.example.gunjan.spil_cpv_appyes_bank;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.gunjan.spil_cpv_appyes_bank.models.formdata;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Gunjan on 06-11-2016.
 */
public class ButtonupdateActivity {
     public static final String BASE_URL="http://survinindia.in/capital/json/update_buttonverified.php?";
    /*http://survinindia.in/capital/json/update_buttonverified.php?CPVId=2171&buttonverified=1&Type=BVR*/
    private static final String TAG = ButtonupdateActivity.class.getSimpleName();
    private static String message;
    private static OnUpdateListener mListener;
    public static void updatebuttonvalueforBVR(String CPVId, String type, final DataEntryBVRActivity context) {
        String visibility="1";
        String post_values="CPVId="+CPVId+"&buttonverified="+visibility+"&Type="+type;
        String FULL_URL=BASE_URL+post_values;

        Log.e(TAG, FULL_URL);
        updateButtonforBVR(context, FULL_URL);


    }
    public static void updatebuttonvalueforRVR(String CPVId, String type, final DataEntryRVRActivity context) {
        String visibility="1";
        String post_values="CPVId="+CPVId+"&buttonverified="+visibility+"&Type="+type;
        String FULL_URL=BASE_URL+post_values;

        Log.e(TAG, FULL_URL);
        updateButtonforRVR(context, FULL_URL);


    }
    private static void updateButtonforRVR(final DataEntryRVRActivity context, String FULL_URL) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, FULL_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = new Gson();
                formdata allMainUser = gson.fromJson(response.toString(), formdata.class);
                final List<String> status = allMainUser.getStatus();

                for (int i = 0; i < status.size(); i++) {
                    Log.i("Value of element " + i, status.get(i));
                    message = status.get(i);

                }
                if (message.equalsIgnoreCase("success")) {
                    //  Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                    if(mListener!=null){
                        mListener.onUpdate();
                    }
                    context.finish();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(context, "--" + "Error" + error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });

        App.getInstance().addToRequestQueue(request);
    }
    private static void updateButtonforBVR(final DataEntryBVRActivity context, String FULL_URL) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, FULL_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = new Gson();
                formdata allMainUser = gson.fromJson(response.toString(), formdata.class);
                final List<String> status = allMainUser.getStatus();

                for (int i = 0; i < status.size(); i++) {
                    Log.i("Value of element " + i, status.get(i));
                    message = status.get(i);

                }
                if (message.equalsIgnoreCase("success")) {
                  //  Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                    if(mListener!=null){
                        mListener.onUpdate();
                    }
                    context.finish();
                      }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(context, "--" + "Error" + error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });
        App.getInstance().addToRequestQueue(request);
    }
    //Refreshing the page
    public static void setOnUpdateListener(OnUpdateListener onUpdateListener) {

        mListener=onUpdateListener;
    }
    interface OnUpdateListener {
        void onUpdate();
    }
}
