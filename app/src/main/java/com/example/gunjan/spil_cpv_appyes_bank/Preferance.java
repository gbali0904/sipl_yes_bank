package com.example.gunjan.spil_cpv_appyes_bank;

import android.content.SharedPreferences;

/**
 * Created by Gunjan on 27-10-2016.
 */
public class Preferance {

    static SharedPreferences sharedPreferences;

    public static void savePreferences(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static String getPreferences(String key, String val) {
        String value = sharedPreferences.getString(key, val);
        return value;
    }


}
