package com.example.gunjan.spil_cpv_appyes_bank.models;

/**
 * Created by Gunjan on 29-10-2016.
 */

public class FormDataForBVR {
    private static FormDataForBVR mInstance;
    /**
     * executive_name : kljlkjl
     * loan_no : kjlkjl
     * cpv_id : kjlkjl
     * type : kjlkjl
     * date : kjlkjl
     * time : kjlkjl
     * app_name : kjlkjl
     * dob : kjlkjl
     * address_confirm : 1
     * address_confirm_value : sj
     * landmark : lkjkjkj
     * designation : lkjkjl
     * department : lkjkjl
     * met_with : lkjkjl
     * app_designate : lkjkjl
     * located : 1
     * located_value : sjhd
     * education_index : 1
     * education_index_value : fdgdf
     * working_since : lkjlkj
     * co_status : false
     * nature_of_business : lkjl
     * mobile : lkjl
     * no_of_emp_seen : lkjl
     * company_board_seen : 1
     * company_board_seen_value : dff
     * business_activity_seen : 1
     * business_activity_seen_value : fgfd
     * stock_seen : 1
     * stock_seen_value : dfdgdf
     * exterior : 1
     * exterior_value : dfdv
     * exterior_seen : 1
     * exterior_seen_value : dsf
     * name_and_job_conf1 : 1
     * name_and_job_conf1_value : dfd
     * name_and_job_conf2 : 1
     * name_and_job_conf2_value : sds
     * working_last1 : 1
     * working_last2 : 1
     * type_of_stock : 1
     * locality : 1
     * locality_value : dfdf
     * tcp1 : 1
     * tcp2 : 1
     * gate_color : 1
     * wall_color : 1
     * executive_comment : 1
     * any_update : 1
     * resident_address : lkjlkj
     * emp_id : lkjl
     * area_sqft : lkjl
     */

    private String executive_name;
    private String loan_no;
    private String cpv_id;
    private String type;
    private String date;
    private String time;
    private String app_name;
    private String dob;
    private int address_confirm;
    private String address_confirm_value;
    private String landmark;
    private String designation;
    private String department;
    private String met_with;
    private String app_designate;
    private int located;
    private String located_value;
    private int education_index;
    private String education_index_value;
    private String working_since;
    private int co_status;
    private String co_status_value;
    private String nature_of_business;
    private String mobile;
    private String no_of_emp_seen;
    private int company_board_seen;
    private String company_board_seen_value;
    private int business_activity_seen;
    private String business_activity_seen_value;
    private int stock_seen;
    private String stock_seen_value;
    private int exterior;
    private String exterior_value;
    private boolean exterior_seen;
    private String exterior_seen_value;
    private int name_and_job_conf1;
    private String name_and_job_conf1_value;
    private int name_and_job_conf2;
    private String name_and_job_conf2_value;
    private String working_last1;
    private String working_last2;
    private String type_of_stock;
    private int locality;
    private String locality_value;
    private String tcp1;
    private String tcp2;
    private String gate_color;
    private String wall_color;
    private String executive_comment;
    private String any_update;
    private String resident_address;
    private String emp_id;
    private String area_sqft;

    /**
     * executive_name : kljlkjl
     * loan_no : kjlkjl
     * cpv_id : kjlkjl
     * type : kjlkjl
     * date : kjlkjl
     * time : kjlkjl
     * app_name : kjlkjl
     * dob : kjlkjl
     * address_confirm : false
     * landmark : lkjkjkj
     * designation : lkjkjl
     * department : lkjkjl
     * met_with : lkjkjl
     * app_designate : lkjkjl
     * located : false
     * education_index : 1
     * working_since : lkjlkj
     * co_status : false
     * nature_of_business : lkjl
     * mobile : lkjl
     * no_of_emp_seen : lkjl
     * company_board_seen : false
     * business_activity_seen : false
     * stock_seen : false
     * exterior : false
     * exterior_seen : false
     * name_and_job_conf1 : false
     * name_and_job_conf2 : false
     * working_last1 : 1
     * working_last2 : 1
     * type_of_stock : 1
     * locality : 1
     * tcp1 : 1
     * tcp2 : 1
     * gate_color : 1
     * wall_color : 1
     * executive_comment : 1
     * any_update : 1
     * resident_address : lkjlkj
     * emp_id : lkjl
     * area_sqft : lkjl
     */
    private FormDataForBVR() {
    }
    public static synchronized FormDataForBVR getInstance() {
        if(mInstance==null){
            mInstance=new FormDataForBVR();
        }
        return mInstance;
    }

    public static void setInstance(FormDataForBVR formData) {
        mInstance=formData;
    }


    public String getExecutive_name() {
        return executive_name;
    }

    public void setExecutive_name(String executive_name) {
        this.executive_name = executive_name;
    }

    public String getLoan_no() {
        return loan_no;
    }

    public void setLoan_no(String loan_no) {
        this.loan_no = loan_no;
    }

    public String getCpv_id() {
        return cpv_id;
    }

    public void setCpv_id(String cpv_id) {
        this.cpv_id = cpv_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }


    //forindex
    public int getAddress_confirm() {
        return address_confirm;
    }

    public void setAddress_confirm(int address_confirm) {
        this.address_confirm = address_confirm;
    }
    //for value
    public String getAddress_confirm_value() {
        return address_confirm_value;
    }

    public void setAddress_confirm_value(String address_confirm_value) {
        this.address_confirm_value = address_confirm_value;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getMet_with() {
        return met_with;
    }

    public void setMet_with(String met_with) {
        this.met_with = met_with;
    }

    public String getApp_designate() {
        return app_designate;
    }

    public void setApp_designate(String app_designate) {
        this.app_designate = app_designate;
    }
    //forindex

    public int getLocated() {
        return located;
    }

    public void setLocated(int located) {
        this.located = located;
    }
    //forvalue
    public String getLocated_value() {
        return located_value;
    }
    public void setLocated_value(String located_value) {
        this.located_value = located_value;
    }
    //foreductionindex

    public int getEducation_index() {
        return education_index;
    }

    public void setEducation_index(int education_index) {
        this.education_index = education_index;
    }
    //  for eduction value

    public String getEducation_index_value() {
        return education_index_value;
    }

    public void setEducation_index_value(String education_index_value) {
        this.education_index_value = education_index_value;
    }

    public String getWorking_since() {
        return working_since;
    }

    public void setWorking_since(String working_since) {
        this.working_since = working_since;
    }

    public String getCo_status_value() {
        return co_status_value;
    }

    public void setCo_status_value(String co_status_value) {
        this.co_status_value = co_status_value;
    }

    public int getCo_status() {
        return co_status;
    }

    public void setCo_status(int co_status) {
        this.co_status = co_status;
    }


    public String getNature_of_business() {
        return nature_of_business;
    }

    public void setNature_of_business(String nature_of_business) {
        this.nature_of_business = nature_of_business;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNo_of_emp_seen() {
        return no_of_emp_seen;
    }

    public void setNo_of_emp_seen(String no_of_emp_seen) {
        this.no_of_emp_seen = no_of_emp_seen;
    }

    public int getCompany_board_seen() {
        return company_board_seen;
    }

    public void setCompany_board_seen(int company_board_seen) {
        this.company_board_seen = company_board_seen;
    }

    public String getCompany_board_seen_value() {
        return company_board_seen_value;
    }

    public void setCompany_board_seen_value(String company_board_seen_value) {
        this.company_board_seen_value = company_board_seen_value;
    }

    public int getBusiness_activity_seen() {
        return business_activity_seen;
    }

    public void setBusiness_activity_seen(int business_activity_seen) {
        this.business_activity_seen = business_activity_seen;
    }

    public String getBusiness_activity_seen_value() {
        return business_activity_seen_value;
    }

    public void setBusiness_activity_seen_value(String business_activity_seen_value) {
        this.business_activity_seen_value = business_activity_seen_value;
    }

    public int getStock_seen() {
        return stock_seen;
    }

    public void setStock_seen(int stock_seen) {
        this.stock_seen = stock_seen;
    }

    public String getStock_seen_value() {
        return stock_seen_value;
    }

    public void setStock_seen_value(String stock_seen_value) {
        this.stock_seen_value = stock_seen_value;
    }

    public int getExterior() {
        return exterior;
    }

    public void setExterior(int exterior) {
        this.exterior = exterior;
    }

    public String getExterior_value() {
        return exterior_value;
    }

    public void setExterior_value(String exterior_value) {
        this.exterior_value = exterior_value;
    }



    //for interior seen index
    public boolean getInterior_seen_index() {
        return exterior_seen;
    }

    public void setInterior_seen_index(boolean exterior_seen) {
        this.exterior_seen = exterior_seen;
    }

    public String getInterior_seen_value() {
        return exterior_seen_value;
    }

    public void setInterior_seen_value(String exterior_seen_value) {
        this.exterior_seen_value = exterior_seen_value;
    }

    public int getName_and_job_conf1() {
        return name_and_job_conf1;
    }

    public void setName_and_job_conf1(int name_and_job_conf1) {
        this.name_and_job_conf1 = name_and_job_conf1;
    }

    public String getName_and_job_conf1_value() {
        return name_and_job_conf1_value;
    }

    public void setName_and_job_conf1_value(String name_and_job_conf1_value) {
        this.name_and_job_conf1_value = name_and_job_conf1_value;
    }

    public int getName_and_job_conf2() {
        return name_and_job_conf2;
    }

    public void setName_and_job_conf2(int name_and_job_conf2) {
        this.name_and_job_conf2 = name_and_job_conf2;
    }

    public String getName_and_job_conf2_value() {
        return name_and_job_conf2_value;
    }

    public void setName_and_job_conf2_value(String name_and_job_conf2_value) {
        this.name_and_job_conf2_value = name_and_job_conf2_value;
    }

    public String getWorking_last1() {
        return working_last1;
    }

    public void setWorking_last1(String working_last1) {
        this.working_last1 = working_last1;
    }

    public String getWorking_last2() {
        return working_last2;
    }

    public void setWorking_last2(String working_last2) {
        this.working_last2 = working_last2;
    }

    public String getType_of_stock() {
        return type_of_stock;
    }

    public void setType_of_stock(String type_of_stock) {
        this.type_of_stock = type_of_stock;
    }

    public int getLocality() {
        return locality;
    }

    public void setLocality(int locality) {
        this.locality = locality;
    }

    public String getLocality_value() {
        return locality_value;
    }

    public void setLocality_value(String locality_value) {
        this.locality_value = locality_value;
    }

    public String getTcp1() {
        return tcp1;
    }

    public void setTcp1(String tcp1) {
        this.tcp1 = tcp1;
    }

    public String getTcp2() {
        return tcp2;
    }

    public void setTcp2(String tcp2) {
        this.tcp2 = tcp2;
    }

    public String getGate_color() {
        return gate_color;
    }

    public void setGate_color(String gate_color) {
        this.gate_color = gate_color;
    }

    public String getWall_color() {
        return wall_color;
    }

    public void setWall_color(String wall_color) {
        this.wall_color = wall_color;
    }

    public String getExecutive_comment() {
        return executive_comment;
    }

    public void setExecutive_comment(String executive_comment) {
        this.executive_comment = executive_comment;
    }

    public String getAny_update() {
        return any_update;
    }

    public void setAny_update(String any_update) {
        this.any_update = any_update;
    }

    public String getResident_address() {
        return resident_address;
    }

    public void setResident_address(String resident_address) {
        this.resident_address = resident_address;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getArea_sqft() {
        return area_sqft;
    }

    public void setArea_sqft(String area_sqft) {
        this.area_sqft = area_sqft;
    }
}
