package com.example.gunjan.spil_cpv_appyes_bank;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

/**
 * Created by App on 4/23/2016.
 */
public class ImagesViewActivity extends AppCompatActivity {

    private TextView textView;
    private Button buttonGetImage;
    private ImageView imageView;
    String CPVId;
    RecyclerView recyclerView;

    //private final String imageURL = "http://simplifiedcoding.16mb.com/ImageUpload/getImage.php?id=";
    private static final String TAG = ImagesViewActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imageview_activity);
        recyclerView=(RecyclerView) findViewById(R.id.rvUsers);
        Intent intent = getIntent();
        CPVId=intent.getStringExtra(RecyclerViewAdapter.KEY_CPVID);
     //   Toast.makeText(ImagesViewActivity.this, CPVId, Toast.LENGTH_LONG).show();
        dounloadImage();

    }

    private void dounloadImage() {
        final ProgressDialog pd = new ProgressDialog(ImagesViewActivity.this);
        pd.setMessage("Please wait getting Data");
        pd.show();
        String url = "http://survinindia.in/CAPITAL/APP_Json/AndroidDataBase/viewImages.php/?CPVId=";
        String finalurl=url+CPVId;
        JsonArrayRequest request=new JsonArrayRequest(Request.Method.GET, finalurl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
//                txtSqlData.setText(response.toString());

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(ImagesViewActivity.this));
                    recyclerView.setAdapter(new CustomViewAdapter(ImagesViewActivity.this, response));
                    pd.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                pd.dismiss();
            }
        });
        App.getInstance().addToRequestQueue(request);
    }

}
