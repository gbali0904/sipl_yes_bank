package com.example.gunjan.spil_cpv_appyes_bank;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by App on 4/23/2016.
 */
public class CustomViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final Context context;
    private final Activity activity;
    private final JSONArray userList;


    ImageLoader imageLoader = App.getInstance().getImageLoader();
    public CustomViewAdapter(Context ctx, JSONArray response) {
        this.context = ctx;
        this.activity = (Activity) ctx;
        userList = response;

    }


    @Override
    public int getItemCount() {

        return userList.length();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.imgviewcustom_activity, parent, false);
        holder = new ItemViewHolder(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;

        try {
            JSONObject user = userList.getJSONObject(position);
            itemHolder.imagetextview.setText("Name: "+user.getString("name"));
            User u1=new User();
            u1.setThumbnailUrl(user.getString("imagename"));
            // thumbnail image
            itemHolder.thumbNail.setImageUrl(u1.getThumbnailUrl(), imageLoader);

                            } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView imagetextview ;
        NetworkImageView thumbNail;

        public ItemViewHolder(View itemView) {
            super(itemView);
            imagetextview = (TextView) itemView.findViewById(R.id.imagename);
             thumbNail = (NetworkImageView) itemView.findViewById(R.id.thumbnail);

        }

    }

}
