package com.example.gunjan.spil_cpv_appyes_bank;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Gunjan on 11-09-2016.
 */
public class LocationActivity {

    public String addressfromLatLon(Context context , double LONGITUDE , double LATITUDE ) {
        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
        List<Address> addresses = null;
        String ss="";
        try {
            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(addresses != null) {
            Address returnedAddress = addresses.get(0);
            StringBuilder strReturnedAddress = new StringBuilder("");
            for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("");
            }
            ss=strReturnedAddress.toString();
        }
        else{
            Log.e("this", "No Address returned!");
        }
        return ss;
    }
}
