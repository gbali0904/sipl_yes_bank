package com.example.gunjan.spil_cpv_appyes_bank;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by App on 4/22/2016.
 */
public class UploadImagesActivity extends AppCompatActivity implements View.OnClickListener,LocationListener {

    private Button buttonChoose;
    private Button buttonUpload;

    private ImageView imageView;
    private TextView textView, textViewforappid, textViewforusername;
    private Bitmap bitmap;
    Thread thread;
    private int PICK_IMAGE_REQUEST = 1;

    //private String UPLOAD_URL = "http://systemamc.com/AndroidDataBase/uploadImage.php";
    private String UPLOAD_URL = "http://survinindia.in/CAPITAL/APP_Json/AndroidDataBase/uploadImage.php";
    private String KEY_IMAGE = "image";
    private String KEY_NAME = "name";
    private String KEY_DATE = "date";
    private String KEY_UPLOADADDRESS = "map";
    String name, username, appid;
    String nameperfix;

    private static long MIN_TIME = 1 * 5 * 1000; // 5 seconds
    private static final long MIN_DISTANCE = 1; // 1 meter
    LocationManager locationManager;
    String mprovider;
    double LONGITUDE;
    double LATITUDE;
    String date, currentDateandTime;
    String address,map;
    // flag for GPS status
    boolean isGPSEnabled = false;

    User user =new User();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_activity);
        buttonChoose = (Button) findViewById(R.id.buttonChoose);
        buttonUpload = (Button) findViewById(R.id.buttonUpload);
        imageView = (ImageView) findViewById(R.id.imageView);
        textView = (TextView) findViewById(R.id.textnothing1);
        textViewforappid = (TextView) findViewById(R.id.txtAppId);
        textViewforusername = (TextView) findViewById(R.id.txtName);

        Intent intent = getIntent();
        nameperfix = intent.getStringExtra(RecyclerViewAdapter.KEY_CPVID);

        username = intent.getStringExtra(RecyclerViewAdapter.KEY_NAME);
        textViewforusername.setText(username);

        appid = intent.getStringExtra(RecyclerViewAdapter.KEY_APPID);
        textViewforappid.setText(appid);

        buttonChoose.setOnClickListener(this);
        buttonUpload.setOnClickListener(this);
        //for address
        if (getLocationAddress()) return;
    }
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        return encodedImage;
    }

    private void uploadImage() {
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();
                        //Showing toast message of the response
                        if(s.equals("Success")) {
                            Toast.makeText(UploadImagesActivity.this, s, Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(UploadImagesActivity.this, s, Toast.LENGTH_LONG).show();
                            finish();
                        }

                        thread.start();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                           Toast.makeText(UploadImagesActivity.this, volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);
                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put(KEY_IMAGE, image);
                params.put(KEY_NAME, name);
                params.put(KEY_DATE, date);
                params.put(KEY_UPLOADADDRESS, map);
                //returning parameters
                return params;
            }
        };
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);

        thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3500); // As I am using LENGTH_LONG in Toast
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

    }
    private void showFileChooser() {
       Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {
                buttonUpload.setVisibility(View.VISIBLE);
                //getting file URI from result
                Uri selectedImageUri = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                //setting image bitmap to imageview
                imageView.setImageBitmap(bitmap);
                //get file path [we need this file path only for creating a new file ]
                String file_path = getPath(selectedImageUri, filePathColumn);
                Log.e("this", "App Debug File Path" + file_path);

                //convert the file path into file object
                File file=new File(file_path);
                //get modified date from file
                Date dateCrated=new Date(file.lastModified());
                Log.e("this", "App Debug Date modified" + dateCrated.toString());

                //convert date to desired pattern
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                date=sdf.format(dateCrated);
                Log.e("this", "App Debug Date modified" + date);

                //set this formated date string to textview
                name=nameperfix+"_"+file.getName();
                textView.setText(name + "\nDate" + date);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Get file path from URI and column
     * @param uri
     * @param column
     * @return
     */
    public String getPath(Uri uri, String[] column)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        //Cursor cursor = managedQuery(uri, projection, null, null, null);
        Cursor cursor = getContentResolver().query(uri, column, null, null, null);
        if (cursor == null) return null;
        int column_index =cursor.getColumnIndexOrThrow(column[0]);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }


    @Override
    public void onClick(View v) {

        if (v == buttonChoose) {

            showFileChooser();
        }
       if (v == buttonUpload) {

                uploadImage();
            }

    }
    //for uploadingaddress
    private boolean getLocationAddress() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME,MIN_DISTANCE, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME,MIN_DISTANCE, this);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);//true if required
        criteria.setBearingRequired(false);//true if required
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        mprovider = locationManager.getBestProvider(criteria, false);
        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if ( !isGPSEnabled) {

            buildAlertMessageNoGps();

        }
        else {
        if (mprovider != null && !mprovider.equals("")) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            Location location = locationManager.getLastKnownLocation(mprovider);
            if (location == null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            locationManager.requestLocationUpdates(mprovider, 15000, 1, this);

            if (location != null) {
                onLocationChanged(location);
                map	= user.getLocationAddress();
           //  Toast.makeText(getBaseContext(), "address" + map, Toast.LENGTH_LONG).show();
            }
            else
                Toast.makeText(getBaseContext(), "No Location Provider Found Check Your Code", Toast.LENGTH_SHORT).show();
        }
        }
        return false;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        LONGITUDE = location.getLongitude();
        LATITUDE = location.getLatitude();
        MyAddress timeSpan=new MyAddress();
        address= timeSpan.addressfromLatLon(this,LONGITUDE,LATITUDE);
        user.setLocationAddress(address);
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
/**/
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


}
