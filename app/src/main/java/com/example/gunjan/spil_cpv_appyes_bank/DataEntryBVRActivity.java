package com.example.gunjan.spil_cpv_appyes_bank;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.gunjan.spil_cpv_appyes_bank.Fragment.JsonClassForFragment;
import com.example.gunjan.spil_cpv_appyes_bank.models.FormDataForBVR;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Gunjan on 06-11-2016.
 */

public class DataEntryBVRActivity extends AppCompatActivity {
    private static final String TAG = DataEntryBVRActivity.class.getSimpleName();
    private static DataEntryBVRActivity mInstance;
    private final FormDataForBVR formData;
    @Bind(R.id.edtexecutivecode)
    TextView txtxecutivecode;
    @Bind(R.id.edtloanno)
    TextView txtloanno;
    @Bind(R.id.txtfordate)
    TextView txtfordate;
    @Bind(R.id.txtfortime)
    TextView txtfortime;
    @Bind(R.id.edtAppname)
    TextView txtAppname;
    @Bind(R.id.edtFordob)
    EditText edtFordob;
    @Bind(R.id.rbFirstFragConfirm)
    RadioButton rbFirstFragConfirm;
    @Bind(R.id.radionoaddconfirm)
    RadioButton radionoaddconfirm;
    @Bind(R.id.radiononeaddconfirm)
    RadioButton radiononeaddconfirm;
    @Bind(R.id.rgAddconfirm)
    RadioGroup rgAddconfirm;
    @Bind(R.id.txtforlandmrak)
    EditText txtforlandmrak;
    @Bind(R.id.txtdesignation)
    EditText txtdesignation;
    @Bind(R.id.txtdepartment)
    EditText txtdepartment;
    @Bind(R.id.textView11)
    TextView textView11;
    @Bind(R.id.txtmetwith)
    EditText txtmetwith;
    @Bind(R.id.txtforDesignate)
    EditText txtforDesignate;
    @Bind(R.id.Located)
    TextView Located;
    @Bind(R.id.radiobuttonforactivitylocate)
    RadioButton radiobuttonforactivitylocate;
    @Bind(R.id.radionotseenactivitylocate)
    RadioButton radionotseenactivitylocate;
    @Bind(R.id.activitylocate)
    RadioGroup activitylocate;
    @Bind(R.id.txtforworkingsince)
    EditText txtforworkingsince;
    @Bind(R.id.companystatusyes)
    RadioButton companystatusyes;
    @Bind(R.id.companystatusno)
    RadioButton companystatusno;
    @Bind(R.id.companystatus)
    RadioGroup companystatus;
    @Bind(R.id.txtforemployid)
    EditText txtforemployid;
    @Bind(R.id.txtfornature)
    EditText txtfornature;
    @Bind(R.id.txtformobileno)
    EditText txtformobileno;
    @Bind(R.id.txtfornoofemployeeseen)
    EditText txtfornoofemployeeseen;
    @Bind(R.id.companynameboardseenyes)
    RadioButton companynameboardseenyes;
    @Bind(R.id.companynameboardseenno)
    RadioButton companynameboardseenno;
    @Bind(R.id.companynameboardseennone)
    RadioButton companynameboardseennone;
    @Bind(R.id.companynameboardseen)
    RadioGroup companynameboardseen;
    @Bind(R.id.txtforareaaquarefeet)
    EditText txtforareaaquarefeet;
    @Bind(R.id.busnessactivityseenyes)
    RadioButton busnessactivityseenyes;
    @Bind(R.id.busnessactivityseenno)
    RadioButton busnessactivityseenno;
    @Bind(R.id.busnessactivityseennone)
    RadioButton busnessactivityseennone;
    @Bind(R.id.busnessactivityseen)
    RadioGroup busnessactivityseen;
    @Bind(R.id.stockseenyes)
    RadioButton stockseenyes;
    @Bind(R.id.stockseenseenno)
    RadioButton stockseenseenno;
    @Bind(R.id.stockseennone)
    RadioButton stockseennone;
    @Bind(R.id.stockseen)
    RadioGroup stockseen;
    @Bind(R.id.txtfortypeofstock)
    EditText txtfortypeofstock;
    @Bind(R.id.locality)
    TextView locality;
    @Bind(R.id.spinnerforeduction)
    Spinner spinnerforeduction;
    @Bind(R.id.exterior)
    TextView exterior;
    @Bind(R.id.radioexteriorgood)
    RadioButton radioexteriorgood;
    @Bind(R.id.radioexteriorpoor)
    RadioButton radioexteriorpoor;
    @Bind(R.id.radioexterioravergare)
    RadioButton radioexterioravergare;
    @Bind(R.id.radioexterior)
    RadioGroup radioexterior;
    @Bind(R.id.ch_privacy)
    CheckBox chPrivacy;
    @Bind(R.id.exteriorseen)
    TextView exteriorseen;
    @Bind(R.id.edtexteriorseen)
    EditText edtexteriorseen;
    @Bind(R.id.layout)
    LinearLayout layout;
    @Bind(R.id.txtfortcp1)
    EditText txtfortcp1;
    @Bind(R.id.radionamejobyes)
    RadioButton radionamejobyes;
    @Bind(R.id.radionamejobNo)
    RadioButton radionamejobNo;
    @Bind(R.id.radionamejobnone)
    RadioButton radionamejobnone;
    @Bind(R.id.radionamejob)
    RadioGroup radionamejob;
    @Bind(R.id.txtforworkinglast)
    EditText txtforworkinglast;
    @Bind(R.id.txtfortcp2)
    EditText txtfortcp2;
    @Bind(R.id.radionamejobyes1)
    RadioButton radionamejobyes1;
    @Bind(R.id.radionamejobNo1)
    RadioButton radionamejobNo1;
    @Bind(R.id.radionamejobnone1)
    RadioButton radionamejobnone1;
    @Bind(R.id.radionamejob1)
    RadioGroup radionamejob1;
    @Bind(R.id.txtforworkinglast1)
    EditText txtforworkinglast1;
    @Bind(R.id.txtforgatecolor)
    EditText txtforgatecolor;
    @Bind(R.id.txtforwallcolor)
    EditText txtforwallcolor;
    @Bind(R.id.txtforExecutiveComment)
    EditText txtforExecutiveComment;
    @Bind(R.id.txtforanyupdate)
    EditText txtforanyupdate;
    @Bind(R.id.txtforresidentadd)
    EditText txtforresidentadd;
    @Bind(R.id.right_nav)
    Button rightNav;

    public static String CPVId;
    public static String Education;
    @Bind(R.id.spinnerforLocality)
    Spinner spinnerforLocality;
    @Bind(R.id.scrollView)
    ScrollView scrollView;
    @Bind(R.id.textView2)
    TextView textView2;
    private String type;
    private View view;
    private String address_confirm;

    public static String BussinessCenter = null;
    private boolean seen_index_value;

    public DataEntryBVRActivity() {
        // Required empty public constructor
        formData = FormDataForBVR.getInstance();
    }
    public static DataEntryBVRActivity getInstance() {
        if (mInstance == null) {
            mInstance = new DataEntryBVRActivity();
        }
        return mInstance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.data_entry_bvr_activity);
        ButterKnife.bind(this);
        chPrivacy.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (chPrivacy.isChecked()) {
                    layout.setVisibility(View.VISIBLE);
                    seen_index_value=true;
                    formData.setInterior_seen_index(seen_index_value);


                } else {
                    layout.setVisibility(View.GONE);
                    seen_index_value=false;
                    formData.setInterior_seen_index(seen_index_value);
                }
            }
        });

        Calendar c = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd ");
        String VerifierClosedOn = dateFormat.format(c.getTime());
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+05:30"));
        Date currentLocalTime = cal.getTime();
        DateFormat time = new SimpleDateFormat("HH:mm a");
        time.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
        String localTime = time.format(currentLocalTime);
        Intent mIntent = getIntent();

        //using intent
        CPVId = mIntent.getStringExtra(TAGS.KEY_CPVID);
        type = mIntent.getStringExtra(TAGS.KEY_TYPE);
        formData.setCpv_id(CPVId);
        formData.setType(type);
        txtxecutivecode.setText(mIntent.getStringExtra(TAGS.KEY_EXECUTIVECODE));
        txtAppname.setText(mIntent.getStringExtra(TAGS.KEY_NAME));
        txtloanno.setText(mIntent.getStringExtra(TAGS.KEY_APPID));
        txtforlandmrak.setText(mIntent.getStringExtra(TAGS.KEY_LANDMARK));
        txtforDesignate.setText(mIntent.getStringExtra(TAGS.KEY_DESIGNATION));
        txtdepartment.setText(mIntent.getStringExtra(TAGS.KEY_DEPARTMENT));
        txtfordate.setText(VerifierClosedOn);
        txtfortime.setText(localTime);


        //for address confirm
        final int checkedRadioButtonId = rgAddconfirm.getCheckedRadioButtonId();
        rbFirstFragConfirm = (RadioButton) findViewById(checkedRadioButtonId);
        final int id = rbFirstFragConfirm.getId();
        int index = rgAddconfirm.indexOfChild(rbFirstFragConfirm);
        address_confirm = rbFirstFragConfirm.getText().toString();

        formData.setAddress_confirm_value(address_confirm);
        formData.setAddress_confirm(index);


        //for located
        final int checkedactivitylocate = activitylocate.getCheckedRadioButtonId();
        radiobuttonforactivitylocate = (RadioButton) findViewById(checkedactivitylocate);
        formData.setLocated_value(radiobuttonforactivitylocate.getText().toString());
        formData.setLocated(activitylocate.indexOfChild(radiobuttonforactivitylocate));

        //for co-status
        final int checkedcompanystatus = companystatus.getCheckedRadioButtonId();
        companystatusyes = (RadioButton) findViewById(checkedcompanystatus);
        formData.setCo_status(companystatus.indexOfChild(companystatusyes));
        formData.setCo_status_value(companystatusyes.getText().toString());


        // for company board seen
        final int checkedcompanynameboardseen = companynameboardseen.getCheckedRadioButtonId();
        companynameboardseenyes = (RadioButton) findViewById(checkedcompanynameboardseen);
        formData.setCompany_board_seen(companynameboardseen.indexOfChild(companynameboardseenyes));
        formData.setCompany_board_seen_value(companynameboardseenyes.getText().toString());

        //activity seen
        final int checkedbusnessactivityseen = busnessactivityseen.getCheckedRadioButtonId();
        busnessactivityseenyes = (RadioButton)findViewById(checkedbusnessactivityseen);
        formData.setBusiness_activity_seen(busnessactivityseen.indexOfChild(busnessactivityseenyes));
        formData.setBusiness_activity_seen_value(busnessactivityseenyes.getText().toString());

        // exterior seen

        final int checkedradioexterior = radioexterior.getCheckedRadioButtonId();
        radioexteriorgood = (RadioButton) findViewById(checkedradioexterior);
        formData.setExterior(radioexterior.indexOfChild(radioexteriorgood));
        formData.setExterior_value(radioexteriorgood.getText().toString());

        //stock seen

        final int checkedstockseen = stockseen.getCheckedRadioButtonId();
        stockseenyes = (RadioButton) findViewById(checkedstockseen);
        formData.setStock_seen(stockseen.indexOfChild(stockseenyes));
        formData.setStock_seen_value(stockseenyes.getText().toString());

        //name con.1
        final int checkedradionamejob = radionamejob.getCheckedRadioButtonId();
        radionamejobyes = (RadioButton) findViewById(checkedradionamejob);
        formData.setName_and_job_conf1(radionamejob.indexOfChild(radionamejobyes));
        formData.setName_and_job_conf1_value(radionamejobyes.getText().toString());


        //name and job conf.2
        final int checkedradionamejob1 = radionamejob1.getCheckedRadioButtonId();
        radionamejobyes1 = (RadioButton)findViewById(checkedradionamejob1);
        formData.setName_and_job_conf2(radionamejob1.indexOfChild(radionamejobyes1));
        formData.setName_and_job_conf2_value(radionamejobyes1.getText().toString());


        //for all radio groups ie address confirm,located
         radioMethod();

        //for all spinners ie eductaion
       spinerMethod();


    }
    private void spinerMethod() {
        //for eduction
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.Eduction_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforeduction.setAdapter(adapter);
        spinnerforeduction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Education = String.valueOf(spinnerforeduction.getSelectedItem());
                formData.setEducation_index(position);
                formData.setEducation_index_value(Education);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //for locality
        ArrayAdapter<CharSequence> adapterLocality_array = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.Locality_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapterLocality_array.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinnerforlocated
        spinnerforLocality.setAdapter(adapterLocality_array);
        spinnerforLocality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BussinessCenter = String.valueOf(spinnerforLocality.getSelectedItem());
                formData.setLocality(position);
                formData.setLocality_value(BussinessCenter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void radioMethod() {
        //address confirm
            rgAddconfirm.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                rbFirstFragConfirm = (RadioButton) findViewById(checkedId);
                address_confirm = rbFirstFragConfirm.getText().toString();
                int index = rgAddconfirm.indexOfChild(rbFirstFragConfirm);

                formData.setAddress_confirm_value(address_confirm);
                formData.setAddress_confirm(checkedId);
                Toast.makeText(getApplicationContext(), address_confirm + index, Toast.LENGTH_LONG).show();
            }
        });


        //located
        activitylocate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radiobuttonforactivitylocate = (RadioButton) findViewById(checkedId);
                formData.setLocated_value(radiobuttonforactivitylocate.getText().toString());
                formData.setLocated(checkedId);
            }
        });


        //compny status

        companystatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                companystatusyes = (RadioButton) findViewById(checkedId);
                formData.setCo_status(checkedId);
                formData.setCo_status_value(companystatusyes.getText().toString());
            }
        });


        //company board seen
        companynameboardseen.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                companynameboardseenyes = (RadioButton)findViewById(checkedId);
                formData.setCompany_board_seen(checkedId);
                formData.setCompany_board_seen_value(companynameboardseenyes.getText().toString());
            }
        });
        //activity seen
        busnessactivityseen.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                busnessactivityseenyes = (RadioButton)findViewById(checkedId);
                formData.setBusiness_activity_seen(checkedId);
                formData.setBusiness_activity_seen_value(busnessactivityseenyes.getText().toString());

            }
        });
        //stock seen
        stockseen.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                stockseenyes = (RadioButton) findViewById(checkedId);
                formData.setStock_seen(checkedId);
                formData.setStock_seen_value(stockseenyes.getText().toString());

            }
        });
        //Exterior
        radioexterior.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radioexteriorgood = (RadioButton) findViewById(checkedId);
                formData.setExterior(checkedId);
                formData.setExterior_value(radioexteriorgood.getText().toString());
            }
        });
        //name and job conf.1
           radionamejob.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radionamejobyes = (RadioButton) findViewById(checkedId);
                formData.setName_and_job_conf1(checkedId);
                formData.setName_and_job_conf1_value(radionamejobyes.getText().toString());
            }
        });
        //name and job conf.2
                radionamejob1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radionamejobyes1 = (RadioButton)findViewById(checkedId);
                formData.setName_and_job_conf2(checkedId);
                formData.setName_and_job_conf2_value(radionamejobyes1.getText().toString());
            }
        });


    }
    private void setFormData() {
        formData.setDate(txtfordate.getText().toString());
        formData.setTime(txtfortime.getText().toString());
        formData.setLoan_no(txtloanno.getText().toString());
        formData.setDob(edtFordob.getText().toString());
        formData.setExecutive_name(txtxecutivecode.getText().toString());
        formData.setApp_name(txtAppname.getText().toString());
        formData.setLandmark(txtforlandmrak.getText().toString());
        formData.setDesignation(txtdesignation.getText().toString());
        formData.setDepartment(txtdepartment.getText().toString());
        formData.setMet_with(txtmetwith.getText().toString());
        formData.setApp_designate(txtforDesignate.getText().toString());

        formData.setWorking_since(txtforworkingsince.getText().toString());
        formData.setEmp_id(txtforemployid.getText().toString());
        formData.setNature_of_business(txtfornature.getText().toString());
        formData.setMobile(txtformobileno.getText().toString());
        formData.setNo_of_emp_seen(txtfornoofemployeeseen.getText().toString());
        formData.setType_of_stock(txtfortypeofstock.getText().toString());
        formData.setArea_sqft(txtforareaaquarefeet.getText().toString());


        formData.setTcp1(txtfortcp1.getText().toString());
        formData.setWorking_last1(txtforworkinglast.getText().toString());
        formData.setTcp2(txtfortcp2.getText().toString());
        formData.setWorking_last2(txtforworkinglast1.getText().toString());
        formData.setGate_color(txtforgatecolor.getText().toString());
        formData.setWall_color(txtforwallcolor.getText().toString());
        formData.setExecutive_comment(txtforExecutiveComment.getText().toString());
        formData.setInterior_seen_value(edtexteriorseen.getText().toString());

        formData.setAny_update(txtforanyupdate.getText().toString());
        formData.setResident_address(txtforresidentadd.getText().toString());

        final String jsonFormData = App.getInstance().getGSON().toJson(formData);
        Log.e(TAG, jsonFormData);
        App.getInstance().getPreferencesEditor().putString(TAGS.JSON_FORM_DATA, jsonFormData).commit();
    }
    @OnClick(R.id.right_nav)
    public void onClick() {
        setFormData();
        uploadToServer();
    }
    private void uploadToServer() {

        final String formDataURL = API.getFormDataURL(formData);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, formDataURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //  Toast.makeText(getContext(),  response.toString() , Toast.LENGTH_LONG).show();


                Gson gson = new Gson();
                JsonClassForFragment allMainUser = gson.fromJson(response.toString(), JsonClassForFragment.class);
                final List<String> status = allMainUser.getStatus();
                String message = null;
                // retrieving data from string list array in for loop
                for (int i = 0; i < status.size(); i++) {
                    Log.i("Value of element " + i, status.get(i));
                    message = status.get(i);

                }
                if (message.equalsIgnoreCase("success")) {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    ButtonupdateActivity.updatebuttonvalueforBVR(CPVId,type,DataEntryBVRActivity.this);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(getApplicationContext(), "--" + "Error" + error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });
        App.getInstance().addToRequestQueue(request);

    }



}
