package com.example.gunjan.spil_cpv_appyes_bank;

/**
 * Created by Gunjan on 27-10-2016.
 */
public class TAGS {

    public static final String KEY_USERID = "user_id";
    public static final String KEY_TYPE = "user_type";
    public static final String KEY_CPVID = "cpvid";
    public static final String KEY_EXECUTIVECODE = "executivecode";
    public static final String KEY_NAME = "name";
    public static final String KEY_LANDMARK = "landmark";
    public static final String KEY_DEPARTMENT = "department";
    public static final String KEY_DESIGNATION = "desigmation";
    public static final String JSON_FORM_DATA = "json_form_data";
    public static final String JSON_FORM_DATA_RVR = "json_for_rvr_data";
    public static final String KEY_APPID = "user_app_id";
    public static final String KEY_USERDATA = "viewforbvrvalues";

    public static final String JSONDATA = "user_json_data_for_login";
    public static final String KEY_USERNAME = "user_name";
    public static final String KEY_MOBILENO = "mobile_no";
    public static final String LOGIN_USER = "user_login_Session";
    public static final String KEY_PASS = "user_password";
    public static final String KEY_STATUS = "status_user";
    public static final String CONTEXT = "secound_activity_context";
    public static final String GPS_LOCATION = "gps_location";
}
