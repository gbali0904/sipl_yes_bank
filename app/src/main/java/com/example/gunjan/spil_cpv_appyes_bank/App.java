package com.example.gunjan.spil_cpv_appyes_bank;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.gunjan.spil_cpv_appyes_bank.models.FormDataForBVR;
import com.example.gunjan.spil_cpv_appyes_bank.models.FormDataForRVR;
import com.google.gson.Gson;

public class App extends Application {

    public static final String TAG = App.class.getSimpleName();
    private static SharedPreferences mPreferences;
    private static SharedPreferences.Editor mPreferencesEditor;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static App mInstance;
    private Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        final String jsonFormData = getPreferences().getString(TAGS.JSON_FORM_DATA, "");
        Log.d(TAG, "jsonFormData: " + jsonFormData);
        if(!jsonFormData.equalsIgnoreCase("")){
            final FormDataForBVR formData = getGSON().fromJson(jsonFormData, FormDataForBVR.class);
            FormDataForBVR.setInstance(formData);
        }
               final String jsonFormDataforRVR = getPreferences().getString(TAGS.JSON_FORM_DATA_RVR, "");
        if(!jsonFormDataforRVR.equalsIgnoreCase("")){
            final FormDataForRVR formDataforrvr = getGSON().fromJson(jsonFormDataforRVR, FormDataForRVR.class);
            FormDataForRVR.setInstance(formDataforrvr);
        }


    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public Gson getGSON(){
        if(gson==null)
            gson=new Gson();
        return gson;
    }

    public static void getPreferenceEditor() {

    }

    public SharedPreferences getPreferences() {
        if(mPreferences==null)
            mPreferences= PreferenceManager.getDefaultSharedPreferences(App.this);
        return mPreferences;
    }
    public SharedPreferences.Editor getPreferencesEditor() {
        if(mPreferencesEditor==null)
            mPreferencesEditor= PreferenceManager.getDefaultSharedPreferences(App.this).edit();
        return mPreferencesEditor;
    }

}
