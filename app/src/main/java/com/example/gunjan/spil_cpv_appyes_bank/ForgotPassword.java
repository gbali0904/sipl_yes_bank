package com.example.gunjan.spil_cpv_appyes_bank;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by App on 6/3/2016.
 */
public class ForgotPassword {
    private static final String TAG = ForgotPassword.class.getSimpleName();

    public static void getpassword(String username, final OnFetchUserDetailListener listener) {
        String LOGIN_URL = "http://survinindia.in/hdfcapp/APP_Json/login.php/?username=";
        String finalurl = LOGIN_URL + username;
        Log.d(TAG, "getpassword : url: " + finalurl);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, finalurl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONArray jsonarray = response;
                try {
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        String mobile_number=jsonobject.getString("Mobile");
                        String pass = jsonobject.getString("password");
                        Log.d(TAG, "psswordset" + pass);
                        //return password to the interface
                        listener.onUserFetched(mobile_number,pass);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
            }
        });
        App.getInstance().addToRequestQueue(request);
    }
    //interface for returning password to calling method
    public interface OnFetchUserDetailListener{
        void onUserFetched(String mobile_number, String pass);
    }
}
