package com.example.gunjan.spil_cpv_appyes_bank;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.gunjan.spil_cpv_appyes_bank.Fragment.JsonClassForFragment;
import com.example.gunjan.spil_cpv_appyes_bank.models.FormDataForRVR;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Gunjan on 08-11-2016.
 */

public class DataEntryRVRActivity extends AppCompatActivity {

    private static final String TAG = DataEntryRVRActivity.class.getSimpleName();
    private static DataEntryRVRActivity mInstance;
    private final FormDataForRVR formData;
    @Bind(R.id.edtexecutivecode)
    TextView txtexecutivecode;
    @Bind(R.id.textView3)
    TextView textView3;
    @Bind(R.id.edtloanno)
    TextView txtloanno;
    @Bind(R.id.txtfordate)
    TextView txtfordate;
    @Bind(R.id.textView5)
    TextView textView5;
    @Bind(R.id.txtfortime)
    TextView txtfortime;
    @Bind(R.id.edtAppname)
    TextView txtAppname;
    @Bind(R.id.edtFordob)
    EditText edtFordob;
    @Bind(R.id.textView6)
    TextView textView6;
    @Bind(R.id.rbFirstFragConfirm)
    RadioButton rbFirstFragConfirm;
    @Bind(R.id.radionoaddconfirm)
    RadioButton radionoaddconfirm;
    @Bind(R.id.radiononeaddconfirm)
    RadioButton radiononeaddconfirm;
    @Bind(R.id.rgAddconfirm)
    RadioGroup rgAddconfirm;
    @Bind(R.id.txtforlandmrak)
    EditText txtforlandmrak;
    @Bind(R.id.txtmetwith)
    EditText txtmetwith;
    @Bind(R.id.edtForrelation)
    EditText edtForrelation;
    @Bind(R.id.rbformarrid)
    RadioButton rbformarrid;
    @Bind(R.id.rbforunmarrid)
    RadioButton rbforunmarrid;
    @Bind(R.id.rgforrelation)
    RadioGroup rgforrelation;
    @Bind(R.id.txtforfamilymember)
    EditText txtforfamilymember;
    @Bind(R.id.edtforEarning_Member)
    EditText edtforEarningMember;
    @Bind(R.id.Located)
    TextView Located;
    @Bind(R.id.radiobuttonforactivitylocate)
    RadioButton radiobuttonforactivitylocate;
    @Bind(R.id.radionotseenactivitylocate)
    RadioButton radionotseenactivitylocate;
    @Bind(R.id.activitylocate)
    RadioGroup activitylocate;
    @Bind(R.id.textView10)
    TextView textView10;
    @Bind(R.id.spinnerforeduction)
    Spinner spinnerforeduction;
    @Bind(R.id.edtForYear_of_current_Resi)
    EditText edtForYearOfCurrentResi;
    @Bind(R.id.edtforYears_in_city)
    EditText edtforYearsInCity;
    @Bind(R.id.edtforidno)
    EditText edtforidno;
    @Bind(R.id.spinnerforResident_Status)
    Spinner spinnerforResidentStatus;
    @Bind(R.id.spinnerforLocality)
    Spinner spinnerforLocality;
    @Bind(R.id.txtformobileno)
    EditText txtformobileno;
    @Bind(R.id.txtforareaaquarefeet)
    EditText txtforareaaquarefeet;
    @Bind(R.id.rbforhouseflat)
    RadioButton rbforhouseflat;
    @Bind(R.id.rbforhouseIndependent)
    RadioButton rbforhouseIndependent;
    @Bind(R.id.rgfortypeofhouse)
    RadioGroup rgfortypeofhouse;
    @Bind(R.id.locality)
    TextView locality;
    @Bind(R.id.spinnerforresidentcons)
    Spinner spinnerforresidentcons;
    @Bind(R.id.exterior)
    TextView exterior;
    @Bind(R.id.radioexteriorgood)
    RadioButton radioexteriorgood;
    @Bind(R.id.radioexteriorpoor)
    RadioButton radioexteriorpoor;
    @Bind(R.id.radioexterioravergare)
    RadioButton radioexterioravergare;
    @Bind(R.id.radioexterior)
    RadioGroup radioexterior;
    @Bind(R.id.spinnerforinteriorseen)
    Spinner spinnerforinteriorseen;
    @Bind(R.id.txtfortcp1)
    EditText txtfortcp1;


    @Bind(R.id.radionamejobyes)
    RadioButton radionamejobyes;
    @Bind(R.id.radionamejobNo)
    RadioButton radionamejobNo;
    @Bind(R.id.radionamejobnone)
    RadioButton radionamejobnone;
    @Bind(R.id.radionamejob)
    RadioGroup radionamejob;


    @Bind(R.id.edtforresidinglast1)
    EditText edtforresidinglast1;
    @Bind(R.id.txtfortcp2)
    EditText txtfortcp2;


    @Bind(R.id.radionamejobyes1)
    RadioButton radionamejobyes1;
    @Bind(R.id.radionamejobNo1)
    RadioButton radionamejobNo1;
    @Bind(R.id.radionamejobnone1)
    RadioButton radionamejobnone1;
    @Bind(R.id.radionamejob1)
    RadioGroup radionamejob1;


    @Bind(R.id.edtforresidinglast2)
    EditText edtforresidinglast2;
    @Bind(R.id.txtforgatecolor)
    EditText txtforgatecolor;
    @Bind(R.id.txtforwallcolor)
    EditText txtforwallcolor;
    @Bind(R.id.edtforexecomment)
    EditText edtforexecomment;
    @Bind(R.id.edtforpermanentaddress)
    EditText edtforpermanentaddress;
    @Bind(R.id.edtforofficeandbussiaddressaddress)
    EditText edtforofficeandbussiaddressaddress;
    @Bind(R.id.edtforDesignation)
    EditText edtforDesignation;
    @Bind(R.id.right_nav)
    Button rightNav;

    public static String CPVId, type;
    public static String Education;

    public DataEntryRVRActivity() {
        // Required empty public constructor
        formData = FormDataForRVR.getInstance();
    }

    public static DataEntryRVRActivity getInstance() {
        if (mInstance == null) {
            mInstance = new DataEntryRVRActivity();
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.data_entry_rvr_activity);
        ButterKnife.bind(this);

        Calendar c = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd ");
        String VerifierClosedOn = dateFormat.format(c.getTime());
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+05:30"));
        Date currentLocalTime = cal.getTime();
        DateFormat time = new SimpleDateFormat("HH:mm a");
        time.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
        String localTime = time.format(currentLocalTime);
        Intent mIntent = getIntent();

        //using intent
        CPVId = mIntent.getStringExtra(TAGS.KEY_CPVID);
        type = mIntent.getStringExtra(TAGS.KEY_TYPE);
        formData.setCpv_id(CPVId);
        formData.setType(type);
        txtexecutivecode.setText(mIntent.getStringExtra(TAGS.KEY_EXECUTIVECODE));
        txtAppname.setText(mIntent.getStringExtra(TAGS.KEY_NAME));
        txtloanno.setText(mIntent.getStringExtra(TAGS.KEY_APPID));
        txtforlandmrak.setText(mIntent.getStringExtra(TAGS.KEY_LANDMARK));
        txtfordate.setText(VerifierClosedOn);
        txtfortime.setText(localTime);

        //for address confirm
        final int checkedRadioButtonId = rgAddconfirm.getCheckedRadioButtonId();
        rbFirstFragConfirm = (RadioButton) findViewById(checkedRadioButtonId);
        int index = rgAddconfirm.indexOfChild(rbFirstFragConfirm);
        formData.setAddress_confirm_value(rbFirstFragConfirm.getText().toString());
        formData.setAddress_confirm(index);

        //for located
        final int checkedactivitylocate = activitylocate.getCheckedRadioButtonId();
        radiobuttonforactivitylocate = (RadioButton) findViewById(checkedactivitylocate);
        formData.setLocated_value(radiobuttonforactivitylocate.getText().toString());
        formData.setLocated(activitylocate.indexOfChild(radiobuttonforactivitylocate));

        //for married status
        final int checkedrelation = rgforrelation.getCheckedRadioButtonId();
        rbformarrid = (RadioButton) findViewById(checkedrelation);
        formData.setStatusMarried(rbformarrid.getText().toString());
        formData.setMar_satus(rgforrelation.indexOfChild(rbformarrid));

        //for typeofhouse
        final int checketypeofhouse = rgfortypeofhouse.getCheckedRadioButtonId();
        rbforhouseflat = (RadioButton) findViewById(checketypeofhouse);
        formData.setType_of_house_value(rbforhouseflat.getText().toString());
        formData.setType_of_house_index(rgfortypeofhouse.indexOfChild(rbforhouseflat));

        //for job
        final int checkejobconf= radionamejob1.getCheckedRadioButtonId();
        radionamejobyes1 = (RadioButton) findViewById(checkejobconf);
        formData.setType_of_house_value(radionamejobyes1.getText().toString());
        formData.setType_of_house_index(radionamejob1.indexOfChild(radionamejobyes1));

        //for name
        final int checknameconf= radionamejob.getCheckedRadioButtonId();
        radionamejobyes = (RadioButton) findViewById(checknameconf);
        formData.setType_of_house_value(radionamejobyes.getText().toString());
        formData.setType_of_house_index(radionamejob.indexOfChild(radionamejobyes));

        //for exterior
        final int checkexterior= radioexterior.getCheckedRadioButtonId();
        radioexterioravergare = (RadioButton) findViewById(checkexterior);
        formData.setType_of_house_value(radioexterioravergare.getText().toString());
        formData.setType_of_house_index(radioexterior.indexOfChild(radioexterioravergare));


        //for all radio groups ie address confirm,located
        radioMethod();
        //for all spinners ie eductaion
        spinerMethod();
    }

    private void spinerMethod() {
        //eduction
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.Eduction_array,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforeduction.setAdapter(adapter);
        spinnerforeduction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Education = String.valueOf(spinnerforeduction.getSelectedItem());
                formData.setEducation_index(position);
                formData.setEducation_index_value(Education);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //locality
        ArrayAdapter<CharSequence> adapterforlocality = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.Locality_array_forrvr,
                android.R.layout.simple_spinner_item);
        adapterforlocality.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforLocality.setAdapter(adapterforlocality);
        spinnerforLocality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                formData.setLocality_index(position);
                formData.setLocality_value(spinnerforLocality.getSelectedItem().toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //resident status
        ArrayAdapter<CharSequence> adapterforresidentstatus = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.resident_status_forrvr,
                android.R.layout.simple_spinner_item);
        adapterforresidentstatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforResidentStatus.setAdapter(adapterforresidentstatus);
        spinnerforResidentStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                formData.setResident_status_index(position);
                formData.setResident_status_value(spinnerforResidentStatus.getSelectedItem().toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //resident construction
        ArrayAdapter<CharSequence> adapterforresidentconstruction = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.resident_construction_forrvr,
                android.R.layout.simple_spinner_item);
        adapterforresidentconstruction.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforresidentcons.setAdapter(adapterforresidentconstruction);
        spinnerforresidentcons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                formData.setResident_construction_index(position);
                formData.setResident_construction_value(spinnerforresidentcons.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //interior seen
        ArrayAdapter<CharSequence> adapterforinteriorseen = ArrayAdapter.createFromResource(getBaseContext(),
                R.array.InteriorSeen_array_forrvr,
                android.R.layout.simple_spinner_item);
        adapterforinteriorseen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerforinteriorseen.setAdapter(adapterforinteriorseen);
        spinnerforinteriorseen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                formData.setResident_construction_index(position);
                formData.setResident_construction_value(spinnerforinteriorseen.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void radioMethod() {
        activitylocate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radiobuttonforactivitylocate = (RadioButton) findViewById(checkedId);
                formData.setLocality_value(radiobuttonforactivitylocate.getText().toString());
                formData.setLocated(activitylocate.indexOfChild(radiobuttonforactivitylocate));
            }
        });
        rgAddconfirm.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                rbFirstFragConfirm = (RadioButton) findViewById(checkedId);
                formData.setAddress_confirm_value(rbFirstFragConfirm.getText().toString());
                formData.setAddress_confirm(rgAddconfirm.indexOfChild(rbFirstFragConfirm));

            }
        });
        rgforrelation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                rbformarrid = (RadioButton) findViewById(checkedId);
                formData.setMar_status_value(rbformarrid.getText().toString());
                formData.setMar_satus(rgforrelation.indexOfChild(rbformarrid));

            }
        });
        rgfortypeofhouse.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                rbforhouseflat = (RadioButton) findViewById(checkedId);
                formData.setType_of_house_index(rgfortypeofhouse.indexOfChild(rbforhouseflat));
                formData.setType_of_house_value(rbforhouseflat.getText().toString());
            }
        });
        //for job
        radionamejob1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radionamejobyes1 = (RadioButton) findViewById(checkedId);
                formData.setType_of_house_index(radionamejob1.indexOfChild(radionamejobyes1));
                formData.setType_of_house_value(radionamejobyes1.getText().toString());
            }
        });
        //for name
        radionamejob.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radionamejobyes = (RadioButton) findViewById(checkedId);
                formData.setType_of_house_index(radionamejob.indexOfChild(radionamejobyes));
                formData.setType_of_house_value(radionamejobyes.getText().toString());
            }
        });
        //for exterior
        radioexterior.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radioexteriorgood = (RadioButton) findViewById(checkedId);
                formData.setType_of_house_index(radioexterior.indexOfChild(radioexteriorgood));
                formData.setType_of_house_value(radioexteriorgood.getText().toString());
            }
        });
    }

    @OnClick(R.id.right_nav)
    public void onClick() {
        setFormData();
        uploadToServer();
    }

    private void setFormData() {
        formData.setDate(txtfordate.getText().toString());
        formData.setTime(txtfortime.getText().toString());
        formData.setLoan_no(txtloanno.getText().toString());
        formData.setExecutive_name(txtexecutivecode.getText().toString());
        formData.setLandmark(txtforlandmrak.getText().toString());
        formData.setApp_name(txtAppname.getText().toString());
        //app name missing
        formData.setMet_with(txtmetwith.getText().toString());
        formData.setFamily_member(txtforfamilymember.getText().toString());
        formData.setEarning_member(edtforEarningMember.getText().toString());
        formData.setDob(edtFordob.getText().toString());
        formData.setRelation(edtForrelation.getText().toString());
        formData.setMobile(txtformobileno.getText().toString());
        formData.setArea_sqft(txtforareaaquarefeet.getText().toString());
        formData.setYear_of_resident(edtForYearOfCurrentResi.getText().toString());
        formData.setYears_in_city(edtforYearsInCity.getText().toString());
        formData.setId_no(edtforidno.getText().toString());



        formData.setTcp1(txtfortcp1.getText().toString());
        formData.setResiding_last( edtforresidinglast1.getText().toString());
        formData.setTcp2(txtfortcp2.getText().toString());
        formData.setResiding_last2( edtforresidinglast2.getText().toString());
        formData.setGate_color(txtforgatecolor.getText().toString());
        formData.setWall_color(txtforwallcolor.getText().toString());

        formData.setExecutive_comment(edtforexecomment.getText().toString());
        formData.setOffice_address(edtforofficeandbussiaddressaddress.getText().toString());
        formData.setPermanent_address(edtforpermanentaddress.getText().toString());
        formData.setDesignation(edtforDesignation.getText().toString());


        final String jsonFormData = App.getInstance().getGSON().toJson(formData);
        App.getInstance().getPreferencesEditor().putString(TAGS.JSON_FORM_DATA_RVR, jsonFormData).commit();
    }

    private void uploadToServer() {
        final String formDataURL = API.getFormDataURLForRVR(formData);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, formDataURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //  Toast.makeText(getContext(),  response.toString() , Toast.LENGTH_LONG).show();
                Gson gson = new Gson();
                JsonClassForFragment allMainUser = gson.fromJson(response.toString(), JsonClassForFragment.class);
                final List<String> status = allMainUser.getStatus();
                String message = null;
                // retrieving data from string list array in for loop
                for (int i = 0; i < status.size(); i++) {
                    Log.i("Value of element " + i, status.get(i));
                    message = status.get(i);

                }
                if (message.equalsIgnoreCase("success")) {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    ButtonupdateActivity.updatebuttonvalueforRVR(CPVId,type,DataEntryRVRActivity.this);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(getApplicationContext(), "--" + "Error" + error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });
        App.getInstance().addToRequestQueue(request);

    }
}
