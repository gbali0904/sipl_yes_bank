package com.example.gunjan.spil_cpv_appyes_bank;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by App on 4/15/2016.
 */
public class SecondActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = SecondActivity.class.getSimpleName();
    String url = "http://survinindia.in/CAPITAL/APP_Json/AndroidDataBase/sms.php/?VerifierId=";

    RecyclerView rvUsers;
    TextView textView, textView3;
    String verifierId, name;
    String currentstatus;
    private List<User> userlist = new ArrayList<>();
    RecyclerViewAdapter recyclerViewAdapter;
    ProgressDialog pd1;
    String date1, date2;

    Date newStartdate, mydate;

    Date dateStart = new Date();
    Date dateEnd = new Date();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        // Session class instance
        textView = (TextView) findViewById(R.id.textViewUsername);
        textView3 = (TextView) findViewById(R.id.txttotal);
        rvUsers = (RecyclerView) findViewById(R.id.rvUsers);
        Intent intent = getIntent();

        textView.setText("User: " + intent.getStringExtra(TAGS.KEY_USERNAME));
        //for id
        verifierId = intent.getStringExtra(TAGS.KEY_USERID);


        rvUsers.setHasFixedSize(true);
        rvUsers.setLayoutManager(new LinearLayoutManager(SecondActivity.this));
        refreshList1();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshList();
            }
        });

        UpdateActivity.setOnUpdateListener(new UpdateActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {
                Toast.makeText(SecondActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                refreshList();

            }
        });


        ButtonupdateActivity.setOnUpdateListener(new ButtonupdateActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {
                refreshList();

            }
        });
    }

    //refreshing the fetching data
    public void refreshList() {

        if((userlist.isEmpty()))
        {
            Toast.makeText(getApplication(), "NO DATA TO REFFRESH", Toast.LENGTH_LONG).show();
            refreshList1();
        }
        else if (!userlist.equals(" ")) {
            userlist.clear();
            refreshList1();
            recyclerViewAdapter.notifyDataSetChanged();
        }
    }
    /*
* Make a server request for fetching data
* */
    private void refreshList1() {

        Intent intent1 = new Intent(this, MyService.class);
        intent1.putExtra(TAGS.KEY_USERID, verifierId);
        startService(intent1);

        final ProgressDialog pd = new ProgressDialog(SecondActivity.this);
        pd.setMessage("please wait fetching your data");
        pd.show();
        String finalurl = url + verifierId;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, finalurl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //calling method to parse json array

                parseData(response);
                pd.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                pd.dismiss();
            }
        });
        App.getInstance().addToRequestQueue(request);
    }

    private void parseData(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {

                JSONObject obj = response.getJSONObject(i);

                User user = new User();
                user.setdate(obj.getString("DateOfInitiation"));
                user.setaging(obj.getString("lastsms"));
                //geting string for aging
                String s1 = getStringForAgingData(obj);
                user.setfinalaging(s1);
                user.setpriority(obj.getString("PriorityCustomer"));
                user.setappno(obj.getString("ApplicationReferenceNumber"));
                user.settype(obj.getString("Type"));
                user.setname(obj.getString("ApplicantName"));
                user.setcpvid(obj.getString("CPVId"));
                user.setexecutivecode(obj.getString("ExecutiveCode"));
                user.setaddress(obj.getString("Address"));
                user.setpinno(obj.getString("PinCode"));
                user.setlandmark(obj.getString("Landmark"));
                user.setdepartment(obj.getString("Department"));
                user.setdesignation(obj.getString("Designation"));
                user.setextension(obj.getString("Extn"));
                user.setCurrentStatus(obj.getString("CurrentStatus"));
                user.setVerifyByMobile(obj.getString("VerifyByMobile"));
                user.setremark(obj.getString("REMARKS"));
                user.setbuttonvisibility(obj.getString("buttonverified"));
                // adding list to list array
                userlist.add(user);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            recyclerViewAdapter = new RecyclerViewAdapter(SecondActivity.this, userlist);
            rvUsers.setAdapter(recyclerViewAdapter);
        }
    }

    @NonNull
    private String getStringForAgingData(JSONObject obj) throws JSONException {
        DateFormat formatter = new SimpleDateFormat("yyy/MM/dd HH:mm:ss");
        String aging = obj.getString("lastsms");
        String returnres = null;
        //  Toast.makeText(this,"this"+aging,Toast.LENGTH_LONG).show();
        if (!aging.equals("")) {
            try {
                JSONObject jsonObj = new JSONObject(aging);
                date1 = jsonObj.getString("date");
                date1 = date1.replace("-", "/");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("thistag", "Total workday11" + "\n" + date1 + " day");

            Calendar c = Calendar.getInstance();
            date2 = formatter.format(c.getTime());
            try {
                dateStart = formatter.parse(date1);
                dateEnd = formatter.parse(date2);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            AgingActivity agingActivity = new AgingActivity();
            DateTimeFormatter formatter1 = DateTimeFormat.forPattern("yyy/MM/dd HH:mm:ss");
            DateTime start = formatter1.parseDateTime(date1);
            DateTime end = formatter1.parseDateTime(date2);
            Log.e("thistag", "Total workday" + "\n" + start + "\n" + date2 + " day");
            returnres = agingActivity.calculateWorkingPeriod(start, end);
            Log.e("thistag", "Total workday11" + "\n" + returnres + " day");
        } else {
            returnres = "Please Check ur Last Date";
        }
        return returnres;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_settings was selected
            case R.id.action_logout:
                //        Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT).show();
                logout();
                break;
            default:

                break;
        }
        return true;
    }

    public void logout() {
        SplashActivity.savePreferences(TAGS.LOGIN_USER,"");
        SplashActivity.savePreferences(TAGS.KEY_MOBILENO, "");
        SplashActivity.savePreferences(TAGS.KEY_PASS, "");
        SplashActivity.savePreferences(TAGS.KEY_STATUS, "");
        SplashActivity.savePreferences(TAGS.KEY_USERNAME, "");
        SplashActivity.savePreferences(TAGS.KEY_USERID, "");
        Intent ints=new Intent(SecondActivity.this,MainActivity.class);
        startActivity(ints);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true); // Iconify the widget
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }


    private List<User> filter(List<User> models, String query) {
        query = query.toLowerCase();

        final List<User> filteredModelList = new ArrayList<>();
        for (User model : models) {
            final String text = model.getname().toLowerCase();
            final String id = model.getaddress().toLowerCase();
            if (text.contains(query) || id.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final List<User> filteredModelList = filter(userlist, query);
        recyclerViewAdapter.setFilter(filteredModelList);
        return true;
    }


}


