package com.example.gunjan.spil_cpv_appyes_bank;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Period;
/*
*
 * Created by App on 5/10/2016.*/

public class AgingActivity {
    //for aging
    //TODO change with office start time
    private static final LocalTime START_TIME = new LocalTime().withHourOfDay(9).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    //TODO change with office end time
    private static final LocalTime END_TIME = new LocalTime().withHourOfDay(18).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    private static int workingPeriod = Hours.hoursBetween(START_TIME, END_TIME).getHours();

    int totalWorkingHour = 0;
    int totalWorkingMinute = 0;
    int totalWorkingSecond = 0;

    /**
     * Calculate total working duration from project start date time and project end date time
     * by excluding saturday and sunday and considering {@link #workingPeriod} as a full working day
     *
     * @param projectStart Starting date and time of the project
     * @param projectEnd   Ending date and time of the project
     */


    public String calculateWorkingPeriod(DateTime projectStart, DateTime projectEnd) {
        boolean isPeriodValid = true;
        String work = null;
        DateTime rectifiedProjectStart, rectifiedProjectEnd;

        //rectify projectStart time and projectEnd time
        rectifiedProjectStart = rectifyDateTime(projectStart);
        rectifiedProjectEnd = rectifyDateTime(projectEnd);

        //rectify projectStart date and projectEnd date
        if (rectifiedProjectStart.getDayOfWeek() == 7) {//if project start on sunday then reset it to Monday with START_TIME
            rectifiedProjectStart = new DateTime().withDate(rectifiedProjectStart.toLocalDate().plusDays(1)).withTime(START_TIME);
            System.out.println("rectified start date to " + rectifiedProjectStart.toLocalDate().toString() + " " + rectifiedProjectStart.toLocalTime().toString());
        }
        if (rectifiedProjectEnd.getDayOfWeek() == 7) {//if project ends on sunday then reset it to Saturday with END_TIME
            rectifiedProjectStart = new DateTime().withDate(rectifiedProjectStart.toLocalDate().minusDays(1)).withTime(END_TIME);
            System.out.println("rectified end date to " + rectifiedProjectStart.toLocalDate().toString() + " " + rectifiedProjectStart.toLocalTime().toString());
        }


        if (rectifiedProjectStart.isBefore(rectifiedProjectEnd)) {//starting date should before projectEnd date

            //calculate time for starting date
            if (rectifiedProjectStart.toLocalTime().isBefore(END_TIME)) {//work assigned before projectEnd of working day
                Period p = new Period(rectifiedProjectStart.toLocalTime(), END_TIME);
                calculateWorkingTimeFrom(p);
            }

            //calculating time for projectEnd date
            if (rectifiedProjectEnd.toLocalTime().isAfter(START_TIME)) {//work assigned before projectEnd of working day
                //calculate period for project date only if project startDate and project endDate are not date to avoid adding period twice
                if (!(rectifiedProjectStart.toLocalDate().isEqual(rectifiedProjectEnd.toLocalDate()))) {//if rectifiedProjectStart and rectifiedProjectEnd are not same
                    Period p = new Period(START_TIME, rectifiedProjectEnd.toLocalTime());
                    calculateWorkingTimeFrom(p);
                }
                else {
                    Period p = new Period(END_TIME, rectifiedProjectEnd.toLocalTime());
                    calculateWorkingTimeFrom(p);
                }
            }

            //calculating working time between projectStart date and projectEnd date
            for (LocalDate date = (rectifiedProjectStart.toLocalDate()).plusDays(1); //skip projectStart date
                 date.isBefore((rectifiedProjectEnd).toLocalDate()); //skip last date
                 date = date.plusDays(1)) {

                if (date.getDayOfWeek() != 7) {//skip saturday and sunday
                    totalWorkingHour += workingPeriod;//add full day time to total working hour
                    System.out.println("added hour from loop :" + workingPeriod + "total hours : " + totalWorkingHour + " for date" + date.toString());
                }
            }
            System.out.println("total working hours :" + totalWorkingHour);
            int workingDays = (totalWorkingHour / workingPeriod);//exclude 1 day for projectStart and projectEnd difference
            System.out.println("\ntotal working time duration for given dates is \n" +
                    workingDays + " Day " +
                    totalWorkingHour % workingPeriod + " : " +//print remaining hour after calculating a working day
                    totalWorkingMinute + " : " +
                    totalWorkingSecond  + " : ");


            if(totalWorkingMinute>=60 && totalWorkingSecond>=60)
            {
                totalWorkingMinute=totalWorkingMinute%60;
                totalWorkingSecond=totalWorkingSecond%60;
            }

            work = workingDays + " Day " +
                    totalWorkingHour % workingPeriod + " : " +//print remaining hour after calculating a working day
                    (totalWorkingMinute) + " : " +
                    (totalWorkingSecond );
        } else {//projectStart date or projectEnd date is not a working day or projectStart date is not before projectEnd date
            System.out.println("Start and projectEnd date should be a working day and projectStart date must have a date before projectEnd date");
        }


        return work;
    }

    /**
     * Rectify time slot according to {@link #START_TIME} and {@link #END_TIME}
     * if given time is before START_TIME then it will be reset to START_TIME and
     * if given time is after END_TIME then it will be reset to END_TIME
     * this method will ensure the time will always be in range of START_TIME and END_TIME
     *
     * @param dateTime given {@see #DateTime} Value
     * @return
     */
    private static DateTime rectifyDateTime(DateTime dateTime) {
        DateTime tempDateTime;
        if (dateTime.toLocalTime().isBefore(START_TIME)) {//if time is before START_TIME
            //generate new dateTime with same date and new time (reset to START_TIME)
            tempDateTime = new DateTime().withDate(dateTime.toLocalDate()).withTime(START_TIME.getHourOfDay(), 0, 0, 0);
        } else if (dateTime.toLocalTime().isAfter(END_TIME)) {//if time is after END_TIME
            //generate new dateTime with incremented date (current value of date plus 1)and new time (reset to START_TIME)
            tempDateTime = new DateTime().withDate(dateTime.toLocalDate()).withTime(END_TIME.getHourOfDay(), 0, 0, 0);
        } else {//time is between START_TIME and END_TIME
            tempDateTime = dateTime;//return as it is
        }
        System.out.println("rectified date " + dateTime.toLocalDate().toString() + " " + dateTime.toLocalTime().toString() + " to " +
                tempDateTime.toLocalDate().toString() + " " + tempDateTime.toLocalTime().toString());
        return tempDateTime;//return updated value of tempDateTime
    }

    /**
     * Calculate total working time from a given period and extract Hours,Minutes and Seconds
     * and add them to the {@link #totalWorkingHour} , {@link #totalWorkingMinute} and {@link #totalWorkingSecond} respectively
     *
     * @param p Given period
     */
    private void calculateWorkingTimeFrom(Period p) {
        if (p.getHours() <= workingPeriod) {
            totalWorkingHour += p.getHours();//add hour of this period to total working hour
            System.out.println("added hour :" + p.getHours() + "total hours : " + totalWorkingHour);
            totalWorkingMinute += p.getMinutes();//add minute of this period to total working minute
            totalWorkingSecond += p.getSeconds();//add seconds of this period to total working seconds
        } else {
            totalWorkingHour += workingPeriod;//add full day to total working hour
            System.out.println("added hour :" + workingPeriod + "total hours : " + totalWorkingHour);
        }
    }
}