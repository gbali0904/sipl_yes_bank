package com.example.gunjan.spil_cpv_appyes_bank;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

@SuppressWarnings("deprecation")
public class SplashActivity extends ActionBarActivity {
    TextView tv_logo,tv_sublogo;

    static SharedPreferences sharedPreferences;
    private static int Splash_TIME_DELEY = 3000;
    public static final String KEY_USERNAME = "username";
    public static final String KEY_VerifierId = "verifierId";
    public static final String KEY_MOBILENO = "mobileno";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.content_main);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                if (!SplashActivity.getPreferences(TAGS.LOGIN_USER, "").equalsIgnoreCase("")) {
                    String verifierId = SplashActivity.getPreferences(TAGS.KEY_USERID, "");
                    String name = SplashActivity.getPreferences(TAGS.KEY_USERNAME, "");
                    String mobileno=SplashActivity.getPreferences(TAGS.KEY_MOBILENO, "");
                    Intent i = new Intent(SplashActivity.this, SecondActivity.class);
                    i.putExtra(TAGS.KEY_USERNAME, name);
                    i.putExtra(TAGS.KEY_USERID, verifierId);
                    i.putExtra(TAGS.KEY_MOBILENO, mobileno);
                    // UtilClassForValidations.runService(Splash_Activity.this);
                    startActivity(i);
                    finish();
                }
                else {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    // UtilClassForValidations.runService(Splash_Activity.this);
                    startActivity(i);
                    finish();
                }

            }
        };
        Timer t = new Timer();
        t.schedule(task, Splash_TIME_DELEY);
    }
    public static void savePreferences(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static String getPreferences(String key, String val) {
        String value = sharedPreferences.getString(key, val);
        return value;
    }




}
