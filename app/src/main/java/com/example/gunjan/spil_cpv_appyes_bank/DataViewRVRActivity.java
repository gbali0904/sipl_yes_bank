package com.example.gunjan.spil_cpv_appyes_bank;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.gunjan.spil_cpv_appyes_bank.models.ViewDataEntryForRvr;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Gunjan on 11-11-2016.
 */

public class DataViewRVRActivity extends AppCompatActivity {

    private static final String TAG = DataViewRVRActivity.class.getSimpleName();
    public static final String BASE_URL_FOR_VIEW_RVR = "http://survinindia.in/capital/json/ViewDataEntryRVR.php?CPVId=";

    @Bind(R.id.edtexecutivecode)
    TextView edtexecutivecode;
    @Bind(R.id.edtloanno)
    TextView edtloanno;
    @Bind(R.id.txtfordate)
    TextView txtfordate;
    @Bind(R.id.textView5)
    TextView textView5;
    @Bind(R.id.txtfortime)
    TextView txtfortime;
    @Bind(R.id.edtAppname)
    TextView edtAppname;
    @Bind(R.id.edtFordob)
    TextView edtFordob;
    @Bind(R.id.txtforaddressconfirm)
    TextView txtforaddressconfirm;
    @Bind(R.id.textView7)
    TextView textView7;
    @Bind(R.id.txtforlandmrak)
    TextView txtforlandmrak;
    @Bind(R.id.textView8)
    TextView textView8;
    @Bind(R.id.txtmetwith)
    TextView txtmetwith;
    @Bind(R.id.edtForrelation)
    TextView edtForrelation;
    @Bind(R.id.txtformarridstatus)
    TextView txtformarridstatus;
    @Bind(R.id.txtforfamilymember)
    TextView txtforfamilymember;
    @Bind(R.id.textView9)
    TextView textView9;
    @Bind(R.id.edtforEarning_Member)
    TextView edtforEarningMember;
    @Bind(R.id.Located)
    TextView Located;
    @Bind(R.id.spinnerforeduction)
    TextView spinnerforeduction;
    @Bind(R.id.edtForYear_of_current_Resi)
    TextView edtForYearOfCurrentResi;
    @Bind(R.id.edtforYears_in_city)
    TextView edtforYearsInCity;
    @Bind(R.id.textView)
    TextView textView;
    @Bind(R.id.edtforidno)
    TextView edtforidno;
    @Bind(R.id.spinnerforResident_Status)
    TextView spinnerforResidentStatus;
    @Bind(R.id.spinnerforLocality)
    TextView spinnerforLocality;
    @Bind(R.id.txtformobileno)
    TextView txtformobileno;
    @Bind(R.id.txtforareaaquarefeet)
    TextView txtforareaaquarefeet;
    @Bind(R.id.txtfortypeofhouse)
    TextView txtfortypeofhouse;
    @Bind(R.id.spinnerforresidentcons)
    TextView spinnerforresidentcons;
    @Bind(R.id.exterior)
    TextView exterior;
    @Bind(R.id.spinnerforinteriorseen)
    TextView spinnerforinteriorseen;
    @Bind(R.id.txtfortcp1)
    TextView txtfortcp1;
    @Bind(R.id.txtfornamecof)
    TextView txtfornamecof;
    @Bind(R.id.edtforresidinglast1)
    TextView edtforresidinglast1;
    @Bind(R.id.txtfortcp2)
    TextView txtfortcp2;
    @Bind(R.id.txtforjobconf)
    TextView txtforjobconf;
    @Bind(R.id.edtforresidinglast2)
    TextView edtforresidinglast2;
    @Bind(R.id.txtforgatecolor)
    TextView txtforgatecolor;
    @Bind(R.id.txtforwallcolor)
    TextView txtforwallcolor;
    @Bind(R.id.edtforexecomment)
    TextView edtforexecomment;
    @Bind(R.id.edtforpermanentaddress)
    TextView edtforpermanentaddress;
    @Bind(R.id.edtforofficeandbussiaddressaddress)
    TextView edtforofficeandbussiaddressaddress;
    @Bind(R.id.edtforDesignation)
    TextView edtforDesignation;
    private String cpvid;
    private ViewDataEntryForRvr.UserDataBean user_data;
    private String type;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.data_view_rvr_activity);
        ButterKnife.bind(this);
        final Intent intent = getIntent();
        cpvid = intent.getStringExtra(TAGS.KEY_CPVID);
        type= intent.getStringExtra(TAGS.KEY_TYPE);

        viewDataMethodForrvr(cpvid);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewDataEntryForRvr.UserDataBean user_data_pass = user_data;
                Intent intent1 = new Intent(DataViewRVRActivity.this, EditValuesOfRVRActivity.class);
                intent1.putExtra(TAGS.KEY_CPVID, cpvid);
                intent1.putExtra(TAGS.KEY_TYPE,type);
                intent1.putExtra(TAGS.KEY_USERDATA, (Serializable) user_data_pass);
                startActivity(intent1);
            }
        });
        EditValuesOfRVRActivity.setOnUpdateListener(new EditValuesOfRVRActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {
                refreshList();

            }
        });


    }

    private void refreshList() {
        viewDataMethodForrvr(cpvid);
    }

    private void viewDataMethodForrvr(String cpvid) {
        String POST_VALUE = cpvid;
        String FULL_URL_FORBVR = BASE_URL_FOR_VIEW_RVR + POST_VALUE;
        Log.e(TAG, FULL_URL_FORBVR + "\n" + cpvid);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, FULL_URL_FORBVR, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = new Gson();
                ViewDataEntryForRvr viewDataEntryForRvr = gson.fromJson(response.toString(), ViewDataEntryForRvr.class);
                final String status = viewDataEntryForRvr.getStatus();
                if (status.equals("true")) {
                    user_data = viewDataEntryForRvr.getUser_data();
                    putValuesInTextRVR(user_data);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(getApplicationContext(), "--" + "Error" + error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });

        App.getInstance().addToRequestQueue(request);
    }
    private void putValuesInTextRVR(ViewDataEntryForRvr.UserDataBean user_data) {
        txtfortcp1.setText(user_data.getTpc1());
        spinnerforResidentStatus.setText(user_data.getResStatus());
        spinnerforLocality.setText(user_data.getLaoclity());
        txtformobileno.setText(user_data.getMobileNo());
        txtforareaaquarefeet.setText(user_data.getAreaSqft());
        txtfortypeofhouse.setText(user_data.getTypeOfHouse());
        spinnerforresidentcons.setText(user_data.getResiConstruction());
        exterior.setText(user_data.getExterior());
        spinnerforinteriorseen.setText(user_data.getInteriorSeen());
        txtfornamecof.setText(user_data.getNameConfirm());
        edtforresidinglast1 .setText(user_data.getResidingLast1());
        txtfortcp2 .setText(user_data.getTpc2());
        txtforjobconf .setText(user_data.getJobConfirm());
        edtforresidinglast2 .setText(user_data.getResidingLast2());
        txtforgatecolor.setText(user_data.getGateColor());
        txtforwallcolor .setText(user_data.getWallColor());
        edtforexecomment .setText(user_data.getExeComment());
        edtforpermanentaddress.setText(user_data.getPermAddress());
        edtforofficeandbussiaddressaddress.setText(user_data.getOffAddress());
        edtforidno.setText(user_data.getIDNo());
        edtforYearsInCity.setText(user_data.getYearsInCity());
        edtforDesignation.setText(user_data.getDesignation());
        edtexecutivecode.setText(user_data.getExecutive());
        edtloanno.setText(user_data.getLoanNo());
        txtfordate.setText(user_data.getDates());
        txtfortime.setText(user_data.getTimes());
        edtAppname .setText(user_data.getAppName());
        txtforaddressconfirm .setText(user_data.getAddConfirm());
        txtforlandmrak.setText(user_data.getLandmark());
        edtForYearOfCurrentResi.setText(user_data.getYrOfCurrResidence());
        txtmetwith .setText(user_data.getMetWith());
        edtForrelation.setText(user_data.getRelation());
        txtformarridstatus .setText("// no field in database");
        txtforfamilymember .setText(user_data.getFamilyMember());
        edtforEarningMember.setText(user_data.getEarningMember());
       Located.setText(user_data.getLoacate());
       spinnerforeduction .setText(user_data.getEducation());
       edtFordob.setText(user_data.getDOB());

    }
}
